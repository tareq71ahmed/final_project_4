$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    $("#sort").on("change", function() {
        var sort = $(this).val();
        var fabric = get_filter("fabric");
        var brand = get_filter("brand");
        var sleeve = get_filter("sleeve");
        var pattern = get_filter("pattern");
        var fit = get_filter("fit");
        var occasion = get_filter("occasion");
        var url = $("#url").val();
        $.ajax({
            url: url,
            method: "post",
            data: {
                fabric: fabric,
                brand: brand,
                sleeve: sleeve,
                pattern: pattern,
                fit: fit,
                occasion: occasion,
                sort: sort,
                url: url
            },
            success: function(data) {
                $(".filter_products").html(data);
            }
        });
    });

    // sort (sidebar)

    $(".fabric").on("click", function() {
        var brand = get_filter("brand");
        var fabric = get_filter("fabric");
        var sleeve = get_filter("sleeve");
        var pattern = get_filter("pattern");
        var fit = get_filter("fit");
        var occasion = get_filter("occasion");
        var sort = $("#sort option:selected").val();
        var url = $("#url").val();
        $.ajax({
            url: url,
            method: "post",
            data: {
                fabric: fabric,
                brand: brand,
                sleeve: sleeve,
                pattern: pattern,
                fit: fit,
                occasion: occasion,
                sort: sort,
                url: url
            },
            success: function(data) {
                $(".filter_products").html(data);
            }
        });
    });

    $(".brand").on("click", function() {
        var brand = get_filter("brand");
        var fabric = get_filter("fabric");
        var sleeve = get_filter("sleeve");
        var pattern = get_filter("pattern");
        var fit = get_filter("fit");
        var occasion = get_filter("occasion");
        var sort = $("#sort option:selected").val();
        var url = $("#url").val();
        $.ajax({
            url: url,
            method: "post",
            data: {
                brand: brand,
                fabric: fabric,
                sleeve: sleeve,
                pattern: pattern,
                fit: fit,
                occasion: occasion,
                sort: sort,
                url: url
            },
            success: function(data) {
                $(".filter_products").html(data);
            }
        });
    });

    $(".sleeve").on("click", function() {
        var brand = get_filter("brand");
        var fabric = get_filter("fabric");
        var sleeve = get_filter("sleeve");
        var pattern = get_filter("pattern");
        var fit = get_filter("fit");
        var occasion = get_filter("occasion");
        var sort = $("#sort option:selected").val();
        var url = $("#url").val();
        $.ajax({
            url: url,
            method: "post",
            data: {
                fabric: fabric,
                brand: brand,
                sleeve: sleeve,
                pattern: pattern,
                fit: fit,
                occasion: occasion,
                sort: sort,
                url: url
            },
            success: function(data) {
                $(".filter_products").html(data);
            }
        });
    });

    $(".pattern").on("click", function() {
        var brand = get_filter("brand");
        var fabric = get_filter("fabric");
        var sleeve = get_filter("sleeve");
        var pattern = get_filter("pattern");
        var fit = get_filter("fit");
        var occasion = get_filter("occasion");
        var sort = $("#sort option:selected").val();
        var url = $("#url").val();
        $.ajax({
            url: url,
            method: "post",
            data: {
                fabric: fabric,
                brand: brand,
                sleeve: sleeve,
                pattern: pattern,
                fit: fit,
                occasion: occasion,
                sort: sort,
                url: url
            },
            success: function(data) {
                $(".filter_products").html(data);
            }
        });
    });

    $(".fit").on("click", function() {
        var brand = get_filter("brand");
        var fabric = get_filter("fabric");
        var sleeve = get_filter("sleeve");
        var pattern = get_filter("pattern");
        var fit = get_filter("fit");
        var occasion = get_filter("occasion");
        var sort = $("#sort option:selected").val();
        var url = $("#url").val();
        $.ajax({
            url: url,
            method: "post",
            data: {
                brand: brand,
                fabric: fabric,
                sleeve: sleeve,
                pattern: pattern,
                fit: fit,
                occasion: occasion,
                sort: sort,
                url: url
            },
            success: function(data) {
                $(".filter_products").html(data);
            }
        });
    });

    $(".occasion").on("click", function() {
        var brand = get_filter("brand");
        var fabric = get_filter("fabric");
        var sleeve = get_filter("sleeve");
        var pattern = get_filter("pattern");
        var fit = get_filter("fit");
        var occasion = get_filter("occasion");
        var sort = $("#sort option:selected").val();
        var url = $("#url").val();
        $.ajax({
            url: url,
            method: "post",
            data: {
                brand: brand,
                fabric: fabric,
                sleeve: sleeve,
                pattern: pattern,
                fit: fit,
                occasion: occasion,
                sort: sort,
                url: url
            },
            success: function(data) {
                $(".filter_products").html(data);
            }
        });
    });

    function get_filter(class_name) {
        var filter = [];
        $("." + class_name + ":checked").each(function() {
            filter.push($(this).val());
        });
        return filter;
    }

    // Change price by Ajax

    $("#getPrice").change(function() {
        var size = $(this).val();
        if (size == "") {
            alert("Please Select Size");
            return false;
        }
        var product_id = $(this).attr("product-id");
        $.ajax({
            url: "/get-product-price",
            data: { size: size, product_id: product_id },
            type: "post",
            success: function(resp) {
                if (resp["discount"] > 0) {
                    $(".getAttrPrice").html(
                        "<del>BDT. " +
                            resp["product_price"] +
                            "</del> BDT. " +
                            resp["final_price"]
                    );
                } else {
                    $(".getAttrPrice").html("BDT. " + resp["product_price"]);
                }
            },
            error: function() {
                alert("Error");
            }
        });
    });
    // Change price by Ajax End

    //Update Cart Item
    $(document).on("click", ".btnItemUpdate", function() {
        if ($(this).hasClass("qtyMinus")) {
            var quantity = $(this)
                .prev()
                .val();
            if (quantity <= 1) {
                alert("Item Quantity must be 1 or Greater!");
                return false;
            } else {
                new_qty = parseInt(quantity) - 1;
            }
        }
        if ($(this).hasClass("qtyPlus")) {
            var quantity = $(this)
                .prev()
                .prev()
                .val();
            new_qty = parseInt(quantity) + 1;
        }
        var cartid = $(this).data("cartid");
        $.ajax({
            data: { cartid: cartid, qty: new_qty },
            url: "/update-cart-item-qty",
            type: "post",
            success: function(resp) {
                if (resp.status == false) {
                    alert(resp.message);
                }

                $(".totalCartItems").html(resp.totalCartItems);
                $("#AppendCartItems").html(resp.view);
            },
            error: function() {
                alert("Error");
            }
        });
    });
    //Update Cart Item End

    //Delete  Cart Item
    $(document).on("click", ".btnItemDelete", function() {
        var cartid = $(this).data("cartid");
        var result = confirm("Want To Delete Cart Item ?");
        if (result) {
            $.ajax({
                data: { cartid: cartid },
                url: "/delete-cart-item",
                type: "post",
                success: function(resp) {
                    $(".totalCartItems").html(resp.totalCartItems);
                    $("#AppendCartItems").html(resp.view);
                },
                error: function() {
                    alert("Error");
                }
            });
        }
    });
    //Delete  Cart Item  End

    // validate Register form on keyup and submit
    $("#registerForm").validate({
        rules: {
            name: "required",
            mobile: {
                required: true,
                minlength: 11,
                maxlength: 11,
                digits: true
            },
            email: {
                required: true,
                email: true,
                remote: "check-email"
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            name: "Please enter your Name",
            mobile: {
                required: "Please enter a valid Phone Number",
                minlength: "Your Phone Number Must contain at least 11 Digit",
                maxlength: "Your Phone Number Must contain at least 11 Digit",
                digits: "Please Enter Your Valid Mobile"
            },
            email: {
                required: "Please enter a valid email address",
                email: "Valid Email Address Required",
                remote: "Email Alreqady Exists"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 6 characters long"
            }
        }
    });
    // validate Register form on keyup and submit End

    //Validate Login Form
    $("#loginForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            email: {
                required: "Please enter Your valid email address",
                email: "Valid Email Address Required"
            },
            password: {
                required: "Please provide Your Correct password",
                minlength: "Your password must be at least 6 characters long"
            }
        }
    });
    //Validate Login Form End

    // validate account form on keyup and submit
    $("#accountForm").validate({
        rules: {
            name: {
                required: true,
                accept: "[a-zA-Z]+"
            },
            mobile: {
                required: true,
                minlength: 11,
                maxlength: 11,
                digits: true
            }
        },
        messages: {
            name: {
                required: "Please enter your name",
                accept: "Please enter valid Name"
            },
            mobile: {
                required: "Please enter a valid Phone Number",
                minlength: "Your Phone Number Must contain at least 11 Digit",
                maxlength: "Your Phone Number Must contain at least 11 Digit",
                digits: "Please Enter Your Valid Mobile"
            }
        }
    });

    // validate account form on keyup and submit End

    //Check Current Password
    $("#current_pwd").keyup(function() {
        var current_pwd = $(this).val();
        $.ajax({
            type: "post",
            url: "/check-user-pwd",
            data: { current_pwd: current_pwd },
            success: function(resp) {
                if (resp == "false") {
                    $("#chkPwd").html(
                        "<font color='red'>Current password is Incorrect</font>"
                    );
                } else if (resp == "true") {
                    $("#chkPwd").html(
                        "<font color='green'>Current password is Correct</font>"
                    );
                }
            },
            error: function() {
                alert("Error");
            }
        });
    });
    //Check Current Password End

    //User Password validation

    $("#passwordForm").validate({
        rules: {
            current_pwd: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
            new_pwd: {
                required: true,
                minlength: 6,
                maxlength: 20
            },
            confirm_pwd: {
                required: true,
                minlength: 6,
                maxlength: 20,
                equalTo: "#new_pwd"
            }
        },
        messages: {
            current_pwd: {
                required: "Please enter your current password",
                minlength: "Minlength Must be at least 6 characters",
                maxlength: "Maxlength must be at least 20 characters"
            },
            new_pwd: {
                required: "Please enter your new password",
                minlength: "Minlength Must be at least 6 characters",
                maxlength: "Maxlength must be at least 20 characters"
            },
            confirm_pwd: {
                required: "Please enter your confirm password",
                minlength: "Minlength Must be at least 6 characters",
                maxlength: "Maxlength must be at least 20 characters"
            }
        }
    });

    //User Password Validation End

    //Apply Coupon

    $("#ApplyCoupon").submit(function() {
        var user = $(this).attr("user");
        if (user == 1) {
        } else {
            alert("Please Login To Apply COUPON");
            return false;
        }

        var code = $("#code").val();
        $.ajax({
            type: "post",
            data: { code: code },
            url: "/apply-coupon",
            success: function(resp) {
                if (resp.message != "") {
                    alert(resp.message);
                }
                $(".totalCartItems").html(resp.totalCartItems);
                $("#AppendCartItems").html(resp.view);

                if (resp.couponAmount >= 0) {
                    $(".couponAmount").text("BDT." + resp.couponAmount);
                } else {
                    $(".couponAmount").text("BDT.0");
                }

                if (resp.grand_total >= 0) {
                    $(".grand_total").text("BDT." + resp.grand_total);
                }
            },
            error: function() {
                alert("Error");
            }
        });
    });

    //Apply Coupon End

    //Delete Delivery Address
    $(document).on("click", ".addressDelete", function() {
        var result = confirm("Want to Delete this Address");
        if (!result) {
            return false;
        }
    });
    //Delete Delivery Address End

    //Calculate Shipping Charges and updated Grand Total
    $("input[name=address_id]").bind("change", function() {
        var shipping_charges = $(this).attr("shipping_charges");
        var total_price = $(this).attr("total_price");
        var coupon_amount = $(this).attr("coupon_amount");
        var codpincodeCount = $(this).attr("codpincodeCount");
        var prepaidpincodeCount = $(this).attr("prepaidpincodeCount");

        if (codpincodeCount > 0) {
            //Show Cod Method
            $(".codMethod").show();
        } else {
            //Hide Cod Method
            $(".codMethod").hide();
        }

        if (prepaidpincodeCount > 0) {
            //Show prePaid Method
            $(".prepaidMethod").show();
        } else {
            //Hide prePaid Method
            $(".prepaidMethod").hide();
        }

        if (coupon_amount == "") {
            coupon_amount = 0;
        }
        $(".shipping_charges").html("BDT." + shipping_charges);
        var grand_total =
            parseInt(total_price) +
            parseInt(shipping_charges) -
            parseInt(coupon_amount);

        $(".grand_total").html("BDT." + grand_total);
    });
    //Calculate Shipping Charges and updated Grand Total End

    //check Pincode in Detail Page
    $("#checkPincode").click(function() {
        var pincode = $("#pincode").val();
        if (pincode == "") {
            alert("please Enter Delivery Post code");
            return false;
        }
        $.ajax({
            type: "post",
            data: { pincode: pincode },
            url: "/check-pincode",
            success: function(resp) {
                alert(resp);
            },
            error: function() {
                alert("Error");
            }
        });
    });
    //check Pincode in Detail Page End
});
