$(document).ready(function () {
    $("#current_pwd").keyup(function () {
        var current_pwd = $("#current_pwd").val();

        $.ajax({
            type: "post",
            url: "/admin/check-current-pwd",
            data: {current_pwd: current_pwd},
            success: function (resp) {
                if (resp == "false") {
                    $("#chkCurrentPwd").html(
                        "<font color = red >Current Password is incorrect</font>"
                    );
                } else if (resp == "true") {
                    $("#chkCurrentPwd").html(
                        "<font color = green >Current Password is Correct</font>"
                    );
                }
            },
            error: function () {
                alert("error");
            }
        });
    });

    //Update Sections Status
    $(document).on("click", ".updateSectionStatus", function () {
        var status = $(this).children("i").attr("status");
        var section_id = $(this).attr("section_id");
        $.ajax({
            type: "post",
            url: "/admin/update-section-status",
            data: {status: status, section_id: section_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#section-" + section_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#section-" + section_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });


    //Update Brands Status
    $(document).on("click", ".updateBrandStatus", function () {
        var status = $(this).children("i").attr("status");
        var brand_id = $(this).attr("brand_id");
        $.ajax({
            type: "post",
            url: "/admin/update-brand-status",
            data: {status: status, brand_id: brand_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#brand-" + brand_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#brand-" + brand_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });



    //Update CmsPage Status
    $(document).on("click", ".updateCmsStatus", function () {
        var status = $(this).children("i").attr("status");
        var page_id = $(this).attr("page_id");
        $.ajax({
            type: "post",
            url: "/admin/update-page-status",
            data: {status: status, page_id: page_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#page-" + page_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#page-" + page_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });



    //Update Categories status
    $(document).on("click", ".updateCategoryStatus", function () {
        var status = $(this).children("i").attr("status");
        var category_id = $(this).attr("category_id");
        $.ajax({
            type: "post",
            url: "/admin/update-category-status",
            data: {status: status, category_id: category_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#category-" + category_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#category-" + category_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });

    //Append Categories
    $("#section_id").change(function () {
        var section_id = $(this).val();
        $.ajax({
            type: "post",
            url: "/admin/append-categories-level",
            data: {section_id: section_id},
            success: function (resp) {
                $("#appendCategoriesLevel").html(resp);
            },
            error: function () {
                alert("Error");
            }
        });
    });


    //Update Product status
    $(document).on("click", ".updateProductStatus", function () {
        var status = $(this).children("i").attr("status");
        var product_id = $(this).attr("product_id");
        $.ajax({
            type: "post",
            url: "/admin/update-product-status",
            data: {status: status, product_id: product_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#product-" + product_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#product-" + product_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });


    //Update Attribute status
    $(document).on("click", ".updateAttributeStatus", function () {
        var status = $(this).children("i").attr("status");
        var attribute_id = $(this).attr("attribute_id");
        $.ajax({
            type: "post",
            url: "/admin/update-attribute-status",
            data: {status: status, attribute_id: attribute_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#attribute-" + attribute_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#attribute-" + attribute_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });


    // Confirm deletion of record By sweetAlert
    $(document).on("click", ".confirmDelete", function () {
        var record = $(this).attr("record");
        var recordid = $(this).attr("recordid");
        Swal.fire({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Yes, delete it!"
        }).then(result => {
            if (result.isConfirmed) {
                Swal.fire("Deleted!", "Your file has been deleted.", "success");
                window.location.href =
                    "/admin/delete-" + record + "/" + recordid;
            }
        });
    });


    //Products Attribute
    var maxField = 10; //Input fields increment limitation
    var addButton = $(".add_button"); //Add button selector
    var wrapper = $(".field_wrapper"); //Input field wrapper
    var fieldHTML =
        '<div style="margin-top:10px;margin-left:2px;"><div></div><input  type="text" name="size[]" style="width:120px" placeholder="Size" required  />&nbsp;<input  type="text" name="sku[]" style="width:120px" placeholder="SKU"  />&nbsp;<input  type="number" name="price[]" style="width:120px" placeholder="Price" min="1" />&nbsp;<input type="number" name="stock[]" style="width:120px" placeholder="Stock" min="1" />&nbsp;<a href="javascript:void(0);" class="remove_button">Remove</a></div>'; //New input field html
    var x = 1; //Initial field counter is 1
    //Once add button is clicked
    $(addButton).click(function () {
        //Check maximum number of input fields
        if (x < maxField) {
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    //Once remove button is clicked
    $(wrapper).on("click", ".remove_button", function (e) {
        e.preventDefault();
        $(this)
            .parent("div")
            .remove(); //Remove field html
        x--; //Decrement field counter
    });

    //Products Attribute End


    //Update Image Status
    $(document).on("click", ".updateImageStatus", function () {
        var status = $(this).children("i").attr("status");
        var image_id = $(this).attr("image_id");
        $.ajax({
            type: "post",
            url: "/admin/update-image-status",
            data: {status: status, image_id: image_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#image-" + image_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#image-" + image_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });

    //UpdateBanner Status
    $(document).on("click", ".updateBannerStatus", function () {
        var status = $(this).children("i").attr("status");
        var banner_id = $(this).attr("banner_id");
        $.ajax({
            type: "post",
            url: "/admin/update-banner-status",
            data: {status: status, banner_id: banner_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#banner-" + banner_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#banner-" + banner_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });




    //Update Coupon status

    $(document).on("click", ".updateCouponStatus", function () {
        var status = $(this).children("i").attr("status");
        var coupon_id = $(this).attr("coupon_id");
        $.ajax({
            type: "post",
            url: "/admin/update-coupon-status",
            data: {status: status, coupon_id: coupon_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#coupon-" + coupon_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#coupon-" + coupon_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });

    //Update Coupon status End


    //Show/hide Coupon Field
    $("#ManualCoupon").click(function(){
        $("#couponField").show();
    });

    $("#AutomaticCoupon").click(function(){
        $("#couponField").hide();
    });
    //Show/hide Coupon Field End


    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()
    //Datemask dd/mm/yyyy End


    //Show Couirer Name And Tracking Number in Case of  shipped status
        $("#courier_name").hide();
        $("#tracking_number").hide();
        $("#order_status").on("change",function(){
           // debugger;
           // alert(this.value);
            if(this.value=="Shipped"){
                $("#courier_name").show();
                $("#tracking_number").show();
            }else{
                $("#courier_name").hide();
                $("#tracking_number").hide();
            }
        });
    //Show Couirer Name And Tracking Number in Case of  shipped status End



// Update Shipping Status
    $(document).on("click", ".updateShippingStatus", function () {
        var status = $(this).children("i").attr("status");
        var shipping_id = $(this).attr("shipping_id");
        $.ajax({
            type: "post",
            url: "/admin/update-shipping-status",
            data: {status: status, shipping_id: shipping_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#shipping-" + shipping_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#shipping-" + shipping_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });





    // Update Users Status
    $(document).on("click", ".updateUserStatus", function () {
        var status = $(this).children("i").attr("status");
        var user_id = $(this).attr("user_id");
        $.ajax({
            type: "post",
            url: "/admin/update-user-status",
            data: {status: status, user_id: user_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#user-" + user_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#user-" + user_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });



    // Update Admin/ Sub-Admin

    $(document).on("click", ".updateAdminStatus", function () {
        var status = $(this).children("i").attr("status");
        var admin_id = $(this).attr("admin_id");
        $.ajax({
            type: "post",
            url: "/admin/update-admin-status",
            data: {status: status, admin_id: admin_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#admin-" + admin_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#admin-" + admin_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });

    // Update Admin/ Sub-Admin End


    // Update Rating & Review
    $(document).on("click", ".updateRatingStatus", function () {
        var status = $(this).children("i").attr("status");
        var rating_id = $(this).attr("rating_id");
        $.ajax({
            type: "post",
            url: "/admin/update-rating-status",
            data: {status: status, rating_id: rating_id},
            success: function (resp) {
                if (resp["status"] == 0) {
                    $("#rating-" + rating_id).html(
                        "<i title='Inactive' style='color:grey' class='fas fa-toggle-off' aria-hidden='true' status='Inactive'></i>"
                    );
                } else if (resp["status"] == 1) {
                    $("#rating-" + rating_id).html(
                        "<i  title='Active' class='fas fa-toggle-on' aria-hidden='true' status='Active'></i>"
                    );
                }
            },
            error: function () {
                alert("Error");
            }
        });
    });
    // Update Rating & Review End






});
