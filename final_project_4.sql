-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2021 at 11:28 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_project_4`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `type`, `mobile`, `email`, `email_verified_at`, `password`, `image`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admins', 'superadmin', '01679091279', 'admin@gmail.com', NULL, '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', '', 1, NULL, NULL, '2021-10-09 21:18:13'),
(2, 'Amit Saha', 'subadmin', '04160100961', 'amit@gmail.com', NULL, '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', '', 1, NULL, NULL, NULL),
(3, 'Bayzid Ahmed', 'subadmin', '04160100976', 'bayzid@gmail.com', NULL, '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', '', 1, NULL, NULL, NULL),
(4, 'Tareq Ahmed', 'admin', '04160100955', 'tareq@gmail.com', NULL, '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', '', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admins_roles`
--

CREATE TABLE `admins_roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `module` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_access` tinyint(4) NOT NULL,
  `edit_access` tinyint(4) NOT NULL,
  `full_access` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins_roles`
--

INSERT INTO `admins_roles` (`id`, `admin_id`, `module`, `view_access`, `edit_access`, `full_access`, `created_at`, `updated_at`) VALUES
(1, 2, 'categories', 1, 1, 1, NULL, NULL),
(2, 2, 'products', 1, 1, 1, NULL, NULL),
(3, 2, 'coupons', 1, 1, 1, NULL, NULL),
(4, 2, 'orders', 1, 1, 1, NULL, NULL),
(5, 3, 'categories', 1, 1, 1, NULL, NULL),
(6, 3, 'products', 1, 1, 1, NULL, NULL),
(7, 3, 'coupons', 1, 1, 1, NULL, NULL),
(8, 3, 'orders', 1, 1, 1, NULL, NULL),
(9, 4, 'categories', 1, 1, 1, NULL, NULL),
(10, 4, 'products', 1, 1, 1, NULL, NULL),
(11, 4, 'coupons', 1, 1, 1, NULL, NULL),
(12, 4, 'orders', 1, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `link`, `title`, `alt`, `status`, `created_at`, `updated_at`) VALUES
(1, 'banner1.png', 'product/6', 'Black Jacket', 'Black Jacket', 1, NULL, '2021-10-09 07:25:42'),
(2, 'banner2.png', 'product/1', 'Blue Casual T-shirt', 'Blue Casual T-shirt', 1, NULL, '2021-10-09 07:26:18'),
(3, 'banner2.png', 'product/1', 'Blue Casual T-shirt', 'Blue Casual T-shirt', 1, NULL, '2021-10-09 07:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Arrow', 1, NULL, NULL),
(2, 'Gap', 1, NULL, NULL),
(3, 'Lee', 1, NULL, NULL),
(4, 'Montee', 1, NULL, NULL),
(5, 'Peter England', 1, NULL, NULL),
(6, 'Polo', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_discount` double(8,2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `section_id`, `category_name`, `category_image`, `category_discount`, `description`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'T-shirts', '', 0.00, 'T-shirts are generally made of a stretchy, light and inexpensive fabric and are easy to clean. ', 't-shirts', '', '', '', 1, NULL, NULL),
(2, 1, 1, 'Casual T-shirts', '', 0.00, 'Casual T-shirts are generally made of a stretchy, light and inexpensive fabric and are easy to clean. ', 'casual-t-shirts', '', '', '', 1, NULL, NULL),
(3, 0, 1, 'Mens Winter Jacket', '42215.jpg', 20.00, '', 'Mens-Winter-Jacket', '', '', '', 1, '2021-09-28 11:24:25', '2021-09-28 11:24:25'),
(4, 3, 1, 'Winter Collection', NULL, 20.00, '', 'Winter-Collection', '', '', '', 1, '2021-09-30 07:57:22', '2021-09-30 07:57:34'),
(6, 0, 2, 'Women T-shirt', NULL, 10.00, '', 'women-T-shirt', '', '', '', 1, '2021-10-02 22:54:21', '2021-10-02 22:54:21'),
(7, 0, 2, 'women Jacket', NULL, 10.00, '', 'women-Jacket', '', '', '', 1, '2021-10-02 23:19:16', '2021-10-02 23:19:16'),
(8, 6, 2, 'Fabric T-shirt', NULL, 10.00, '', 'Print-T-Shirt', '', '', '', 1, '2021-10-03 08:02:22', '2021-10-03 08:15:57'),
(9, 7, 2, 'Ladies winter Jacket', NULL, 10.00, '', 'Ladies-winter-Jacket', '', '', '', 1, '2021-10-03 09:11:16', '2021-10-03 09:11:16'),
(11, 0, 3, 'Kids T-shirt', NULL, 10.00, '', 'kids', '', '', '', 1, '2021-10-10 03:10:03', '2021-10-10 03:10:03');

-- --------------------------------------------------------

--
-- Table structure for table `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `title`, `description`, `url`, `meta_title`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 'About us', 'Content Page is coming', 'about-us', 'About us', 'About E-commerce website', 'about-us,about-ecommerce', 1, '2021-09-28 18:11:26', '2021-10-09 21:17:18'),
(2, 'Privacy Policy', 'Content Page is coming', 'privacy-policy', 'Privacy policy', 'Privacy Policy of E-commerce website', 'privacy-policy', 1, '2021-09-22 18:11:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cod_pincodes`
--

CREATE TABLE `cod_pincodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cod_pincodes`
--

INSERT INTO `cod_pincodes` (`id`, `pincode`, `created_at`, `updated_at`) VALUES
(1, '1000', NULL, NULL),
(2, '1001', NULL, NULL),
(3, '1002', NULL, NULL),
(4, '1003', NULL, NULL),
(5, '1004', NULL, NULL),
(6, '1005', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AX', 'Åland Islands'),
(3, 'AL', 'Albania'),
(4, 'DZ', 'Algeria'),
(5, 'AS', 'American Samoa'),
(6, 'AD', 'Andorra'),
(7, 'AO', 'Angola'),
(8, 'AI', 'Anguilla'),
(9, 'AQ', 'Antarctica'),
(10, 'AG', 'Antigua and Barbuda'),
(11, 'AR', 'Argentina'),
(12, 'AM', 'Armenia'),
(13, 'AW', 'Aruba'),
(14, 'AU', 'Australia'),
(15, 'AT', 'Austria'),
(16, 'AZ', 'Azerbaijan'),
(17, 'BS', 'Bahamas'),
(18, 'BH', 'Bahrain'),
(19, 'BD', 'Bangladesh'),
(20, 'BB', 'Barbados'),
(21, 'BY', 'Belarus'),
(22, 'BE', 'Belgium'),
(23, 'BZ', 'Belize'),
(24, 'BJ', 'Benin'),
(25, 'BM', 'Bermuda'),
(26, 'BT', 'Bhutan'),
(27, 'BO', 'Bolivia, Plurinational State of'),
(28, 'BQ', 'Bonaire, Sint Eustatius and Saba'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British Indian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CA', 'Canada'),
(41, 'CV', 'Cape Verde'),
(42, 'KY', 'Cayman Islands'),
(43, 'CF', 'Central African Republic'),
(44, 'TD', 'Chad'),
(45, 'CL', 'Chile'),
(46, 'CN', 'China'),
(47, 'CX', 'Christmas Island'),
(48, 'CC', 'Cocos (Keeling) Islands'),
(49, 'CO', 'Colombia'),
(50, 'KM', 'Comoros'),
(51, 'CG', 'Congo'),
(52, 'CD', 'Congo, the Democratic Republic of the'),
(53, 'CK', 'Cook Islands'),
(54, 'CR', 'Costa Rica'),
(55, 'CI', 'Cote'),
(56, 'HR', 'Croatia'),
(57, 'CU', 'Cuba'),
(58, 'CW', 'Curaçao'),
(59, 'CY', 'Cyprus'),
(60, 'CZ', 'Czech Republic'),
(61, 'DK', 'Denmark'),
(62, 'DJ', 'Djibouti'),
(63, 'DM', 'Dominica'),
(64, 'DO', 'Dominican Republic'),
(65, 'EC', 'Ecuador'),
(66, 'EG', 'Egypt'),
(67, 'SV', 'El Salvador'),
(68, 'GQ', 'Equatorial Guinea'),
(69, 'ER', 'Eritrea'),
(70, 'EE', 'Estonia'),
(71, 'ET', 'Ethiopia'),
(72, 'FK', 'Falkland Islands'),
(73, 'FO', 'Faroe Islands'),
(74, 'FJ', 'Fiji'),
(75, 'FI', 'Finland'),
(76, 'FR', 'France'),
(77, 'GF', 'French Guiana'),
(78, 'PF', 'French Polynesia'),
(79, 'TF', 'French Southern Territories'),
(80, 'GA', 'Gabon'),
(81, 'GM', 'Gambia'),
(82, 'GE', 'Georgia'),
(83, 'DE', 'Germany'),
(84, 'GH', 'Ghana'),
(85, 'GI', 'Gibraltar'),
(86, 'GR', 'Greece'),
(87, 'GL', 'Greenland'),
(88, 'GD', 'Grenada'),
(89, 'GP', 'Guadeloupe'),
(90, 'GU', 'Guam'),
(91, 'GT', 'Guatemala'),
(92, 'GG', 'Guernsey'),
(93, 'GN', 'Guinea'),
(94, 'GW', 'Guinea-Bissau'),
(95, 'GY', 'Guyana'),
(96, 'HT', 'Haiti'),
(97, 'HM', 'Heard Island and McDonald Mcdonald Islands'),
(98, 'VA', 'Holy See'),
(99, 'HN', 'Honduras'),
(100, 'HK', 'Hong Kong'),
(101, 'HU', 'Hungary'),
(102, 'IS', 'Iceland'),
(103, 'IN', 'India'),
(104, 'ID', 'Indonesia'),
(105, 'IR', 'Iran'),
(106, 'IQ', 'Iraq'),
(107, 'IE', 'Ireland'),
(108, 'IM', 'Isle of Man'),
(109, 'IL', 'Israel'),
(110, 'IT', 'Italy'),
(111, 'JM', 'Jamaica'),
(112, 'JP', 'Japan'),
(113, 'JE', 'Jersey'),
(114, 'JO', 'Jordan'),
(115, 'KZ', 'Kazakhstan'),
(116, 'KE', 'Kenya'),
(117, 'KI', 'Kiribati'),
(118, 'KP', 'Korea'),
(119, 'KR', 'Korea'),
(120, 'KW', 'Kuwait'),
(121, 'KG', 'Kyrgyzstan'),
(122, 'LA', 'Lao'),
(123, 'LV', 'Latvia'),
(124, 'Lebanon', 'LB'),
(125, 'LS', 'Lesotho'),
(126, 'LR', 'Liberia'),
(127, 'LY', 'Libya'),
(128, 'LI', 'Liechtenstein'),
(129, 'LT', 'Lithuania'),
(130, 'LU', 'Luxembourg'),
(131, 'MO', 'Macao'),
(132, 'MK', 'Macedonia'),
(133, 'MG', 'Madagascar'),
(134, 'MW', 'Malawi'),
(135, 'MY', 'Malaysia'),
(136, 'MV', 'Maldives'),
(137, 'ML', 'Mali'),
(138, 'MT', 'Malta'),
(139, 'MH', 'Marshall Islands'),
(140, 'MQ', 'Martinique'),
(141, 'MR', 'Mauritania'),
(142, 'MU', 'Mauritius'),
(143, 'YT', 'Mayotte'),
(144, 'MX', 'Mexico'),
(145, 'FM', 'Micronesia'),
(146, 'MD', 'Moldova'),
(147, 'MC', 'Monaco'),
(148, 'MN', 'Mongolia'),
(149, 'ME', 'Montenegro'),
(150, 'MS', 'Montserrat'),
(151, 'MA', 'Morocco'),
(152, 'MZ', 'Mozambique'),
(153, 'MM', 'Myanmar'),
(154, 'NA', 'Namibia'),
(155, 'NR', 'Nauru'),
(156, 'NP', 'Nepal'),
(157, 'NL', 'Netherlands'),
(158, 'NC', 'New Caledonia'),
(159, 'NZ', 'New Zealand'),
(160, 'NI', 'Nicaragua'),
(161, 'NE', 'Niger'),
(162, 'NG', 'Nigeria'),
(163, 'NU', 'Niue'),
(164, 'NF', 'Norfolk Island'),
(165, 'MP', 'Northern Mariana Islands'),
(166, 'NO', 'Norway'),
(167, 'OM', 'Oman'),
(168, 'PK', 'Pakistan'),
(169, 'PW', 'Palau'),
(170, 'PS', 'Palestine'),
(171, 'PA', 'Panama'),
(172, 'PG', 'Papua New Guinea'),
(173, 'PY', 'Paraguay'),
(174, 'PE', 'Peru'),
(175, 'PH', 'Philippines'),
(176, 'PN', 'Pitcairn'),
(177, 'PL', 'Poland'),
(178, 'PT', 'Portugal'),
(179, 'PR', 'Puerto Rico'),
(180, 'QA', 'Qatar'),
(181, 'RE', 'Réunion'),
(182, 'RO', 'Romania'),
(183, 'RU', 'Russian Federation'),
(184, 'RW', 'Rwanda'),
(185, 'BL', 'Saint Barthélemy'),
(186, 'SH', 'Saint Helena'),
(187, 'KN', 'Saint Kitts and Nevis'),
(188, 'LC', 'Saint Lucia'),
(189, 'MF', 'Saint Martin'),
(190, 'PM', 'Saint Pierre and Miquelon'),
(191, 'VC', 'Saint Vincent and the Grenadines'),
(192, 'WS', 'Samoa'),
(193, 'SM', 'San Marino'),
(194, 'ST', 'Sao Tome and Principe'),
(195, 'SA', 'Saudi Arabia'),
(196, 'SN', 'Senegal'),
(197, 'RS', 'Serbia'),
(198, 'SC', 'Seychelles'),
(199, 'SL', 'Sierra Leone'),
(200, 'SG', 'Singapore'),
(201, 'SX', 'Sint Maarten'),
(202, 'SK', 'Slovakia'),
(203, 'SI', 'Slovenia'),
(204, 'SB', 'Solomon Islands'),
(205, 'SO', 'Somalia'),
(206, 'ZA', 'South Africa'),
(207, 'GS', 'South Georgia'),
(208, 'SS', 'South Sudan'),
(209, 'ES', 'Spain'),
(210, 'LK', 'Sri Lanka'),
(211, 'SD', 'Sudan'),
(212, 'SR', 'Suriname'),
(213, 'SJ', 'Svalbard and Jan Mayen'),
(214, 'SZ', 'Swaziland'),
(215, 'SE', 'Sweden'),
(216, 'CH', 'Switzerland'),
(217, 'SY', 'Syrian Arab Republic'),
(218, 'TW', 'Taiwan'),
(219, 'TJ', 'Tajikistan'),
(220, 'TZ', 'Tanzania, United Republic of'),
(221, 'TH', 'Thailand'),
(222, 'TL', 'Timor-Leste'),
(223, 'TG', 'Togo'),
(224, 'TK', 'Tokelau'),
(225, 'TO', 'Tonga'),
(226, 'TT', 'Trinidad and Tobago'),
(227, 'TN', 'Tunisia'),
(228, 'TR', 'Turkey'),
(229, 'TM', 'Turkmenistan'),
(230, 'TC', 'Turks and Caicos Islands'),
(231, 'TV', 'Tuvalu'),
(232, 'UG', 'Uganda'),
(233, 'UA', 'Ukraine'),
(234, 'AE', 'United Arab Emirates'),
(235, 'GB', 'United Kingdom'),
(236, 'US', 'United States'),
(237, 'UM', 'United States Minor Outlying Islands'),
(238, 'UY', 'Uruguay'),
(239, 'UZ', 'Uzbekistan'),
(240, 'VU', 'Vanuatu'),
(241, 'VE', 'Venezuela'),
(242, 'VN', 'Viet Nam'),
(243, 'VG', 'Virgin Islands'),
(244, 'VI', 'Virgin Islands'),
(245, 'WF', 'Wallis and Futuna'),
(246, 'EH', 'Western Sahara'),
(247, 'YE', 'Yemen'),
(248, 'ZM', 'Zambia'),
(249, 'ZW', 'Zimbabwe'),
(250, 'USA', 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `coupon_option` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `users` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `coupon_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `expiry_date` date NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `coupon_option`, `coupon_code`, `categories`, `users`, `coupon_type`, `amount_type`, `amount`, `expiry_date`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Manual', 'test,eidmubarak,ramadan', '1,2,3,4,6,8,7,9', 'tareq@gmail.com,amit@gmail.com,bayzid@gmail.com', 'Multiple Times', 'Percentage', 10.00, '2021-12-30', 1, NULL, '2021-10-03 09:24:38'),
(2, 'Manual', 'SUMMER2021', '1,2,3,4,6,8,7,9', 'tareq@gmail.com,amit@gmail.com,bayzid@gmail.com', 'Multiple Times', 'Percentage', 20.00, '2022-01-01', 1, '2021-10-01 03:44:41', '2021-10-03 09:25:31'),
(3, 'Manual', 'winter2021', '1,2,3,4,6,8,7,9', 'tareq@gmail.com,amit@gmail.com,bayzid@gmail.com', 'Multiple Times', 'Percentage', 10.00, '2022-09-11', 1, '2021-10-03 09:27:34', '2021-10-03 09:27:34');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_addresses`
--

CREATE TABLE `delivery_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_addresses`
--

INSERT INTO `delivery_addresses` (`id`, `user_id`, `name`, `address`, `city`, `state`, `pincode`, `country_name`, `mobile`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Tareq Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', '1000', 'Bangladesh', '01679091279', 0, NULL, NULL),
(2, 2, 'Amit saha', 'Dhaka', 'Dhaka', 'Dhaka', '1000', 'Bangladesh', '01000000000', 0, NULL, NULL),
(3, 3, 'Bayzid Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', '1000', 'Bangladesh', '02345678901', 0, NULL, NULL),
(4, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', '1000', 'Bangladesh', '01679091279', 0, '2021-09-28 11:44:42', '2021-09-28 11:44:42'),
(6, 4, 'samad sakib', 'Dhaka', 'Dhaka', 'Dhaka', '1000', 'Bangladesh', '01789012341', 0, '2021-10-09 07:30:41', '2021-10-09 07:30:41');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_02_16_134840_create_admins_table', 1),
(5, '2021_02_20_035256_create_sections_table', 1),
(6, '2021_02_21_034152_create_categories_table', 1),
(7, '2021_02_23_044842_create_products_table', 1),
(8, '2021_02_27_024209_create_products_attributes_table', 1),
(9, '2021_03_01_033624_create_products_images_table', 1),
(10, '2021_03_02_021437_create_brands_table', 1),
(11, '2021_03_05_035447_create_banners_table', 1),
(12, '2021_03_27_044101_create_carts_table', 1),
(13, '2021_04_24_035621_create_coupons_table', 1),
(14, '2021_04_24_042823_create_countries_table', 1),
(15, '2021_05_07_034755_create_delivery_addresses_table', 1),
(16, '2021_05_16_043337_create_orders_table', 1),
(17, '2021_05_16_045114_create_orders_products_table', 1),
(18, '2021_05_31_040215_create_order_statuses_table', 1),
(19, '2021_06_03_100248_create_orders_logs_table', 1),
(20, '2021_06_03_154705_update_orders_table', 1),
(21, '2021_06_14_145004_create_shipping_charges_table', 1),
(22, '2021_06_21_054205_create_cod_pincodes_table', 1),
(23, '2021_06_21_054327_create_prepaid_pincodes_table', 1),
(24, '2021_07_17_145258_create_cms_pages_table', 1),
(25, '2021_08_29_142734_create_admins_roles_table', 1),
(26, '2021_08_31_041435_create_other_settings_table', 1),
(27, '2021_09_06_152514_create_ratings_table', 1),
(28, '2021_10_04_032453_create_refund_orders_table', 2),
(29, '2021_10_05_103040_create_ssl_comerze_histories_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shipping_charges` double(8,2) NOT NULL,
  `coupon_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_amount` double(8,2) DEFAULT NULL,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_getway` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grand_total` double(8,2) NOT NULL,
  `courier_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tracking_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `name`, `address`, `city`, `state`, `country`, `pincode`, `mobile`, `email`, `shipping_charges`, `coupon_code`, `coupon_amount`, `order_status`, `payment_status`, `payment_method`, `payment_getway`, `grand_total`, `courier_name`, `tracking_number`, `created_at`, `updated_at`) VALUES
(35, 1, 'Tareq Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Cancelled', 'Due', 'COD', 'COD', 1240.00, '', '', '2021-10-09 07:18:35', '2021-10-09 07:18:35'),
(36, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'New', 'Success Refund', 'Paypal', 'Paypal', 580.00, '', '', '2021-10-09 07:19:23', '2021-10-09 07:19:23'),
(37, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'New', 'Success Refund', 'Paypal', 'Paypal', 670.00, '', '', '2021-10-09 07:20:28', '2021-10-09 07:20:28'),
(38, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Shipped', 'Paid', 'SSLCommerz', 'SSLCommerz', 580.00, 'Redex', 'Redex7894', '2021-10-09 07:21:42', '2021-10-09 07:22:23'),
(39, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Delivered', 'Paid', 'SSLCommerz', 'SSLCommerz', 580.00, 'Redex', 'ReDEX2341', '2021-10-09 07:23:19', '2021-10-09 07:24:16'),
(40, 2, 'Amit saha', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01000000000', 'amit@gmail.com', 40.00, NULL, NULL, 'New', 'Paid', 'Paypal', 'Paypal', 805.00, '', '', '2021-10-09 07:31:24', '2021-10-09 07:31:24'),
(41, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Delivered', 'Paid', 'COD', 'COD', 580.00, 'Redex', 'Redex123456', '2021-10-09 08:41:35', '2021-10-09 08:43:51'),
(42, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Cancelled', 'Due', 'COD', 'COD', 490.00, '', '', '2021-10-09 08:45:25', '2021-10-09 08:45:25'),
(43, 1, 'Tareq Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Cancelled', 'Due', 'Paypal', 'Paypal', 580.00, '', '', '2021-10-09 08:46:18', '2021-10-09 08:46:18'),
(44, 1, 'Tareq Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'New', 'Success Refund', 'Paypal', 'Paypal', 580.00, '', '', '2021-10-09 08:47:32', '2021-10-09 08:47:32'),
(45, 1, 'Tareq Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Shipped', 'Paid', 'SSLCommerz', 'SSLCommerz', 580.00, 'Ecouirer', 'Evl03r432', '2021-10-09 08:51:15', '2021-10-09 08:54:52'),
(46, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'Cancelled', 'Due', 'COD', 'COD', 580.00, '', '', '2021-10-09 21:19:24', '2021-10-09 21:19:24'),
(47, 1, 'Tareq Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'New', 'Due', 'COD', 'COD', 940.00, '', '', '2021-10-10 03:18:01', '2021-10-10 03:18:01'),
(48, 1, 'Tareq Ahmed', 'Dhaka 1200', 'Dhaka', 'Khilkhet', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', 40.00, NULL, NULL, 'New', 'Paid', 'Paypal', 'Paypal', 625.00, '', '', '2021-10-10 03:18:38', '2021-10-10 03:18:38');

-- --------------------------------------------------------

--
-- Table structure for table `orders_logs`
--

CREATE TABLE `orders_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_logs`
--

INSERT INTO `orders_logs` (`id`, `order_id`, `order_status`, `created_at`, `updated_at`) VALUES
(11, 38, 'Shipped', '2021-10-09 07:22:28', '2021-10-09 07:22:28'),
(12, 39, 'Shipped', '2021-10-09 07:24:10', '2021-10-09 07:24:10'),
(13, 39, 'Delivered', '2021-10-09 07:24:20', '2021-10-09 07:24:20'),
(14, 41, 'Shipped', '2021-10-09 08:43:07', '2021-10-09 08:43:07'),
(15, 41, 'Delivered', '2021-10-09 08:43:55', '2021-10-09 08:43:55'),
(16, 45, 'Shipped', '2021-10-09 08:54:57', '2021-10-09 08:54:57');

-- --------------------------------------------------------

--
-- Table structure for table `orders_products`
--

CREATE TABLE `orders_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_qty` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_products`
--

INSERT INTO `orders_products` (`id`, `order_id`, `user_id`, `product_id`, `product_code`, `product_name`, `product_color`, `product_size`, `product_price`, `product_qty`, `created_at`, `updated_at`) VALUES
(37, 35, 1, 16, 'Trench_Coat_01', 'Trench Coat', 'Almond', 'M-Size 33/35', 1200.00, 1, '2021-10-09 07:18:35', '2021-10-09 07:18:35'),
(38, 36, 1, 22, 'soft-04', 'Soft Fabric T-shirt', 'white', 'M-size 32', 540.00, 1, '2021-10-09 07:19:23', '2021-10-09 07:19:23'),
(39, 37, 1, 24, 'solid-02', 'Women Solid Jacket Coat', 'Pink', 'M-size 38', 630.00, 1, '2021-10-09 07:20:28', '2021-10-09 07:20:28'),
(40, 38, 1, 20, 'Woolen-Coat', 'Woolen Coat', 'Grey', 'M-size-32/32', 540.00, 1, '2021-10-09 07:21:42', '2021-10-09 07:21:42'),
(41, 39, 1, 20, 'Woolen-Coat', 'Woolen Coat', 'Grey', 'M-size-32/32', 540.00, 1, '2021-10-09 07:23:19', '2021-10-09 07:23:19'),
(42, 40, 2, 23, 'anorak-01', 'Anorak Jacket', 'Grey-Pink', 'S-size 34', 765.00, 1, '2021-10-09 07:31:24', '2021-10-09 07:31:24'),
(43, 41, 1, 21, 'Floral', 'Floral Print T Shirt', 'Grey', 'M-size 28', 540.00, 1, '2021-10-09 08:41:35', '2021-10-09 08:41:35'),
(44, 42, 1, 20, 'Woolen-Coat', 'Woolen Coat', 'Grey', 'S-size-30/31', 450.00, 1, '2021-10-09 08:45:25', '2021-10-09 08:45:25'),
(45, 43, 1, 20, 'Woolen-Coat', 'Woolen Coat', 'Grey', 'M-size-32/32', 540.00, 1, '2021-10-09 08:46:18', '2021-10-09 08:46:18'),
(46, 44, 1, 20, 'Woolen-Coat', 'Woolen Coat', 'Grey', 'M-size-32/32', 540.00, 1, '2021-10-09 08:47:32', '2021-10-09 08:47:32'),
(47, 45, 1, 20, 'Woolen-Coat', 'Woolen Coat', 'Grey', 'M-size-32/32', 540.00, 1, '2021-10-09 08:51:15', '2021-10-09 08:51:15'),
(48, 46, 1, 21, 'Floral', 'Floral Print T Shirt', 'Grey', 'M-size 28', 540.00, 1, '2021-10-09 21:19:24', '2021-10-09 21:19:24'),
(49, 47, 1, 28, 'kids-01', 'kids cotton T-shirt', 'Yellow', 'Age 1 Year', 450.00, 2, '2021-10-10 03:18:01', '2021-10-10 03:18:01'),
(50, 48, 1, 27, 'pakra-0341', 'Parka Cotton Jacket', 'Black', 'M-size 30', 585.00, 1, '2021-10-10 03:18:38', '2021-10-10 03:18:38');

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'New', 1, NULL, NULL),
(2, 'Pending', 1, NULL, NULL),
(3, 'Hold', 1, NULL, NULL),
(4, 'Cancelled', 1, NULL, NULL),
(5, 'In Process', 1, NULL, NULL),
(6, 'Paid', 0, NULL, NULL),
(7, 'Shipped', 1, NULL, NULL),
(8, 'Delivered', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `other_settings`
--

CREATE TABLE `other_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `min_cart_value` int(11) NOT NULL,
  `max_cart_value` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `other_settings`
--

INSERT INTO `other_settings` (`id`, `min_cart_value`, `max_cart_value`, `created_at`, `updated_at`) VALUES
(1, 250, 10000, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prepaid_pincodes`
--

CREATE TABLE `prepaid_pincodes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prepaid_pincodes`
--

INSERT INTO `prepaid_pincodes` (`id`, `pincode`, `created_at`, `updated_at`) VALUES
(1, '1000', NULL, NULL),
(2, '1001', NULL, NULL),
(3, '1002', NULL, NULL),
(4, '1003', NULL, NULL),
(5, '1004', NULL, NULL),
(6, '1005', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` double(8,2) NOT NULL,
  `product_discount` double(8,2) NOT NULL,
  `product_weight` double(8,2) NOT NULL,
  `product_video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wash_care` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fabric` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pattern` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sleeve` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `occasion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` enum('No','Yes') COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `section_id`, `brand_id`, `product_name`, `product_code`, `product_color`, `group_code`, `product_price`, `product_discount`, `product_weight`, `product_video`, `main_image`, `description`, `wash_care`, `fabric`, `pattern`, `sleeve`, `fit`, `occasion`, `meta_title`, `meta_description`, `meta_keywords`, `is_featured`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, 'Blue Casual T-shirt', 'BT001', 'Blue', '100', 300.00, 10.00, 200.00, '', 'Royal-Blue-T-shirt-for-men.jpg-91182.jpg', 'A T-shirt (or t-shirt, or tee) is a style of unisex fabric shirt named after the T shape of its body and sleeves. Traditionally it has short sleeves and a round neckline, known as a crew neck, which lacks a collar. T-shirts are generally made of a stretchy, light and inexpensive fabric and are easy to clean.', 'Yes', 'Cotton', 'Plain', 'Half Sleeve', 'Slim', 'Casual', '', '', '', 'No', 1, NULL, '2021-10-02 23:30:55'),
(2, 2, 1, 2, 'Red Casual T-shirt', 'R001', 'Red', '100', 300.00, 10.00, 200.00, '', 'istockphoto-180807014-170667a.jpg-26601.jpg', 'A T-shirt (or t-shirt, or tee) is a style of unisex fabric shirt named after the T shape of its body and sleeves. Traditionally it has short sleeves and a round neckline, known as a crew neck, which lacks a collar. T-shirts are generally made of a stretchy, light and inexpensive fabric and are easy to clean.', 'Yes', 'Cotton', 'Plain', 'Half Sleeve', 'Slim', 'Casual', '', '', '', 'Yes', 1, NULL, '2021-09-29 21:25:44'),
(3, 2, 1, 1, 'Black Casual T-shirt', 'T-shirt-01', 'Black', '100', 300.00, 20.00, 200.00, NULL, '67dee4e83ce9365a1cbc4cdd225a655b.jpg-7378.jpg', 'Size: S-Length 26,Chest 36\r\nSize: M-Length 27,Chest 38\r\nSize: L-Length 28,Chest 40', 'Yes', '', 'Self', '', '', '', '', '', '', 'Yes', 1, '2021-09-28 11:14:39', '2021-09-29 21:25:46'),
(4, 3, 1, 6, 'Mens Winter Jacket', 'Winter-Jacket-01', 'Black', '200', 700.00, 20.00, 300.00, NULL, '51blKojTSXL._UX569_.jpg-51323.jpg', '', 'No', '', 'solid', 'Full Sleeve', 'Regular', '', '', '', '', 'Yes', 1, '2021-09-28 11:26:45', '2021-09-29 21:53:44'),
(5, 3, 1, 6, 'Puffer Winter Jacket', 'puffer-01', 'ceuro', '200', 700.00, 20.00, 300.00, NULL, 'e89c089a9f0ff7032bef86af51b9301e.jpg-27442.jpg', '', '', 'wool', '', 'Full Sleeve', 'Regular', 'Casual', '', '', '', 'Yes', 1, '2021-09-28 11:32:38', '2021-10-02 23:29:20'),
(6, 3, 1, 6, 'Hooded Jacket', 'Hooded-01', 'Navy Blue', '300', 700.00, 20.00, 300.00, NULL, 'Winter-Jacket-Men-Parka-Fashion-Hooded-Jacket-Slim-Cotton-Warm-Jacket-Coat-Men-Solid-Colo-Thick__20552.1609419721.jpg-43216.jpg', '', 'Yes', 'wool', 'Self', 'Full Sleeve', 'Regular', 'Casual', '', '', '', 'Yes', 1, '2021-09-28 11:39:15', '2021-10-02 23:29:28'),
(11, 3, 1, 5, 'Jeans Jacket', 'jeans_jacket-01', 'Green', '99', 500.00, 10.00, 200.00, NULL, '373901633010004.jpg', '', 'No', 'Polyester', 'Plain', 'Full Sleeve', 'Slim', 'Casual', '', '', '', 'Yes', 1, '2021-09-30 07:51:10', '2021-10-02 23:29:55'),
(12, 4, 1, 5, 'China Winter Jacket', 'china_jacket-01', 'Green', '99', 500.00, 10.00, 300.00, NULL, '181591633010875.jpg', '', 'Yes', 'Cotton', 'solid', 'Full Sleeve', 'Regular', 'Formal', '', '', '', 'Yes', 1, '2021-09-30 08:07:55', '2021-10-02 23:30:14'),
(16, 4, 1, 5, 'Trench Coat', 'Trench_Coat_01', 'Almond', '500', 800.00, 20.00, 300.00, NULL, '522181633184821.jpg', '', 'No', 'Cotton', 'solid', 'Half Sleeve', 'Regular', 'Formal', '', '', '', 'Yes', 1, '2021-10-02 08:27:01', '2021-10-02 23:27:33'),
(17, 4, 1, 4, 'Black Cotton switer', 'switter_034', 'Black', '453', 800.00, 15.00, 300.00, NULL, '493301633185788.jpg', '', 'Yes', 'Polyester', 'Self', 'Half Sleeve', 'Regular', 'Formal', '', '', '', 'Yes', 1, '2021-10-02 08:43:08', '2021-10-02 23:27:41'),
(18, 6, 2, 5, 'women Royal Blue T-shirt', 'women-Royal-Blue', 'Royal Blue', '50', 500.00, 10.00, 200.00, NULL, '214991633237339.jpg', '', 'Yes', 'Polyester', 'Plain', 'Half Sleeve', 'Regular', 'Casual', '', '', '', 'Yes', 1, '2021-10-02 23:02:21', '2021-10-02 23:27:50'),
(19, 6, 2, 5, 'women Royal Red T-shirt', 'women-T-shirt', 'Royal  Red', '50', 500.00, 10.00, 200.00, NULL, '442051633237830.jpg', '', 'Yes', 'Polyester', 'Plain', 'Half Sleeve', 'Regular', 'Casual', '', '', '', 'Yes', 1, '2021-10-02 23:10:30', '2021-10-02 23:28:00'),
(20, 7, 2, 4, 'Woolen Coat', 'Woolen-Coat', 'Grey', '34', 500.00, 10.00, 300.00, NULL, '822071633238679.jpg', '', 'yes', 'wool', 'Plain', 'Short Sleeve', 'Regular', 'Casual', '', '', '', 'Yes', 1, '2021-10-02 23:24:39', '2021-10-02 23:28:10'),
(21, 6, 2, 6, 'Floral Print T Shirt', 'Floral', 'Grey', '11', 500.00, 10.00, 190.00, NULL, '65291633269437.jpg', '', 'Yes', '', 'Printed', 'Half Sleeve', 'Regular', '', '', '', '', 'No', 1, '2021-10-03 07:57:18', '2021-10-03 07:59:10'),
(22, 8, 2, 4, 'Soft Fabric T-shirt', 'soft-04', 'white', '21', 500.00, 10.00, 200.00, NULL, '1681633270347.jpg', '', '', 'Cotton', 'Printed', 'Half Sleeve', 'Slim', 'Casual', '', '', '', 'No', 1, '2021-10-03 08:12:27', '2021-10-03 08:12:27'),
(23, 7, 2, 1, 'Anorak Jacket', 'anorak-01', 'Grey-Pink', '12', 750.00, 10.00, 300.00, NULL, '215371633271518.jpg', '', 'Yes', '', 'Printed', 'Half Sleeve', 'Regular', '', '', '', '', 'No', 1, '2021-10-03 08:31:58', '2021-10-03 08:31:58'),
(24, 9, 2, 3, 'Women Solid Jacket Coat', 'solid-02', 'Pink', '12', 500.00, 10.00, 300.00, NULL, '869871633274057.jpg', '', 'No', 'Polyester', 'Plain', 'Half Sleeve', 'Slim', 'Casual', '', '', '', 'No', 1, '2021-10-03 09:14:17', '2021-10-03 09:14:17'),
(26, 8, 2, 3, 'Fabric T-shirt', 'Fabric-0010', 'Grey', '1012', 500.00, 10.00, 200.00, NULL, '866471633856118.png', '', 'Yes', 'Cotton', 'Self', 'Half Sleeve', 'Regular', 'Formal', '', '', '', 'No', 1, '2021-10-10 02:54:24', '2021-10-10 02:55:19'),
(27, 9, 2, 5, 'Parka Cotton Jacket', 'pakra-0341', 'Black', '1212', 550.00, 10.00, 200.00, NULL, '987611633856684.jpg', '', 'Yes', 'Polyester', 'Plain', 'Short Sleeve', 'Regular', 'Casual', '', '', '', 'No', 1, '2021-10-10 03:04:45', '2021-10-10 03:04:45'),
(28, 11, 3, 1, 'kids cotton T-shirt', 'kids-01', 'Yellow', '3212', 500.00, 10.00, 150.00, NULL, '141261633857136.jpg', '', 'Yes', 'Cotton', 'Printed', 'Half Sleeve', 'Regular', 'Casual', '', '', '', 'No', 1, '2021-10-10 03:12:16', '2021-10-10 03:14:33');

-- --------------------------------------------------------

--
-- Table structure for table `products_attributes`
--

CREATE TABLE `products_attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `stock` int(11) NOT NULL,
  `sku` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_attributes`
--

INSERT INTO `products_attributes` (`id`, `product_id`, `size`, `price`, `stock`, `sku`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'S-Size 26', 1200.00, 50, 'BT001-S', 1, NULL, '2021-10-02 08:13:29'),
(2, 1, 'M-Size 30', 1300.00, 50, 'BT001-M', 1, NULL, '2021-10-02 08:13:29'),
(3, 1, 'L-Size 32', 1400.00, 50, 'BT001-L', 1, NULL, '2021-10-02 08:13:29'),
(4, 2, 'S-Size 26', 300.00, 32, 'small-01', 1, '2021-09-28 09:29:07', '2021-10-02 08:13:53'),
(5, 2, 'M-Size 30', 400.00, 24, 'medium-01', 1, '2021-09-28 09:29:07', '2021-10-02 08:13:53'),
(6, 2, 'L-Size 32', 500.00, 27, 'large-01', 1, '2021-09-28 09:29:07', '2021-10-02 08:13:53'),
(7, 3, 'S-Size 26', 300.00, 40, 'S-Length 26', 1, '2021-09-28 11:21:43', '2021-09-28 11:21:43'),
(8, 3, 'M-Size 27', 400.00, 49, 'M-Length 27', 1, '2021-09-28 11:21:43', '2021-09-28 12:04:19'),
(9, 3, 'L-Size 28', 500.00, 50, 'L-Length 28', 1, '2021-09-28 11:21:43', '2021-09-28 11:21:43'),
(10, 4, 'S-Size 26', 700.00, 70, 'Winter-Jacket-01', 1, '2021-09-28 11:28:22', '2021-09-28 11:28:22'),
(11, 4, 'M-Size 28', 1000.00, 20, 'Winter-Jacket-02', 1, '2021-09-28 11:28:22', '2021-09-28 11:28:22'),
(12, 4, 'L-Size 30', 1500.00, 50, 'Winter-Jacket-03', 1, '2021-09-28 11:28:22', '2021-09-28 11:28:22'),
(13, 5, 'S-Size 26', 700.00, 50, 'Puffer-01', 1, '2021-09-28 11:33:48', '2021-09-28 11:33:48'),
(14, 5, 'M-Size 28', 1000.00, 50, 'Puffer-02', 1, '2021-09-28 11:33:48', '2021-09-28 11:33:48'),
(15, 5, 'L-Size 30', 1500.00, 70, 'Puffer-03', 1, '2021-09-28 11:33:49', '2021-09-28 11:33:49'),
(16, 9, 'S-Size 26', 600.00, 89, 'sku-02', 1, '2021-09-29 22:27:09', '2021-09-29 22:27:51'),
(17, 10, 'S-Size 32', 500.00, 64, 'sk-02', 1, '2021-09-29 22:39:51', '2021-09-29 22:40:28'),
(18, 6, 'S-Size 26', 560.00, 40, 'Hooded-01', 1, '2021-09-30 07:41:56', '2021-09-30 07:41:56'),
(19, 6, 'M-Size 28', 600.00, 60, 'Hooded-02', 1, '2021-09-30 07:41:56', '2021-09-30 07:41:56'),
(20, 6, 'L-Size 30', 800.00, 70, 'Hooded-03', 1, '2021-09-30 07:41:56', '2021-09-30 07:41:56'),
(21, 11, 'S-Size 28', 500.00, 69, 'jeans_jacket-02', 1, '2021-09-30 07:55:18', '2021-09-30 11:00:13'),
(22, 11, 'M-Size 32', 700.00, 70, 'jeans_jacket-03', 1, '2021-09-30 07:55:18', '2021-09-30 07:55:18'),
(23, 11, 'L-Size 33', 850.00, 70, 'jeans_jacket-04', 1, '2021-09-30 07:55:18', '2021-09-30 07:55:18'),
(24, 12, 'S-Size 30', 500.00, 50, 'china_jacket-353', 1, '2021-10-02 08:16:08', '2021-10-09 07:38:10'),
(25, 16, 'S-Size 30/32', 800.00, 50, 'Trench_Coat_02', 1, '2021-10-02 08:29:44', '2021-10-02 08:33:48'),
(26, 16, 'M-Size 33/35', 1500.00, 49, 'Trench_Coat_03', 1, '2021-10-02 08:29:44', '2021-10-09 07:18:35'),
(27, 16, 'L-Size 36/38', 1300.00, 50, 'Trench_Coat_04', 1, '2021-10-02 08:29:44', '2021-10-02 08:33:48'),
(28, 17, 'S-size 33', 800.00, 50, 'switer_00', 1, '2021-10-02 08:45:29', '2021-10-02 08:45:29'),
(29, 17, 'M-size 35', 1000.00, 50, 'switer_000', 1, '2021-10-02 08:45:29', '2021-10-02 08:45:29'),
(30, 17, 'L-size 38', 1200.00, 60, 'switer_0000', 1, '2021-10-02 08:46:10', '2021-10-02 08:46:10'),
(31, 18, 'S-size-30', 500.00, 500, 'royal_blue-11', 1, '2021-10-02 23:04:15', '2021-10-02 23:04:15'),
(32, 18, 'M-size-32', 600.00, 600, 'royal_blue-22', 1, '2021-10-02 23:04:15', '2021-10-02 23:04:15'),
(33, 18, 'L-size-34', 700.00, 700, 'royal_blue-33', 1, '2021-10-02 23:04:15', '2021-10-02 23:04:15'),
(34, 19, 'S-size 30', 500.00, 90, 't-shirt-red00', 1, '2021-10-02 23:13:16', '2021-10-02 23:13:16'),
(35, 19, 'M-size 32', 550.00, 90, 't-shirt-red01', 1, '2021-10-02 23:13:16', '2021-10-02 23:13:16'),
(36, 19, 'L-size 34', 650.00, 90, 't-shirt-red02', 1, '2021-10-02 23:13:16', '2021-10-02 23:13:16'),
(37, 20, 'S-size-30/31', 500.00, 64, 'woolen-coat-12', 1, '2021-10-02 23:26:40', '2021-10-09 08:45:25'),
(38, 20, 'M-size-32/32', 600.00, 67, 'woolen-coat-13', 1, '2021-10-02 23:26:40', '2021-10-02 23:26:40'),
(39, 20, 'L-size-33/34', 700.00, 67, 'woolen-coat-14', 1, '2021-10-02 23:26:41', '2021-10-02 23:26:41'),
(40, 21, 'S-size 28', 500.00, 60, 'floral-01', 1, '2021-10-03 07:59:53', '2021-10-03 07:59:53'),
(41, 21, 'M-size 28', 600.00, 78, 'floral-02', 1, '2021-10-03 07:59:53', '2021-10-09 21:19:24'),
(42, 21, 'L-size 32', 700.00, 90, 'floral-03', 1, '2021-10-03 07:59:53', '2021-10-03 07:59:53'),
(43, 22, 'S-size 30', 500.00, 78, 'soft-02', 1, '2021-10-03 08:14:17', '2021-10-03 08:14:17'),
(44, 22, 'M-size 32', 600.00, 78, 'soft-03', 1, '2021-10-03 08:14:17', '2021-10-03 08:14:17'),
(45, 22, 'L-size 35', 700.00, 78, 'soft-04', 1, '2021-10-03 08:14:17', '2021-10-03 08:14:17'),
(46, 23, 'S-size 30', 750.00, 76, 'Anorak-01', 1, '2021-10-03 08:33:45', '2021-10-05 21:33:46'),
(47, 23, 'M-size 34', 850.00, 78, 'Anorak-02', 1, '2021-10-03 08:33:45', '2021-10-03 08:33:45'),
(48, 23, 'L-size 38', 950.00, 78, 'Anorak-03', 1, '2021-10-03 08:33:45', '2021-10-03 08:33:45'),
(49, 24, 'S-size 34', 500.00, 78, 'pink-023', 1, '2021-10-03 09:15:37', '2021-10-03 09:15:37'),
(50, 24, 'M-size 38', 700.00, 78, 'pink-024', 1, '2021-10-03 09:15:37', '2021-10-03 09:15:37'),
(51, 24, 'L-size 40', 800.00, 78, 'pink-025', 1, '2021-10-03 09:15:37', '2021-10-03 09:15:37'),
(52, 12, 'M-Size 32', 600.00, 70, 'china_jacket-354', 1, '2021-10-09 07:37:59', '2021-10-09 07:38:10'),
(53, 12, 'L-Size 34', 700.00, 80, 'china_jacket-355', 1, '2021-10-09 07:37:59', '2021-10-09 07:38:10'),
(54, 26, 'S-Size 33', 500.00, 90, 'fabric-0212', 1, '2021-10-10 02:57:13', '2021-10-10 02:57:13'),
(55, 26, 'M-Size 35', 700.00, 90, 'fabric-0213', 1, '2021-10-10 02:57:13', '2021-10-10 02:57:13'),
(56, 26, 'L-Size 38', 900.00, 90, 'fabric-0214', 1, '2021-10-10 02:57:13', '2021-10-10 02:57:13'),
(57, 27, 'S-size 30', 550.00, 90, 'pakra-0351', 1, '2021-10-10 03:06:10', '2021-10-10 03:06:10'),
(58, 27, 'M-size 30', 650.00, 90, 'pakra-0361', 1, '2021-10-10 03:06:10', '2021-10-10 03:06:10'),
(59, 27, 'L-size 30', 750.00, 90, 'pakra-0371', 1, '2021-10-10 03:06:10', '2021-10-10 03:06:10'),
(60, 28, 'Age 1 Year', 500.00, 87, 'kids-02', 1, '2021-10-10 03:14:17', '2021-10-10 03:18:01'),
(61, 28, 'Age 1.5 Year', 600.00, 89, 'kids-03', 1, '2021-10-10 03:14:17', '2021-10-10 03:14:17'),
(62, 28, 'Age 2 Year', 700.00, 89, 'kids-04', 1, '2021-10-10 03:14:17', '2021-10-10 03:14:17');

-- --------------------------------------------------------

--
-- Table structure for table `products_images`
--

CREATE TABLE `products_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products_images`
--

INSERT INTO `products_images` (`id`, `product_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(5, 1, '214341632808716.jpg', 1, '2021-09-28 09:58:36', '2021-09-28 09:58:36'),
(6, 1, '944851632808787.jpg', 1, '2021-09-28 09:59:47', '2021-09-28 09:59:47'),
(7, 1, '723941632808962.jpg', 1, '2021-09-28 10:02:43', '2021-09-28 10:02:43'),
(8, 2, '404731632809505.jpg', 0, '2021-09-28 10:11:46', '2021-10-02 08:17:29'),
(9, 3, '376191632813300.jpg', 1, '2021-09-28 11:15:00', '2021-09-28 11:15:00'),
(10, 3, '12261632813509.jpg', 1, '2021-09-28 11:18:30', '2021-09-28 11:18:30'),
(11, 4, '943441632814015.jpg', 1, '2021-09-28 11:26:55', '2021-09-28 11:26:55'),
(12, 6, '211841632814770.jpg', 1, '2021-09-28 11:39:30', '2021-09-28 11:39:30'),
(13, 9, '404431632975405.jpg', 1, '2021-09-29 22:16:46', '2021-09-29 22:16:46'),
(14, 5, '791631633009172.jpg', 1, '2021-09-30 07:39:32', '2021-09-30 07:39:32'),
(15, 11, '350431633010019.jpg', 1, '2021-09-30 07:53:39', '2021-09-30 07:53:39'),
(16, 11, '273071633010020.jpeg', 1, '2021-09-30 07:53:40', '2021-09-30 07:53:40'),
(18, 2, '703131633184223.jpg', 1, '2021-10-02 08:17:04', '2021-10-02 08:17:04'),
(19, 2, '399891633184243.jpg', 1, '2021-10-02 08:17:23', '2021-10-02 08:17:23'),
(20, 4, '131051633184311.jpg', 1, '2021-10-02 08:18:31', '2021-10-02 08:18:31'),
(21, 5, '420011633184357.jpg', 1, '2021-10-02 08:19:17', '2021-10-02 08:19:17'),
(22, 12, '20191633184399.jpg', 1, '2021-10-02 08:19:59', '2021-10-02 08:19:59'),
(23, 16, '74171633184833.jpg', 1, '2021-10-02 08:27:14', '2021-10-02 08:27:14'),
(24, 17, '821571633185813.jpg', 1, '2021-10-02 08:43:34', '2021-10-02 08:43:34'),
(25, 18, '61291633237358.jpg', 1, '2021-10-02 23:02:38', '2021-10-02 23:02:38'),
(26, 19, '53171633237910.jpg', 1, '2021-10-02 23:11:50', '2021-10-02 23:11:50'),
(27, 20, '609011633238690.jpg', 1, '2021-10-02 23:24:50', '2021-10-02 23:24:50'),
(28, 21, '964761633269467.jpg', 1, '2021-10-03 07:57:47', '2021-10-03 07:57:47'),
(29, 22, '883511633270390.jpg', 1, '2021-10-03 08:13:10', '2021-10-03 08:13:10'),
(30, 23, '450821633271530.jpg', 1, '2021-10-03 08:32:11', '2021-10-03 08:32:11'),
(31, 24, '729461633274069.jpg', 1, '2021-10-03 09:14:29', '2021-10-03 09:14:29'),
(32, 26, '836941633856077.png', 1, '2021-10-10 02:54:37', '2021-10-10 02:54:37'),
(33, 27, '676631633856697.jpg', 1, '2021-10-10 03:04:57', '2021-10-10 03:04:57'),
(34, 28, '745021633857149.jpg', 1, '2021-10-10 03:12:29', '2021-10-10 03:12:29');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `user_id`, `product_id`, `review`, `rating`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'very very Good', 5, 1, NULL, NULL),
(2, 2, 2, 'very very Good!Recommend', 5, 1, NULL, NULL),
(3, 1, 1, 'very very Good!Recommend', 5, 1, NULL, NULL),
(4, 3, 1, 'very very Good!Recommend', 5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `refund_orders`
--

CREATE TABLE `refund_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `refund_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `refund_orders`
--

INSERT INTO `refund_orders` (`id`, `order_id`, `refund_method`, `number`, `created_at`, `updated_at`) VALUES
(11, 44, 'Paypal', '124314236', '2021-10-09 08:49:25', '2021-10-09 08:49:25'),
(10, 37, 'Paypal', '6789242349234', '2021-10-09 07:21:05', '2021-10-09 07:21:05'),
(9, 36, 'Rocket', '01679091279', '2021-10-09 07:20:06', '2021-10-09 07:20:06');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Men', 1, '2021-09-01 14:58:46', NULL),
(2, 'Women', 1, '2021-09-21 14:58:51', NULL),
(3, 'Kids', 1, '2021-09-07 14:58:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_charges`
--

CREATE TABLE `shipping_charges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `0_500g` double(8,2) NOT NULL,
  `501_1000g` double(8,2) NOT NULL,
  `1001_2000g` double(8,2) NOT NULL,
  `2001_5000g` double(8,2) NOT NULL,
  `above_5000g` double(8,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_charges`
--

INSERT INTO `shipping_charges` (`id`, `country`, `0_500g`, `501_1000g`, `1001_2000g`, `2001_5000g`, `above_5000g`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, '2021-10-09 21:10:34'),
(2, 'Åland Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(3, 'Albania', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(4, 'Algeria', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(5, 'American Samoa', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(6, 'Andorra', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(7, 'Angola', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(8, 'Anguilla', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(9, 'Antarctica', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(10, 'Antigua and Barbuda', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(11, 'Argentina', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(12, 'Armenia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(13, 'Aruba', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(14, 'Australia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(15, 'Austria', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(16, 'Azerbaijan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(17, 'Bahamas', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(18, 'Bahrain', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(19, 'Bangladesh', 40.00, 50.00, 60.00, 70.00, 80.00, 1, NULL, '2021-10-09 21:11:45'),
(20, 'Barbados', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(21, 'Belarus', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(22, 'Belgium', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(23, 'Belize', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(24, 'Benin', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(25, 'Bermuda', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(26, 'Bhutan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(27, 'Bolivia, Plurinational State of', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(28, 'Bonaire, Sint Eustatius and Saba', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(29, 'Bosnia and Herzegovina', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(30, 'Botswana', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(31, 'Bouvet Island', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(32, 'Brazil', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(33, 'British Indian Ocean Territory', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(34, 'Brunei Darussalam', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(35, 'Bulgaria', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(36, 'Burkina Faso', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(37, 'Burundi', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(38, 'Cambodia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(39, 'Cameroon', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(40, 'Canada', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(41, 'Cape Verde', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(42, 'Cayman Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(43, 'Central African Republic', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(44, 'Chad', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(45, 'Chile', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(46, 'China', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(47, 'Christmas Island', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(48, 'Cocos (Keeling) Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(49, 'Colombia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(50, 'Comoros', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(51, 'Congo', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(52, 'Congothe Democratic Republic of the', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(53, 'Cook Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(54, 'Costa Rica', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(55, 'Cote', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(56, 'Croatia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(57, 'Cuba', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(58, 'Curaçao', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(59, 'Cyprus', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(60, 'Czech Republic', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(61, 'Denmark', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(62, 'Djibouti', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(63, 'Dominica', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(64, 'Dominican Republic', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(65, 'Ecuador', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(66, 'Egypt', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(67, 'El Salvador', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(68, 'Equatorial Guinea', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(69, 'Eritrea', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(70, 'Estonia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(71, 'Ethiopia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(72, 'Falkland Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(73, 'Faroe Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(74, 'Fiji', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(75, 'Finland', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(76, 'France', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(77, 'French Guiana', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(78, 'French Polynesia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(79, 'French Southern Territories', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(80, 'Gabon', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(81, 'Gambia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(82, 'Georgia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(83, 'Germany', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(84, 'Ghana', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(85, 'Gibraltar', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(86, 'Greece', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(87, 'Greenland', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(88, 'Grenada', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(89, 'Guadeloupe', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(90, 'Guam', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(91, 'Guatemala', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(92, 'Guernsey', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(93, 'Guinea', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(94, 'Guinea-Bissau', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(95, 'Guyana', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(96, 'Haiti', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(97, 'Heard Island and McDonald Mcdonald Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(98, 'Holy See', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(99, 'Honduras', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(100, 'Hong Kong', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(101, 'Hungary', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(102, 'Iceland', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(103, 'India', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(104, 'Indonesia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(105, 'Iran', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(106, 'Iraq', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(107, 'Ireland', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(108, 'Isle of Man', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(109, 'Israel', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(110, 'Italy', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(111, 'Jamaica', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(112, 'Japan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(113, 'Jersey', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(114, 'Jordan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(115, 'Kazakhstan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(116, 'Kenya', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(117, 'Kiribati', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(118, 'Korea', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(119, 'Korea', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(120, 'Kuwait', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(121, 'Kyrgyzstan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(122, 'Lao', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(123, 'Latvia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(124, 'Lebanon', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(125, 'Lesotho', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(126, 'Liberia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(127, 'Libya', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(128, 'Liechtenstein', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(129, 'Lithuania', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(130, 'Luxembourg', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(131, 'Macao', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(132, 'Macedonia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(133, 'Madagascar', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(134, 'Malawi', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(135, 'Malaysia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(136, 'Maldives', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(137, 'Mali', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(138, 'Malta', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(139, 'Marshall Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(140, 'Martinique', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(141, 'Mauritania', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(142, 'Mauritius', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(143, 'Mayotte', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(144, 'Mexico', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(145, 'Micronesia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(146, 'Moldova', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(147, 'Monaco', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(148, 'Mongolia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(149, 'Montenegro', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(150, 'Montserrat', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(151, 'Morocco', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(152, 'Mozambique', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(153, 'Myanmar', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(154, 'Namibia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(155, 'Nauru', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(156, 'Nepal', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(157, 'Netherlands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(158, 'New Caledonia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(159, 'New Zealand', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(160, 'Nicaragua', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(161, 'Niger', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(162, 'Nigeria', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(163, 'Niue', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(164, 'Norfolk Island', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(165, 'Northern Mariana Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(166, 'Norway', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(167, 'Oman', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(168, 'Pakistan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(169, 'Palau', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(170, 'Palestine', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(171, 'Panama', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(172, 'Papua New Guinea', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(173, 'Paraguay', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(174, 'Peru', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(175, 'Philippines', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(176, 'Pitcairn', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(177, 'Poland', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(178, 'Portugal', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(179, 'Puerto Rico', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(180, 'Qatar', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(181, 'Réunion', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(182, 'Romania', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(183, 'Russian Federation', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(184, 'Rwanda', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(185, 'Saint Barthélemy', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(186, 'Saint Helena', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(187, 'Saint Kitts and Nevis', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(188, 'Saint Lucia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(189, 'Saint Martin', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(190, 'Saint Pierre and Miquelon', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(191, 'Saint Vincent and the Grenadines', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(192, 'Samoa', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(193, 'San Marino', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(194, 'Sao Tome and Principe', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(195, 'Saudi Arabia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(196, 'Senegal', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(197, 'Serbia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(198, 'Seychelles', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(199, 'Sierra Leone', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(200, 'Singapore', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(201, 'Sint Maarten', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(202, 'Slovakia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(203, 'Slovenia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(204, 'Solomon Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(205, 'Somalia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(206, 'South Africa', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(207, 'South Georgia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(208, 'South Sudan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(209, 'Spain', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(210, 'Sri Lanka', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(211, 'Sudan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(212, 'Suriname', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(213, 'Svalbard and Jan Mayen', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(214, 'Swaziland', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(215, 'Sweden', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(216, 'Switzerland', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(217, 'Syrian Arab Republic', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(218, 'Taiwan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(219, 'Tajikistan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(220, 'Tanzania, United Republic of', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(221, 'Thailand', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(222, 'Timor-Leste', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(223, 'Togo', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(224, 'Tokelau', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(225, 'Tonga', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(226, 'Trinidad and Tobago', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(227, 'Tunisia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(228, 'Turkey', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(229, 'Turkmenistan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(230, 'Turks and Caicos Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(231, 'Tuvalu', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(232, 'Uganda', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(233, 'Ukraine', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(234, 'United Arab Emirates', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(235, 'United Kingdom', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(236, 'United States', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(237, 'United States Minor Outlying Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(238, 'Uruguay', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(239, 'Uzbekistan', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(240, 'Vanuatu', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(241, 'Venezuela', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(242, 'Viet Nam', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(243, 'Virgin Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(244, 'Virgin Islands', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(245, 'Wallis and Futuna', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(246, 'Western Sahara', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(247, 'Yemen', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(248, 'Zambia', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(249, 'Zimbabwe', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL),
(250, 'USA', 500.00, 1000.00, 1500.00, 2000.00, 2500.00, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ssl_comerze_histories`
--

CREATE TABLE `ssl_comerze_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ssl_comerze_histories`
--

INSERT INTO `ssl_comerze_histories` (`id`, `order_id`, `name`, `email`, `phone`, `amount`, `address`, `status`, `transaction_id`, `currency`, `created_at`, `updated_at`) VALUES
(11, 45, 'Tareq Ahmed', 'tareq@gmail.com', '01679091279', '580', 'Dhaka', 'Processing', '6161ac9d968ef', 'BDT', NULL, NULL),
(10, 39, 'Tareq Ahmed', 'tareq@gmail.com', '01679091279', '580', 'Dhaka 1200', 'Processing', '616197ca5c8e0', 'BDT', NULL, NULL),
(9, 38, 'Tareq Ahmed', 'tareq@gmail.com', '01679091279', '580', 'Dhaka 1200', 'Processing', '616197692d112', 'BDT', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pincode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `address`, `city`, `state`, `country`, `pincode`, `mobile`, `email`, `email_verified_at`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Tareq Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01679091279', 'tareq@gmail.com', '0000-00-00 00:00:00', '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', 1, '', '2021-09-27 18:10:29', NULL),
(2, 'Amit saha', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01000000000', 'amit@gmail.com', '0000-00-00 00:00:00', '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', 1, '', '2021-08-24 18:10:34', NULL),
(3, 'Bayzid Ahmed', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '02345678901', 'bayzid@gmail.com', '0000-00-00 00:00:00', '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', 1, '', '2021-09-23 18:10:39', NULL),
(4, 'samad sakib', 'Dhaka', 'Dhaka', 'Dhaka', 'Bangladesh', '1000', '01789012345', 'sakib@gmail.com', NULL, '$2y$10$onY1FphXQW7tFfppGrsyHOYTB0Xk.Gjpbk8OuGE.YT81qzWNenEBO', 1, NULL, '2021-10-09 07:28:07', '2021-10-09 07:29:07');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `admins_roles`
--
ALTER TABLE `admins_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cod_pincodes`
--
ALTER TABLE `cod_pincodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_logs`
--
ALTER TABLE `orders_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_products`
--
ALTER TABLE `orders_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_settings`
--
ALTER TABLE `other_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `prepaid_pincodes`
--
ALTER TABLE `prepaid_pincodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_attributes`
--
ALTER TABLE `products_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refund_orders`
--
ALTER TABLE `refund_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_charges`
--
ALTER TABLE `shipping_charges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ssl_comerze_histories`
--
ALTER TABLE `ssl_comerze_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `admins_roles`
--
ALTER TABLE `admins_roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cod_pincodes`
--
ALTER TABLE `cod_pincodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `delivery_addresses`
--
ALTER TABLE `delivery_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `orders_logs`
--
ALTER TABLE `orders_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `orders_products`
--
ALTER TABLE `orders_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `other_settings`
--
ALTER TABLE `other_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `prepaid_pincodes`
--
ALTER TABLE `prepaid_pincodes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `products_attributes`
--
ALTER TABLE `products_attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `refund_orders`
--
ALTER TABLE `refund_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `shipping_charges`
--
ALTER TABLE `shipping_charges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `ssl_comerze_histories`
--
ALTER TABLE `ssl_comerze_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
