<div id="footerSection">
    <div class="container">
        <div class="row" style="margin-left: 40px">
            <div class="span3">
                <h5>ACCOUNT</h5>
                <a href="{{url('/account')}}">YOUR ACCOUNT</a>
                <a href="{{url('/account')}}">PERSONAL INFORMATION</a>
                <a href="{{url('/orders')}}">ORDER HISTORY</a>
            </div>
            <div class="span3">
                <h5>INFORMATION</h5>
                <a href="{{url('about-us')}}">About Us</a>
                <a href="{{url('privacy-policy')}}">Privacy Policy</a>
                <a href="{{url('contact')}}">CONTACT US</a>
            </div>
            <div class="span3">
                <h5>OUR OFFERS</h5>
                <a href="#">NEW PRODUCTS</a>
                <a href="#">TOP SELLERS</a>
                <a href="#">SPECIAL OFFERS</a>
            </div>
            <div id="socialMedia" class="span3 pull-right" style="float: left;">
                <h5>SOCIAL MEDIA </h5>
                <a href="#"><img width="60" height="60"
                                 src="{{asset('images/front_images/facebook.png')}}"
                                 title="facebook" alt="facebook"/></a>
                <a href="#"><img width="60" height="60" src="{{asset('images/front_images/twitter.png')}}"
                                 title="twitter" alt="twitter"/></a>
                <a href="#"><img width="60" height="60"
                                 src="{{asset('images/front_images/youtube.png')}}" title="youtube"
                                 alt="youtube"/></a>
            </div>
        </div>

    </div><!-- Container End -->
</div>


<!--Start of Tawk.to livechat Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/61592f05d326717cb68473eb/1fh25vc5g';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to livechat Script End -->
