@extends('layouts.admin_layout.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Settings</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Update Admin/Sub Admin Details</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Admin/Sub Admin Details</h3>
                            </div>
                            <!-- /.card-header -->


                            @if (Session::has('success_message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert"
                                    style="margin-top:10px;">
                                    <strong>Success!</strong> {{ Session::get('success_message') }} .
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- form start -->
                            <form role="form" action="{{ url('/admin/update-admin-details') }}" method="post"
                                name="updateAdminDetails" id="updateAdminDetails" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Admin Email address</label>
                                        <input class="form-control" value="{{ Auth::guard('admin')->user()->email }}"
                                            readonly="">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Admin Type</label>
                                        <input class="form-control" value="{{ Auth::guard('admin')->user()->type }}"
                                            readonly="">
                                    </div>

                                    <div class="form-group">
                                        <label for="admin_name">Name</label>
                                        <input type="text" class="form-control" name="admin_name" id="admin_name"
                                            value="{{ Auth::guard('admin')->user()->name }}"
                                            placeholder="Enter Admin/sub Admin Name" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="admin_mobile">Mobile</label>
                                        <input type="text" class="form-control" name="admin_mobile" id="admin_mobile"
                                            value="{{ Auth::guard('admin')->user()->mobile }}" placeholder="Enter Your Mobile"
                                            required>
                                    </div>

                                    <div class="form-group">
                                        <label for="admin_image">Admin Image</label>
                                        <input type="file" class="form-control" id="admin_image" name="admin_image"
                                            accept="image/*">
                                        @if (!empty(Auth::guard('admin')->user()->image))
                                            <a target="_blank"
                                                href="{{ url('images/admin_images/admin_photos/' . Auth::guard('admin')->user()->image) }}">View
                                                Image</a>
                                            <input type="hidden" class="form-control" id="current_admin_image"
                                                name="current_admin_image" value="{{ Auth::guard('admin')->user()->image }}">
                                        @endif
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

        <!-- /.content-wrapper -->
    </div>
    <!-- /.content-wrapper -->
@endsection
