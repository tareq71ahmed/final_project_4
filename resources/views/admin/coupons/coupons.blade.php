@extends('layouts.admin_layout.admin_layout')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Coupons</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @if (Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                             style="margin-top:10px;">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Coupons</h3>
                            <a href="{{ url('admin/add-edit-coupon') }}" class="btn btn-primary float-right">Add
                                Coupons</a>
                        </div>
                        <div class="card-body">
                            <table id="coupons" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>
                                    <th>Coupon Type</th>
                                    <th>Amount</th>
                                    <th>Expiry date</th>
                                    <th>status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($coupons as $coupon)
                                    <tr>
                                        <td>{{$coupon['id']}}</td>
                                        <td>{{$coupon['coupon_code']}}</td>
                                        <td>{{$coupon['coupon_type']}}</td>
                                        <td>
                                            {{$coupon['amount']}}
                                            @if($coupon['amount_type']=="Percentage")
                                                %
                                            @else
                                                BDT
                                            @endif
                                        </td>
                                        <td>{{$coupon['expiry_date']}}</td>

                                        <td>
                                            @if($couponModule['edit_access']==1 || $couponModule['full_access']==1)
                                            @if ($coupon['status'] == '1')
                                                <a class="updateCouponStatus" id="coupon-{{ $coupon['id'] }}"
                                                   coupon_id="{{ $coupon['id'] }}" href="javascript:void(0)"><i
                                                        title="Active" class="fas fa-toggle-on" aria-hidden="true"
                                                        status="Active"></i></a>
                                            @else
                                                <a class="updateCouponStatus" id="coupon-{{ $coupon['id'] }}"
                                                   coupon_id="{{ $coupon['id'] }}" href="javascript:void(0)"><i
                                                        title="Inactive" style="color:grey" class="fas fa-toggle-off"
                                                        aria-hidden="true" status="Inactive"></i></a>
                                            @endif
                                            @endif

                                        </td>
                                        <td>
                                            @if($couponModule['edit_access']==1 || $couponModule['full_access']==1)
                                            <a title="Edit Coupon"
                                               href="{{ url('admin/add-edit-coupon/' . $coupon['id']) }}">&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-edit"></i></a>&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            @endif
                                            @if($couponModule['full_access']==1)
                                            <a title="Delete Coupon" href="javascript:void(0)" class="confirmDelete"
                                               record="coupon" recordid="{{ $coupon['id'] }}"><i
                                                    class="fas fa-trash"></i></a>&nbsp;&nbsp;
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
