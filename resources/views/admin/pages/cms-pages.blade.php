
@extends('layouts.admin_layout.admin_layout')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">CMS Pages</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @if (Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                             style="margin-top:10px;">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">CMS Pages</h3>
                            <a href="{{ url('admin/add-edit-cms-page') }}" class="btn btn-primary float-right">Add
                                CMS Pages</a>
                        </div>
                        <div class="card-body">
                            <table id="cmspages" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Url</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($cms_pages as $page)
                                    <tr>
                                        <td>{{ $page->id }}</td>
                                        <td>{{ $page->title }}</td>
                                        <td>{{ $page->url }}</td>
                                        <td>
                                            @if ($page->status == '1')
                                                <a class="updateCmsStatus" id="page-{{ $page->id }}"
                                                   page_id="{{ $page->id }}" href="javascript:void(0)"><i
                                                        title="Active" class="fas fa-toggle-on" aria-hidden="true"
                                                        status="Active"></i></a>
                                            @else
                                                <a class="updateCmsStatus" id="page-{{ $page->id }}"
                                                   page_id="{{ $page->id }}" href="javascript:void(0)"><i
                                                        title="Inactive" style="color:grey" class="fas fa-toggle-off"
                                                        aria-hidden="true" status="Inactive"></i></a>
                                            @endif
                                        </td>

                                        <td>{{ $page->created_at }}</td>
                                        <td>
                                            <a title="Edit Cms Page"
                                               href="{{ url('admin/add-edit-cms-page/' . $page->id) }}">&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-edit"></i></a>&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a title="Delete Cms Page" href="javascript:void(0)" class="confirmDelete"
                                               record="page" recordid="{{ $page->id }}"><i
                                                    class="fas fa-trash"></i></a>&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
