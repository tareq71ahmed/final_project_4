@extends('layouts.admin_layout.admin_layout')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Products Attribute</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top:10px;">
                        <strong>Success!</strong> {{ Session::get('success_message') }} .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if (Session::has('error_message'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top:10px;">
                        <strong>Opps!</strong> {{ Session::get('error_message') }} .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form name="attributeForm" id="attributeForm" action="{{ url('admin/add-attributes/' . $productdata['id']) }}"
                    method="POST">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $productdata['id'] }}">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-remove"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="product_name">product Name :
                                        </label>&nbsp;{{ $productdata['product_name'] }}
                                    </div>
                                    <div class="form-group">
                                        <label for="product_code">product Code :
                                        </label>&nbsp;{{ $productdata['product_code'] }}
                                    </div>
                                    <div class="form-group">
                                        <label for="product_color">product Color :
                                        </label>&nbsp;{{ $productdata['product_color'] }}
                                    </div>
                                    <div class="form-group">
                                        <label for="product_color">product Price :
                                        </label>&nbspBDT.{{ $productdata['product_price'] }}
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php $product_image_path = 'images/product_images/small/' .
                                        $productdata->main_image; ?>
                                        @if (!empty($productdata['main_image']) && file_exists($product_image_path))
                                            <div><img style="width:120px"
                                                    src="{{ asset('images/product_images/small/' . $productdata['main_image']) }}">
                                                &nbsp;
                                            </div>
                                        @else
                                            <div><img style="width:120px"
                                                    src="{{ asset('images/product_images/small/no_image.png') }}">
                                                &nbsp;
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="field_wrapper">
                                            <div>
                                                <input id="size" name="size[]" type="text" value="" placeholder="Size"
                                                    style="width:120px" required />
                                                <input id="sku" name="sku[]" type="text" value="" placeholder="SKU"
                                                    style="width:120px" required />
                                                <input id="price" name="price[]" type="number" value="" placeholder="Price"
                                                    style="width:120px" min='1' required />
                                                <input id="stock" name="stock[]" type="number" value="" placeholder="Stock"
                                                    style="width:120px" min='1' required />
                                                <a href="javascript:void(0);" class="add_button" title="Add field">Add</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">Add Attributes</button>
                        </div>
                    </div>

                </form>


                <form name="editAttributeForm" id="editAttributeForm"
                    action="{{ url('admin/edit-attributes/' . $productdata['id']) }}" method="POST">@csrf
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Products</h3>
                        </div>
                        <div class="card-body">
                            <table id="attributes" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>size</th>
                                        <th>Sku</th>
                                        <th>Price</th>
                                        <th>Stock</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($productdata['attributes'] as $attribute)
                                        <input style="display:none" name="attrId[]" value="{{ $attribute['id'] }}">
                                        <tr>
                                            <td>{{ $attribute['id'] }}</td>
                                            <td>{{ $attribute['size'] }}</td>
                                            <td>{{ $attribute['sku'] }}</td>

                                            <td>
                                                <input min="1" type="number" name="price[]" value="{{ $attribute['price'] }}"
                                                    required>
                                            </td>
                                            <td>
                                                <input min="0" type="number" name="stock[]" value="{{ $attribute['stock'] }}"
                                                    required>
                                            </td>
                                            <td>

                                                @if ($attribute['status'] == '1')
                                                    <a class="updateAttributeStatus" id="attribute-{{ $attribute['id'] }}"
                                                        attribute_id="{{ $attribute['id'] }}" href="javascript:void(0)"><i
                                                            title="Active" class="fas fa-toggle-on" aria-hidden="true"
                                                            status="Active"></i></a>
                                                @else
                                                    <a class="updateAttributeStatus" id="attribute-{{ $attribute['id'] }}"
                                                        attribute_id="{{ $attribute['id'] }}" href="javascript:void(0)"><i
                                                            title="Inactive" style="color:grey" class="fas fa-toggle-off"
                                                            aria-hidden="true" status="Inactive"></i></a>
                                                @endif
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a title="Delete Attribute" href="javascript:void(0)" class="confirmDelete"
                                                    record="attribute" recordid="{{ $attribute['id'] }}"><i
                                                        class="fas fa-trash"></i></a>

                                            </td>

                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">Update Attributes</button>
                        </div>
                    </div>
                </form>

            </div>
        </section>
    </div>
@endsection
