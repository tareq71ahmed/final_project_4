@extends('layouts.admin_layout.admin_layout')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Add Image</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top:10px;">
                        <strong>Success!</strong> {{ Session::get('success_message') }} .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if (Session::has('error_message'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="margin-top:10px;">
                        <strong>Opps!</strong> {{ Session::get('error_message') }} .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form name="ImageForm" id="ImageForm" action="{{ url('admin/add-images/' . $productdata['id']) }}"
                      method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $productdata['id'] }}">
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-remove"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="product_name">product Name :
                                        </label>&nbsp;{{ $productdata['product_name'] }}
                                    </div>
                                    <div class="form-group">
                                        <label for="product_code">product Code :
                                        </label>&nbsp;{{ $productdata['product_code'] }}
                                    </div>
                                    <div class="form-group">
                                        <label for="product_color">product Color :
                                        </label>&nbsp;{{ $productdata['product_color'] }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?php $product_image_path = 'images/product_images/small/' .
                                            $productdata['main_image']; ?>
                                        @if (!empty($productdata['main_image']) && file_exists($product_image_path))
                                            <img style="width:100px"
                                                 src="{{ asset('images/product_images/small/' . $productdata['main_image']) }}">
                                        @else
                                            <img style="width:100px"
                                                 src="{{ asset('images/product_images/small/no_image.png') }}">
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="field_wrapper">
                                            <div>
                                                <input multiple="" id="images" name="images[]" type="file" value=""
                                                       required
                                                       accept="image/*"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success">Add Image</button>
                        </div>
                    </div>
                </form>


                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Product Images</h3>
                    </div>
                    <div class="card-body">
                        <table id="Image" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($productdata['images'] as $image)
                                <input style="display:none" name="attrId[]" value="{{ $image['id'] }}">
                                <tr>
                                    <td>{{ $image['id'] }}</td>
                                    <td>
                                        <img style="width:100px"
                                             src="{{ asset('images/product_images/small/' . $image['image']) }}">
                                    </td>
                                    <td>
                                        @if ($image['status'] == '1')
                                            <a class="updateImageStatus" id="image-{{ $image['id'] }}"
                                               image_id="{{ $image['id'] }}" href="javascript:void(0)"><i title="Active"
                                                                                                          class="fas fa-toggle-on"
                                                                                                          aria-hidden="true"
                                                                                                          status="Active"></i></a>
                                        @else
                                            <a class="updateImageStatus" id="image-{{ $image['id'] }}"
                                               image_id="{{ $image['id'] }}" href="javascript:void(0)"><i
                                                    title="Inactive" style="color:grey" class="fas fa-toggle-off"
                                                    aria-hidden="true" status="Inactive"></i></a>
                                        @endif
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <a title="Delete Image" href="javascript:void(0)" class="confirmDelete"
                                           record="image" recordid="{{ $image['id'] }}"><i class="fas fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
