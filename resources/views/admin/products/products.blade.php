@extends('layouts.admin_layout.admin_layout')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Products</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">

                    @if (Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                             style="margin-top:10px;">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Products</h3>
                            <a href="{{ url('admin/add-edit-product') }}" class="btn btn-primary float-right">Add
                                Product</a>

                        </div>
                        <div class="card-body">
                            <table id="products" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Product Name</th>
                                    <th>Product Code</th>
                                    <th>Product Color</th>
                                    <th>Product Image</th>
                                    <th>Category</th>
                                    <th>Section</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>


                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->product_name }}</td>
                                        <td>{{ $product->product_code }}</td>
                                        <td>{{ $product->product_color }}</td>
                                        <td>
                                            <?php $product_image_path = 'images/product_images/small/' .
                                                $product->main_image; ?>
                                            @if (!empty($product->main_image) && file_exists($product_image_path))
                                                <img style="width:100px"
                                                     src="{{ asset('images/product_images/small/' . $product->main_image) }}">

                                            @else
                                                <img style="width:100px"
                                                     src="{{ asset('images/product_images/small/no_image.png') }}">
                                            @endif
                                        </td>
                                        <td>{{ $product->category->category_name }}</td>
                                        <td>{{ $product->section->name }}</td>
                                        <td>
                                            @if($productModule['edit_access']==1 || $productModule['full_access']==1)
                                            @if ($product->status == '1')
                                                <a class="updateProductStatus" id="product-{{ $product->id }}"
                                                   product_id="{{ $product->id }}" href="javascript:void(0)"><i
                                                        title="Active" class="fas fa-toggle-on" aria-hidden="true"
                                                        status="Active"></i></a>
                                            @else
                                                <a class="updateProductStatus" id="product-{{ $product->id }}"
                                                   product_id="{{ $product->id }}" href="javascript:void(0)"><i
                                                        title="Inactive" style="color:grey" class="fas fa-toggle-off"
                                                        aria-hidden="true" status="Inactive"></i></a>
                                            @endif
                                            @endif
                                        </td>
                                        <td>

                                            @if($productModule['edit_access']==1 || $productModule['full_access']==1)
                                            <a title="Add/Edit Attributes"
                                               href="{{ url('admin/add-attributes/' . $product->id) }}"><i
                                                    class="fas fa-plus"></i></a>

                                            <a title="Add Images"
                                               href="{{ url('admin/add-images/' . $product->id) }}"><i
                                                    class="fas fa-plus-circle"></i></a>

                                            <a title="Edit Product"
                                               href="{{ url('admin/add-edit-product/' . $product->id) }}"><i
                                                    class="fas fa-edit"></i></a>
                                            &nbsp;
                                            @endif
                                                @if( $productModule['full_access']==1)
                                            <a title="Delete Product" href="javascript:void(0)" class="confirmDelete"
                                               record="product" recordid="{{ $product->id }}"><i
                                                    class="fas fa-trash"></i></a>
                                             @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
