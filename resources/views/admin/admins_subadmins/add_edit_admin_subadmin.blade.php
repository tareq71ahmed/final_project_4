@extends('layouts.admin_layout.admin_layout')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">Home</a></li>
                            <li class="breadcrumb-item active">Admin/Sub-Admin</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert" style="margin-top:10px;">
                        <strong>Success!</strong> {{ Session::get('success_message') }} .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <form name="adminForm" id="adminForm"
                      @if (empty($admindata['id'])) action="{{ url('admin/add-edit-admin-subadmin') }}" @else
                      action="{{ url('admin/add-edit-admin-subadmin/' . $admindata['id']) }}" @endif method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="card card-default">
                        <div class="card-header">
                            <h3 class="card-title">{{ $title }}</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                        class="fas fa-minus"></i></button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                        class="fas fa-remove"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">


                                    <div class="form-group">
                                        <label for="admin_name">Admin Name</label>
                                        <input type="text" class="form-control" name="admin_name" id="admin_name"
                                               placeholder="Admin Name"
                                               @if (!empty($admindata['name'])) value="{{ $admindata['name'] }}"
                                               @else value="{{ old('name') }}" @endif>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin_mobile">Admin Mobile</label>
                                        <input type="text" class="form-control" name="admin_mobile" id="admin_mobile"
                                               placeholder="Admin Mobile"
                                               @if (!empty($admindata['mobile'])) value="{{ $admindata['mobile'] }}"
                                               @else value="{{ old('mobile') }}" @endif>
                                    </div>

                                    <div class="form-group">
                                        <label for="admin_image">Admin Image</label>
                                        <input type="file" class="form-control" id="admin_image" name="admin_image"
                                               accept="image/*">
                                        @if (!empty($admindata['image']))
                                            <a target="_blank"
                                               href="{{ url('images/admin_images/admin_photos/' . $admindata['image']) }}">View
                                                Image</a>
                                            <input type="hidden" class="form-control" id="current_admin_image"
                                                   name="current_admin_image"
                                                   value="{{ $admindata['image'] }}">
                                        @endif
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="admin_email">Admin Email</label>
                                        <input @if ($admindata['id']!="") disabled @else required @endif   type="email"
                                               class="form-control" name="admin_email" id="admin_email"
                                               placeholder="Admin Email"
                                               @if (!empty($admindata['email'])) value="{{ $admindata['email'] }}"
                                               @else value="{{ old('email') }}" @endif>
                                    </div>


                                    <div class="form-group">
                                        <label>Admin Type</label>
                                        <select @if ($admindata['id']!="") disabled @else required
                                                @endif  name="admin_type" id="admin_type" class="form-control select2"
                                                style="width: 100%;">
                                            <option disabled selected value="">Select</option>
                                            <option value="admin"
                                                    @if (!empty($admindata['type']) && $admindata['type'] == "admin") selected="" @endif>
                                                Admin
                                            </option>

                                            <option value="subadmin"
                                                    @if (!empty($admindata['type']) && $admindata['type'] == "subadmin") selected="" @endif>
                                                Sub-Admin
                                            </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="admin_password">Admin Password</label>
                                        <input type="password" class="form-control" name="admin_password"
                                               id="admin_password"
                                               placeholder="Admin Password">
                                    </div>
                                </div>


                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
