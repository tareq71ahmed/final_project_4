@extends('layouts.admin_layout.admin_layout')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Admins / Sub-Admins</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @if (Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                             style="margin-top:10px;">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                        @if (Session::has('error_message'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert"
                                 style="margin-top:10px;">
                                <strong>Error!</strong> {{ Session::get('error_message') }} .
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Admins / Sub-Admins</h3>
                            <a href="{{ url('admin/add-edit-admin-subadmin') }}" class="btn btn-primary float-right">Admins
                                / Sub-Admins</a>
                        </div>
                        <div class="card-body">
                            <table id="admins" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Type</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($admins_subadmins as $admin)
                                    <tr>
                                        <td>{{ $admin->id }}</td>
                                        <td>{{ $admin->name }}</td>
                                        <td>{{ $admin->email  }}</td>
                                        <td>{{ $admin->mobile }}</td>
                                        <td>{{ $admin->type }}</td>
                                        <td>
                                            @if($admin->type !='superadmin')
                                                @if ($admin->status == '1')
                                                    <a class="updateAdminStatus" id="admin-{{ $admin->id }}"
                                                       admin_id="{{ $admin->id }}" href="javascript:void(0)"><i
                                                            title="Active" class="fas fa-toggle-on" aria-hidden="true"
                                                            status="Active"></i></a>
                                                @else
                                                    <a class="updateAdminStatus" id="admin-{{ $admin->id }}"
                                                       admin_id="{{ $admin->id }}" href="javascript:void(0)"><i
                                                            title="Inactive" style="color:grey"
                                                            class="fas fa-toggle-off"
                                                            aria-hidden="true" status="Inactive"></i></a>
                                                @endif
                                            @endif
                                        </td>

                                        <td>
                                            @if($admin->type !='superadmin')
                                                <a title="Set Roles/Permission"
                                                   href="{{ url('admin/update-role/' . $admin->id) }}"><i
                                                        class="fas fa-unlock"></i></a>&nbsp;
                                                &nbsp;&nbsp;&nbsp;

                                                <a title="Edit Admin/Sub-Admin"
                                                   href="{{ url('admin/add-edit-admin-subadmin/' . $admin->id) }}"><i
                                                        class="fas fa-edit"></i></a>&nbsp;
                                                &nbsp;&nbsp;&nbsp;
                                                <a title="Delete Admin/Sub-Admin" href="javascript:void(0)"
                                                   class="confirmDelete"
                                                   record="admin" recordid="{{ $admin->id }}"><i
                                                        class="fas fa-trash"></i></a>
                                            @endif
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
