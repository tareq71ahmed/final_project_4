@extends('layouts.admin_layout.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Settings</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Update Admin Password</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Admin Password</h3>
                            </div>
                            <!-- /.card-header -->

                            @if (Session::has('error_message'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert"
                                    style="margin-top:10px;">
                                    <strong>Error!</strong> {{ Session::get('error_message') }} .
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (Session::has('success_message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert"
                                    style="margin-top:10px;">
                                    <strong>Success!</strong> {{ Session::get('success_message') }} .
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            <!-- form start -->
                            <form role="form" action="{{ url('/admin/update-current-pwd') }}" method="post"
                                name="updatePasswordForm" id="updatePasswordForm">
                                @csrf
                                <div class="card-body">

                                    <?php
                                    /*
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Admin/Sub Admin Name</label>
                                        <input type="text" id="admin_name" name="admin_name" class="form-control"
                                            value="{{ $adminDetails->name }}" placeholder="Enter Admin/Sub Admin Name">
                                    </div>
                                    */
                                    ?>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Admin Email address</label>
                                        <input class="form-control" value="{{ $adminDetails->email }}" readonly="">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Admin Type</label>
                                        <input class="form-control" value="{{ $adminDetails->type }}" readonly="">
                                    </div>

                                    <div class="form-group">
                                        <label for="current_pwd">Current Password</label>
                                        <input type="password" class="form-control" name="current_pwd" id="current_pwd"
                                            placeholder="Enter Current Password">
                                        <span id="chkCurrentPwd"></span>
                                    </div>

                                    <div class="form-group">
                                        <label for="new_pwd">New Password</label>
                                        <input type="password" class="form-control" name="new_pwd" id="new_pwd"
                                            placeholder="Enter New Password">
                                    </div>

                                    <div class="form-group">
                                        <label for="confirm_pwd">Confirm Password</label>
                                        <input type="password" class="form-control" id="confirm_pwd" name="confirm_pwd"
                                            placeholder="Confirm New Password">
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

        <!-- /.content-wrapper -->
    </div>
    <!-- /.content-wrapper -->
@endsection
