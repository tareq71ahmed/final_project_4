@extends('layouts.admin_layout.admin_layout')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Orders</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Orders</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">

                    @if (Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                             style="margin-top:10px;">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                   @endif
                    <div class="card">
                        <h3 style="padding: 10px;" class="card-title">Order&nbsp;|&nbsp;<a href="{{ url('admin/view-orders-charts') }}" >Orders Chart</a> 
                            | <a href="{{ route('order-graph') }}" >Order Graph</a>
                            | <a href="{{ route('order-payment-graph') }}" >Payment Graph</a>
                            | <a href="{{ route('order-method-graph') }}" >Payment Method Graph</a>
                        </h3>
                        <div class="card-header"> 
                            <form role="form" action="{{route('filter-order')}}" method="get">
                                @csrf
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">P.Status <span style="color: red">*</span> </label>
                                                <select name="payment_status" id="" class="form-control">
                                                    <option value="">All</option>
                                                    <option value="Paid">Paid</option>
                                                    <option value="Due">Due</option>
                                                    <option value="Refund">Refund</option>
                                                    <option value="Success Refund">Success Refund</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">P.Method <span style="color: red">*</span> </label>
                                                <select name="payment_method" id="" class="form-control">
                                                    <option value="">All</option>
                                                    <option value="COD">COD</option>
                                                    <option value="Paypal">Paypal</option>
                                                    <option value="SSLCommerz">SSLCommerz</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Order Status <span style="color: red">*</span> </label>
                                                <select name="order_status" id="" class="form-control">
                                                    <option value="">All</option>
                                                    @foreach ($order_status as $item)
                                                        <option value="{{$item->name}}">{{$item->name}}</option>  
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Start Date <span style="color: red">*</span> </label>
                                                <input type="date" class="form-control" name="start_date">   
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">End Date <span style="color: red">*</span> </label>
                                                <input type="date" class="form-control" name="end_date">   
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="card-body" style="margin-top: 10px">
                                            <button type="submit" class="btn btn-primary">Filter</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <table id="orders" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Order ID</th>
                                    <th>Order Date</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Order Products</th>
                                    <th>Order Amount</th>
                                    <th>Order status</th>
                                    <th>P.status</th>
                                    <th>Payment Method</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>{{ $order['id'] }}</td>
                                        <td>{{  date('d-m-y',strtotime($order['created_at'])) }} </td>
                                        <td>{{ $order['name'] }}</td>
                                        <td>{{ $order['email'] }}</td>
                                        <td>
                                            @foreach($order['orders_products'] as  $pro)
                                                {{ $pro['product_code'] }} ({{ $pro['product_qty'] }})  <br>
                                            @endforeach
                                        </td>
                                        <td>{{ $order['grand_total'] }}</td>
                                        <td><b>{{ $order['order_status'] }}</b></td>
                                        <td><b>{{ $order['payment_status'] }}</b></td>
                                        <td>{{ $order['payment_method'] }}</td>
                                        <td>
                                            @if($orderModule['edit_access']==1 || $orderModule['full_access']==1)
                                                <a title="View Order Details"
                                                   href="{{url('admin/orders/'.$order['id'])}}"><i
                                                        class="fas fa-eye"></i>
                                                </a>&nbsp;
                                                @if ($order['payment_status'] =="Refund")
                                                <a title="Refund Request"
                                                    href="{{route('refund-request',$order['id'])}}"><i class="fas fa-undo-alt"></i>
                                                </a>&nbsp;
                                                @endif
                                                

                                                @if( $order['order_status'] =="Shipped" || $order['order_status']=="Delivered" )
                                                    <a title="Order Invoice"
                                                       href="{{url('admin/view-order-invoice/'.$order['id'])}}"
                                                       target="_blank"><i class="fas fa-file-alt"></i>
                                                    </a>&nbsp;&nbsp;


                                                    <a title="Print PDF Invoice"
                                                       href="{{url('admin/print-order-invoice/'.$order['id'])}}"
                                                       target="_blank"><i style="color:red" class="fas fa-file-pdf"></i>
                                                    </a>&nbsp;
                                                @endif
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
