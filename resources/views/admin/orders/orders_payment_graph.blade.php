@extends('layouts.admin_layout.admin_layout')
@section('content')

<?php
 
$dataPoints = array( 
	array("label"=>"Due", "y"=>$ordersCount[0]),
	array("label"=>"Paid", "y"=> $ordersCount[1]),
	array("label"=>"Refund", "y"=>$ordersCount[2]),
	array("label"=>"Success Refund", "y"=>$ordersCount[3]),
)
 
?>

<script>
    window.onload = function() {
     
     
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title: {
            text: "Total Orders Payment"
        },
        subtitles: [{
            text: "All Months"
        }],
        data: [{
            type: "pie",
            yValueFormatString: "#,##0.00\"%\"",
            indexLabel: "{label} ({y})",
            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
        }]
    });
    chart.render();
     
    }
    </script>




    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Orders Payment Graph</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Orders Report</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <a href="{{url('admin/orders')}}" class="btn btn-md btn-primary">Back</a>
                    <div id="chartContainer" style="height: 450px; width: 100%;"></div>
                </div>
            </div>
        </section>
    </div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@endsection
