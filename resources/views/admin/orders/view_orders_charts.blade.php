@extends('layouts.admin_layout.admin_layout')
@section('content')

    <?php

    $months = array();
    $count = 0;
    while ($count <= 3) {
        $months[] = date('M Y', strtotime("-" .$count. " month"));
        $count++;
    }
    //echo "<pre>";print_r($months);die;

    $dataPoints = array(
        array("y" =>  $ordersCount[3], "label" => $months[3]),
        array("y" =>  $ordersCount[2], "label" => $months[2]),
        array("y" =>  $ordersCount[1], "label" => $months[1]),
        array("y" =>  $ordersCount[0], "label" => $months[0])
    );
    $dataPoints1 = array(
        array("y" =>  $ordersCount1[3], "label" => $months[3]),
        array("y" =>  $ordersCount1[2], "label" => $months[2]),
        array("y" =>  $ordersCount1[1], "label" => $months[1]),
        array("y" =>  $ordersCount1[0], "label" => $months[0])
    );
    ?>

    <script>
        window.onload = function () {

            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Orders Report"
                },
                axisY: {
                    title: "Number of Orders"
                },
                data: [{
                    type: "line",
                    dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                },{
                    type: "line",
                    dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>




    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Orders Report</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <a href="{{url('admin/orders')}}" class="btn btn-md btn-primary">Back</a>
                    @if (Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                             style="margin-top:10px;">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Orders Report</h3>

                        </div>
                        <div class="card-body">
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@endsection
