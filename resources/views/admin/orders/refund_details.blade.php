<?php  use App\Product;  ?>
@extends('layouts.admin_layout.admin_layout')
@section('content')

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">


                    @if (Session::has('success_message'))
                        <div class="col-sm-12">

                            <div class="alert alert-success alert-dismissible fade show" role="alert"
                                 style="margin-top:10px;">
                                <strong>Success!</strong> {{ Session::get('success_message') }} .
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>


                            {{Session::forget('success_message')}}
                        </div>

                    @endif


                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Order #{{ $orderDetails['id'] }} Details</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Order Details</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-bordered">

                                    <tbody>
                                    <tr>
                                        <td>Order Date</td>
                                        <td>{{  date('d-m-y',strtotime($orderDetails['created_at'])) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Order Status</td>
                                        <td>{{ $orderDetails['order_status'] }}</td>
                                    </tr>

                                    @if(!empty($orderDetails['courier_name']))
                                        <tr>
                                            <td>Courier Name</td>
                                            <td>{{ $orderDetails['courier_name'] }}</td>
                                        </tr>
                                    @endif

                                    @if(!empty($orderDetails['tracking_number']))
                                        <tr>
                                            <td>Tracking Number</td>
                                            <td>{{ $orderDetails['tracking_number'] }}</td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <td>Order Total</td>
                                        <td>BDT.{{ $orderDetails['grand_total'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Shipping Charges</td>
                                        <td>BDT.{{ $orderDetails['shipping_charges'] }}</td>
                                    </tr>

                                    <tr>
                                        <td>Coupon Code</td>
                                        <td>{{ $orderDetails['coupon_code'] }}</td>
                                    </tr>

                                    <tr>
                                        <td>Coupon Amount</td>
                                        <td>BDT.{{ $orderDetails['coupon_amount'] }}</td>
                                    </tr>

                                    <tr>
                                        <td>Payment Method</td>
                                        <td><b>{{ $orderDetails['payment_method'] }}</b></td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Delivery Address</h3>
                            </div>
                            <!-- /.card-header -->


                            <div class="card-body">
                                <table class="table table-bordered">

                                    <tbody>

                                    <tr>
                                        <td>Name</td>
                                        <td>{{ $orderDetails['name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>{{ $orderDetails['address'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>City</td>
                                        <td>{{ $orderDetails['city'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>state</td>
                                        <td>{{ $orderDetails['state'] }}</td>
                                    </tr>

                                    <tr>
                                        <td>Country</td>
                                        <td>{{ $orderDetails['country'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pincode</td>
                                        <td>{{ $orderDetails['pincode'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Mobile</td>
                                        <td>{{ $orderDetails['mobile'] }}</td>
                                    </tr>
                                    

                                    </tbody>
                                </table>
                            </div>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Customer Details</h3>
                            </div>

                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>Name</td>
                                        <td>{{ $userDetails['name'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{{ $userDetails['email'] }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Refund Details</h3>
                            </div>
                            <!-- /.card-header -->


                            <div class="card-body">
                                <table class="table table-bordered">

                                    <tbody>

                                    <tr>
                                        <td>Refund Method</td>
                                        <td>{{ $refund->refund_method }}</td>
                                    </tr>
                                    <tr>
                                        <td>Refund Account Number</td>
                                        <td>{{ $refund->number }}</td>
                                    </tr>
                                    <tr>
                                        <td>Payment Status</td>
                                        <td><b>{{ $orderDetails['payment_status'] }}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>


                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->


                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Update Refund Status</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td colspan="2">
                                            <form action="{{ route('save-refund-status') }}" method="post">
                                                @csrf
                                                <input type="hidden" name="order_id" value="{{ $orderDetails['id'] }}">
                                                <input type="hidden" name="payment_status" value="Refund Success">
                                                 
                                                <button type="submit" class="btn btn-success">Refund</button>
                                            </form>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            @foreach($orderLog as $log)
                                                <strong>{{$log['order_status']}}</strong><br>
                                                {{  date('F j, Y, g:i a  ',strtotime($log['created_at'])) }}
                                                <hr>
                                            @endforeach
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">User Order Products</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                    <tr>
                                        <th>Product Image</th>
                                        <th>Product Code</th>
                                        <th>Product Name</th>
                                        <th>Product Size</th>
                                        <th>Product Color</th>
                                        <th>Product Qty</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($orderDetails['orders_products'] as $product)
                                        <tr>
                                            <td>
                                                <?php   $getProductImage = Product::getProductImage($product['product_id']);   ?>
                                                    @if(!empty($getProductImage) && file_exists(public_path('images/product_images/small/'.$getProductImage)))
                                                    <a target="_blank" href="{{ url('product/'.$product['product_id']) }}"> <img
                                                            style="width: 80px;"
                                                            src="{{ asset('images/product_images/small/'.$getProductImage) }}"></a>
                                                @else
                                                    <a target="_blank" href="{{ url('product/'.$product['product_id']) }}"> <img
                                                            style="width: 80px;"
                                                            src="{{ asset('images/product_images/medium/medium_no_Image.png') }}"></a>
                                                @endif
                                            </td>
                                            <td>{{ $product['product_code'] }}</td>
                                            <td>{{ $product['product_name'] }}</td>
                                            <td>{{ $product['product_size'] }}</td>
                                            <td>{{ $product['product_color'] }}</td>
                                            <td>{{  $product['product_qty'] }}</td>
                                        </tr>

                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->


    </div>

@endsection
