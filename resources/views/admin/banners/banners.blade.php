@extends('layouts.admin_layout.admin_layout')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Catalogues</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Banners</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="row">
                <div class="col-12">
                    @if (Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert"
                             style="margin-top:10px;">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Banners</h3>
                            <a href="{{ url('admin/add-edit-banner') }}" class="btn btn-primary float-right">Add
                                Banners</a>
                        </div>
                        <div class="card-body">
                            <table id="banners" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Image</th>
                                    <th>Link</th>
                                    <th>Title</th>
                                    <th>Alt</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($banners as $banner)
                                    <tr>
                                        <td>{{$banner['id']}}</td>
                                        <td>
                                        <img style="width:150px;" src="{{asset('images/banner_images/'.$banner['image'])}}">
                                        </td>
                                        <td>{{$banner['link']}}</td>
                                        <td>{{$banner['title']}}</td>
                                        <td>{{$banner['alt']}}</td>

                                        <td>

                                            @if ($banner['status'] == '1')
                                                <a class="updateBannerStatus" id="banner-{{ $banner['id'] }}"
                                                   banner_id="{{ $banner['id'] }}" href="javascript:void(0)"><i
                                                        title="Active" class="fas fa-toggle-on" aria-hidden="true"
                                                        status="Active"></i></a>
                                            @else
                                                <a class="updateBannerStatus" id="banner-{{ $banner['id'] }}"
                                                   banner_id="{{ $banner['id'] }}" href="javascript:void(0)"><i
                                                        title="Inactive" style="color:grey" class="fas fa-toggle-off"
                                                        aria-hidden="true" status="Inactive"></i></a>
                                            @endif

                                        </td>
                                        <td>
                                            <a title="Edit Banner"
                                               href="{{ url('admin/add-edit-banner/' . $banner['id']) }}">&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-edit"></i></a>&nbsp;
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a title="Delete Banner" href="javascript:void(0)" class="confirmDelete"
                                               record="banner" recordid="{{ $banner['id'] }}"><i
                                                    class="fas fa-trash"></i></a>&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
