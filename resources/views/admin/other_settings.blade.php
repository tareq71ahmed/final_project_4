@extends('layouts.admin_layout.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Settings</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                            <li class="breadcrumb-item active">Other Settings</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-8">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Other Settings</h3>
                            </div>
                            <!-- /.card-header -->

                            @if (Session::has('error_message'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert"
                                     style="margin-top:10px;">
                                    <strong>Error!</strong> {{ Session::get('error_message') }} .
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            @if (Session::has('success_message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert"
                                     style="margin-top:10px;">
                                    <strong>Success!</strong> {{ Session::get('success_message') }} .
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                        @endif
                        <!-- form start -->
                            <form role="form" action="{{ url('/admin/update-other-settings') }}" method="post"
                                  name="updatePasswordForm" id="updatePasswordForm">
                                @csrf
                                <div class="card-body">

                                    <div class="form-group">
                                        <label for="min_cart_value">Minimum Cart Value</label>
                                        <input class="form-control" name="min_cart_value" id="min_cart_value"
                                               value="{{ $updateOtherSettings['min_cart_value'] }}"
                                               placeholder="Minimum Cart Value">
                                    </div>

                                    <div class="form-group">
                                        <label for="max_cart_value">Maximum Cart Value</label>
                                        <input class="form-control" name="max_cart_value" id="max_cart_value"
                                               value="{{ $updateOtherSettings['max_cart_value'] }}"
                                               placeholder="Maximum Cart Value">
                                    </div>

                                    <div class="card-footer">
                                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                    </div>
                            </form>
                        </div>
                        <!-- /.card -->
                    </div>

                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->

        <!-- /.content-wrapper -->
    </div>
    <!-- /.content-wrapper -->
@endsection
