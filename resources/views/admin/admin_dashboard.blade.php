@extends('layouts.admin_layout.admin_layout')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ url('admin/dashboard') }}">DashBoard</a></li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">

            @if (Session::has('error_message'))
                <div class="alert alert-danger alert-dismissible fade show" role="alert"
                     style="margin-top:10px;">
                    <strong>Error!</strong> {{ Session::get('error_message') }} .
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                            <div class="inner">
                                <h3>{{App\Order::count()}}</h3>

                                <p>New Orders</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="{{ url('admin/orders') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{App\Product::count()}}</h3>

                                <p>Total Products</p>
                            </div>
                            <div class="icon">
<!--                                <i class="ion ion-person-add"></i>-->
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{url('admin/products')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                            <div class="inner">
                                <h3>{{App\User::count()}}</h3>

                                <p>User Registrations</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{url('admin/users')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                            <div class="inner">
                                <h3>{{App\Coupon::count()}}</h3>

                                <p>Coupons</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="{{url('admin/coupons')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
        <!-- Orders chart -->
        <?php

        $months = array();
        $count = 0;
        while ($count <= 3) {
            $months[] = date('M Y', strtotime("-" .$count. " month"));
            $count++;
        }
        //echo "<pre>";print_r($months);die;

        $dataPoints = array(
            array("y" =>  $ordersCount[3], "label" => $months[3]),
            array("y" =>  $ordersCount[2], "label" => $months[2]),
            array("y" =>  $ordersCount[1], "label" => $months[1]),
            array("y" =>  $ordersCount[0], "label" => $months[0])
        );
        $dataPoints1 = array(
            array("y" =>  $ordersCount1[3], "label" => $months[3]),
            array("y" =>  $ordersCount1[2], "label" => $months[2]),
            array("y" =>  $ordersCount1[1], "label" => $months[1]),
            array("y" =>  $ordersCount1[0], "label" => $months[0])
        );

        ?>
        <script>
            window.onload = function () {

                var chart = new CanvasJS.Chart("chartContainer", {
                    title: {
                        text: "Orders Report"
                    },
                    axisY: {
                        title: "Number of Orders"
                    },
                    data: [{
                        type: "line",
                        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                    },{
                        type: "line",
                        dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
                    }
                    ]
                });
                chart.render();

            }
        </script>
        <section class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Orders Report</h3>

                        </div>
                        <div class="card-body">
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Orders chart  End -->
    </div>
    <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@endsection
