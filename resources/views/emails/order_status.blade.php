<html>

<body>
<table style="width:700px;">
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img src="{{asset('images/front_images/logo1.png')}}"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Hellow {{$name}},</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Your Order #{{ $order_id }} Has been Updated to <strong>{{ $order_status }}</strong>. Your Order Details are as Below:-</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
        @if(!empty($courier_name) && !empty($tracking_number))
            <tr><td></td></tr>
        <tr><td>Couirer Name Is: <strong>{{$courier_name}}</strong>  And Tracking ID: <strong>{{$tracking_number}}</strong></td></tr>
            @endif</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>
    <tr style="width:95%; cellpadding:'5'; cellspacing:'5' ;bgcolor:'#f7f4f4'; ">
    <tr bgcolor="#cccccc">
        <td>Product Name</td>
        <td>Code</td>
        <td>Size</td>
        <td>Color</td>
        <td>Quantity</td>
        <td>Price</td>
    </tr>

    @foreach( $orderDetails['orders_products'] as $order )
        <tr>
            <td>{{ $order['product_name'] }}</td>
            <td>{{ $order['product_code'] }}</td>
            <td>{{ $order['product_size'] }}</td>
            <td>{{ $order['product_color'] }}</td>
            <td>{{ $order['product_qty'] }}</td>
            <td>BDT.{{ $order['product_price'] }}</td>
        </tr>

    @endforeach

    <tr>
        <td colspan="5" align="right">Shipping Charges</td>
        <td>BDT.{{$orderDetails['shipping_charges']}}</td>
    </tr>

    <tr>
        <td colspan="5" align="right">Coupon Discount</td>
        <td>BDT.
            @if($orderDetails['coupon_amount']>0)
                {{$orderDetails['coupon_amount']}}
            @else
                0
            @endif
        </td>
    </tr>

    <tr>
        <td colspan="5" align="right">Grand Total</td>
        <td>BDT.{{$orderDetails['grand_total']}}</td>
    </tr>

</table>
</td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>
        <table>
            <tr>
                <td><strong>Delivery Address</strong></td>
            </tr>

            <tr>
                <td>{{$orderDetails['name']}}</td>
            </tr>
            <tr>
                <td>{{$orderDetails['address']}}</td>
            </tr>
            <tr>
                <td>{{$orderDetails['city']}}</td>
            </tr>
            <tr>
                <td>{{$orderDetails['state']}}</td>
            </tr>
            <tr>
                <td>{{$orderDetails['pincode']}}</td>
            </tr>
            <tr>
                <td>{{$orderDetails['mobile']}}</td>
            </tr>

        </table>

    </td>
</tr>


<tr>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>For Any Query You can Contact With <a href="#">AmazonBd</a></td>
</tr>
<br>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>Regards<br>AmazonBd team</td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>

</table>
</body>
</html>

<!--Mail ar jonne ata kete dibo-->
<?php /* die; */?>
