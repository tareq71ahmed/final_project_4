@extends('layouts.front_layout.front_layout')
@section('content')
    <div id="mainBody">
        <div class="container">

            <div class="row">
                <div class="span4">
                    <h4>Contact Details</h4>
                    <p> AmazonBd<br/> CA 1200, Bangladesh
                        <br/><br/>
                        AmazonBd@sitemakers.in<br/>
                        ﻿Tel 00000-00000<br/>
                        Fax 00000-00000<br/>
                        web: AmazonBd.com
                    </p>
                </div>

                <div class="span4">
                    <h4>Email Us</h4>

                    @if (Session::has('success_message'))
                        <div class="alert alert-success " role="alert">
                            <strong>Success!</strong> {{ Session::get('success_message') }} .
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <?php  Session::forget('success_message');  ?>
                    @endif

                    @if($errors->any())
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{url('/contact')}}" class="form-horizontal" method="post">
                        @csrf
                        <fieldset>
                            <div class="control-group">

                                <input type="text" name="name" id="name" style="height: 25px" placeholder="name"
                                       class="input-xlarge"/>

                            </div>
                            <div class="control-group">

                                <input type="text" name="email" id="email" style="height: 25px" placeholder="email"
                                       class="input-xlarge"/>

                            </div>
                            <div class="control-group">

                                <input type="text" name="subject" id="subject" style="height: 25px"
                                       placeholder="subject" class="input-xlarge"/>
                            </div>
                            <div class="control-group">
                                <textarea name="message" id="message" rows="3" id="textarea" placeholder="mesage"
                                          class="input-xlarge"></textarea>
                            </div>
                            <button class="btn btn-large" type="submit">Send Messages</button>
                        </fieldset>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection








