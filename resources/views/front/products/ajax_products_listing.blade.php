<?php  use App\Product;    ?>

<div class="tab-pane  active" id="blockView">
    <ul class="thumbnails">
        @foreach($categoryProducts as $product)
            <li class="span3">
                <div class="thumbnail" style="height:420px;">
                    <a href="{{route('detailpage',$product['id'])}}">
                        @if(isset($product['main_image']))
                            <?php $product_image_path = "images/product_images/small/" . $product['main_image'];    ?>
                        @else
                            <?php $product_image_path = "";    ?>
                        @endif
                        @if(!empty($product['main_image']) && file_exists($product_image_path))
                            <img style="height:250px;width:250px;"
                                 src="{{asset($product_image_path)}}"
                                 alt="">
                        @else
                            <img style="height:250px;width:250px;"
                                 src="{{asset('images/product_images/small/no_image.png')}}"
                                 alt="">
                        @endif
                    </a>
                    <div class="caption">
                        <h5>{{$product['product_name']}}</h5>
                        <p>
                            {{$product['brand']['name']}}
                        </p>

                        <?php $discounted_price = Product::getDiscountedPrice($product['id']);     ?>

                        <h4 style="text-align:center"> <a class="btn"
                                                                     href="{{route('detailpage',$product['id'])}}">Add
                                to <i
                                    class="icon-shopping-cart"></i></a> <a class="btn btn-primary"
                                                                           href="#">
                                @if($discounted_price > 0 )
                                    <del>BDT.{{$product['product_price']}}</del>&nbsp;&nbsp;
                                    <font color="yellow">BDT.{{ $discounted_price }}</font>
                                @else
                                    BDT.{{$product['product_price']}}
                                @endif

                            </a>
                        </h4>

                        {{--@if($discounted_price > 0)
                            <h4><font color="red">Discounted Price: BDT.{{ $discounted_price }}</font></h4>
                        @endif--}}
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
    <hr class="soft"/>
</div>


