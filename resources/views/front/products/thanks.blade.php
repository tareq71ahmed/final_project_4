@extends('layouts.front_layout.front_layout')
@section('content')

    <div class="span9">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a> <span class="divider">/</span></li>
            <li class="active">THANKS</li>
        </ul>
        <h3>THANKS</h3>
        <hr class="soft"/>

        <div align="center">
            <h3>YOUR ORDER HAS BEEN PLACED SUCCESSFULL</h3>
            <p>Your Order Number Is <strong>{{Session::get('order_id')}}</strong> And grand total is <strong>BDT.{{ Session::get('grand_total') }}</strong></p>
        </div>

    </div>
@endsection

<?php
Session::forget('grand_total');
Session::forget('order_id');
Session::forget('couponAmount');
Session::forget('couponCode');
?>
