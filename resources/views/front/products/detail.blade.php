<?php  use App\Product;    ?>


@extends('layouts.front_layout.front_layout')
@section('content')

    {{--Review css--}}

    <style>
        * {
            margin: 0;
            padding: 0;
        }

        .rate {
            float: left;
            height: 46px;
            padding: 0 10px;
        }

        .rate:not(:checked) > input {
            position: absolute;
            top: -9999px;
        }

        .rate:not(:checked) > label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 30px;
            color: #ccc;
        }

        .rate:not(:checked) > label:before {
            content: '★ ';
        }

        .rate > input:checked ~ label {
            color: #ffc700;
        }

        .rate:not(:checked) > label:hover,
        .rate:not(:checked) > label:hover ~ label {
            color: #deb217;
        }

        .rate > input:checked + label:hover,
        .rate > input:checked + label:hover ~ label,
        .rate > input:checked ~ label:hover,
        .rate > input:checked ~ label:hover ~ label,
        .rate > label:hover ~ input:checked ~ label {
            color: #c59b08;
        }

    </style>

    {{--Review css End--}}

    <div class="span9">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a> <span class="divider">/</span></li>
            <li>
                <a href="{{url('/'.$productDetails['category']['url'])}}">{{$productDetails['category']['category_name']}}</a><span
                    class="divider">/</span></li>
            <li class="active">{{$productDetails['product_name']}}</li>
        </ul>
        <div class="row">
            <div id="gallery" class="span3">

                <?php $product_image_path = "images/product_images/large/" . $productDetails['main_image'];    ?>
                @if(!empty($productDetails['main_image']) && file_exists($product_image_path))

                    <a href="{{asset($product_image_path)}}"
                       title="{{$productDetails['product_name']}}">
                        <img src="{{asset($product_image_path)}}"
                             style="width:100%" alt="Blue Casual T-Shirt"/>

                    </a>
                @else

                    <a href="{{asset('images/product_images/medium/medium_no_Image.png')}}"
                       title="{{$productDetails['product_name']}}">
                        <img src="{{asset('images/product_images/medium/medium_no_Image.png')}}"
                             style="width:100%" alt="Blue Casual T-Shirt"/>
                    </a>

                @endif

                <div id="differentview" class="moreOptopm carousel slide">
                    <div class="carousel-inner">
                        <div class="item active">
                            @foreach($productDetails['images'] as $image)
                                @if($image&&$image['status']==1)
                                <a href="{{asset('images/product_images/medium/'.$image['image'])}}"> <img
                                        style="width:29%"
                                        src="{{asset('images/product_images/medium/'.$image['image'])}}"
                                        alt=""/></a>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>

<!--                <div class="btn-toolbar">
                    <div class="btn-group">
                        <span class="btn"><i class="icon-envelope"></i></span>
                        <span class="btn"><i class="icon-print"></i></span>
                        <span class="btn"><i class="icon-zoom-in"></i></span>
                        <span class="btn"><i class="icon-star"></i></span>
                        <span class="btn"><i class=" icon-thumbs-up"></i></span>
                        <span class="btn"><i class="icon-thumbs-down"></i></span>
                    </div>
                </div>-->
            </div>
            <div class="span6">

                @if (Session::has('success_message'))
                    <div class="alert alert-success " role="alert">
                        <strong>Success!</strong> {{ Session::get('success_message') }} .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php  Session::forget('success_message');  ?>
                @endif

                @if (Session::has('error_message'))
                    <div class="alert alert-danger " role="alert">
                        <strong>Error!</strong> {{ Session::get('error_message') }} .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <?php  Session::forget('error_message');  ?>
                @endif


                <h3>{{$productDetails['product_name']}}</h3>
                <small style="font-size:15px;">- {{$productDetails['brand']['name']}}</small>
                    <div>&nbsp;</div>
                    @if($avgStarRating > 0)
                    <div>
                        <?php
                        $star = 1;
                        while($star<=$avgStarRating) { ?>
                         <span style="font-size:20px;color:green">&#9733;</span>
                        <?php  $star++; } ?>({{$avgRating}})
                    </div>
                    @endif
                <hr class="soft"/>

                @if(count($groupProducts)>0)
                    <div>
                        <div><strong>More Colors</strong></div>
                        <div>
                            @foreach($groupProducts as $product)
                                <?php $product_image_path = "images/product_images/small/" . $product['main_image'];    ?>
                                @if(!empty($product['main_image']) && file_exists($product_image_path))
                                    <a href="{{url('product/'.$product['id'])}}"
                                       title="Blue Casual T-Shirt">
                                        <img src="{{asset($product_image_path)}}"
                                             style="width:50px" alt="Blue Casual T-Shirt"/>
                                    </a>
                                @else
                                    <a href="{{url('product/'.$product['id'])}}"
                                       title="Blue Casual T-Shirt">
                                        <img src="{{asset('images/product_images/medium/medium_no_Image.png')}}"
                                             style="width:50px" alt="Blue Casual T-Shirt"/>
                                    </a>

                                @endif
                            @endforeach
                        </div>

                    </div>
                    <br>

                @endif


                <small style="font-size: 15px;">{{$total_stock}} items in stock</small>
                <form action="{{url('add-to-cart')}}" method="post" class="form-horizontal qtyFrm">
                    @csrf
                    <input type="hidden" name="product_id" value="{{$productDetails['id']}}">
                    <div class="control-group">

                        <?php $discounted_price = Product::getDiscountedPrice($productDetails['id']);     ?>

                        <h4 class="getAttrPrice">

                            @if($discounted_price > 0 )

                                <del>BDT. {{$productDetails['product_price']}}</del>&nbsp;&nbsp;
                                BDT.{{$discounted_price}}
                            @else
                                BDT. {{$productDetails['product_price']}}

                            @endif

                        </h4>
                        <select name="size" id="getPrice" product-id="{{$productDetails['id']}}" class="span2 pull-left"
                        >
                            <option value="">select size</option>
                            @foreach($productDetails['attributes'] as $attribute)
                                <option value="{{$attribute['size']}}">{{$attribute['size']}}</option>
                            @endforeach
                        </select>
                        <input name="quantity" id="quantity" min="1" type="number" class="span1" placeholder="Qty."/>
                        <button type="submit" class="btn btn-large btn-primary pull-right"> Add to cart <i
                                class=" icon-shopping-cart"></i>
                        </button>
                        <br><br>
                        <strong>Delivery</strong>
                        <input style="width:120px;" type="text" name="pincode" id="pincode"
                               placeholder="Delivery Zone Post Code"/>
                        <button type="button" id="checkPincode">Check</button>
                    </div>
                </form>
            </div>


            <hr class="soft clr"/>
            <p class="span6">
                {{$productDetails['description']}}

            </p>
<!--            <a class="btn btn-small pull-right" href="#detail">More Details</a>-->
            <br class="clr"/>
            <a href="#" name="detail"></a>
            <hr class="soft"/>
        </div>

        <div class="span9">
            <ul id="productDetail" class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab">Product Details</a></li>
                <li><a href="#profile" data-toggle="tab">Related Products</a></li>
                @if(isset($productDetails['product_video']) && !empty($productDetails['product_video']))
                    <li><a href="#video" data-toggle="tab">Product Video</a></li>
                @endif
                <li><a href="#review" data-toggle="tab">Product Reviews</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">

                <div class="tab-pane fade active in" id="home">
                    <h4>Product Information</h4>
                    <table class="table table-bordered">
                        <tbody>
                        <tr class="techSpecRow">
                            <th colspan="2">Product Details</th>
                        </tr>
                        <tr class="techSpecRow">
                            <td class="techSpecTD1">Brand:</td>
                            <td class="techSpecTD2">{{$productDetails['brand']['name']}}</td>
                        </tr>
                        <tr class="techSpecRow">
                            <td class="techSpecTD1">Code:</td>
                            <td class="techSpecTD2">{{$productDetails['product_code']}}</td>
                        </tr>
                        <tr class="techSpecRow">
                            <td class="techSpecTD1">Color:</td>
                            <td class="techSpecTD2">{{$productDetails['product_color']}}</td>
                        </tr>

                        @if(!empty($productDetails['fabric']))
                            <tr class="techSpecRow">
                                <td class="techSpecTD1">Fabric:</td>
                                <td class="techSpecTD2">{{$productDetails['fabric']}}</td>
                            </tr>
                        @endif

                        @if(!empty($productDetails['pattern']))
                            <tr class="techSpecRow">
                                <td class="techSpecTD1">Pattern:</td>
                                <td class="techSpecTD2">{{$productDetails['pattern']}}</td>
                            </tr>
                        @endif

                        @if(!empty($productDetails['sleeve']))
                            <tr class="techSpecRow">
                                <td class="techSpecTD1">Sleeve:</td>
                                <td class="techSpecTD2">{{$productDetails['sleeve']}}</td>
                            </tr>
                        @endif

                        @if(!empty($productDetails['fit']))
                            <tr class="techSpecRow">
                                <td class="techSpecTD1">Fit:</td>
                                <td class="techSpecTD2">{{$productDetails['fit']}}</td>
                            </tr>
                        @endif

                        @if(!empty($productDetails['occasion']))
                            <tr class="techSpecRow">
                                <td class="techSpecTD1">Occasion:</td>
                                <td class="techSpecTD2">{{$productDetails['occasion']}}</td>
                            </tr>
                        @endif

                        </tbody>
                    </table>

                    <h5>Washcare</h5>
                    <p>{{$productDetails['wash_care']}}</p>
                    <h5>Disclaimer</h5>
                    <p>
                        There may be a slight color variation between the image shown and original product.
                    </p>
                </div>


                <div class="tab-pane fade" id="profile">
                    <div id="myTab" class="pull-right">
                        <a href="#listView" data-toggle="tab"><span class="btn btn-large"><i
                                    class="icon-list"></i></span></a>
                        <a href="#blockView" data-toggle="tab"><span class="btn btn-large btn-primary"><i
                                    class="icon-th-large"></i></span></a>
                    </div>
                    <br class="clr"/>
                    <hr class="soft"/>
                    <div class="tab-content">
                        <div class="tab-pane" id="listView">
                            @foreach($relatedProducts as $product)
                                <div class="row">
                                    <div class="span2">
                                        @if(isset($product['main_image']))
                                            <?php $product_image_path = "images/product_images/small/" . $product['main_image'];    ?>
                                        @else
                                            <?php $product_image_path = "";    ?>
                                        @endif
                                        @if(!empty($product['main_image']) && file_exists($product_image_path))
                                            <img style="height:250px;width:250px;"
                                                 src="{{asset($product_image_path)}}"
                                                 alt="">
                                        @else
                                            <img style="height:250px;width:250px;"
                                                 src="{{asset('images/product_images/small/no_image.png')}}"
                                                 alt="">
                                        @endif
                                    </div>
                                    <div class="span4">
                                        <h3>{{$product['product_name']}}</h3>
                                        <hr class="soft"/>
                                        <h5>{{$product['product_code']}}</h5>
                                        <p>
                                            {{$product['description']}}
                                        </p>
                                        <a class="btn btn-small pull-right"
                                           href="{{route('detailpage',$product['id'])}}">View Details</a>
                                        <br class="clr"/>
                                    </div>
                                    <div class="span3 alignR">
                                        <form class="form-horizontal qtyFrm">
                                            <h3> BDT.{{$product['product_price']}}</h3>
                                            <!--                                        <label class="checkbox">
                                                                                        <input type="checkbox"> Adds product to compair
                                                                                    </label>-->
                                            <br/>
                                            <div class="btn-group">
                                                <a href="{{route('detailpage',$product['id'])}}"
                                                   class="btn btn-large btn-primary"> Add to <i
                                                        class=" icon-shopping-cart"></i></a>
                                                <a href="{{route('detailpage',$product['id'])}}"
                                                   class="btn btn-large"><i
                                                        class="icon-zoom-in"></i></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <hr class="soft"/>
                            @endforeach

                        </div>
                        <div class="tab-pane active" id="blockView">
                            <ul class="thumbnails">
                                @foreach($relatedProducts as $product)
                                    <li class="span3">
                                        <div class="thumbnail">
                                            <a href="{{route('detailpage',$product['id'])}}">
                                                @if(isset($product['main_image']))
                                                    <?php $product_image_path = "images/product_images/small/" . $product['main_image'];    ?>
                                                @else
                                                    <?php $product_image_path = "";    ?>
                                                @endif
                                                @if(!empty($product['main_image']) && file_exists($product_image_path))
                                                    <img style="height:250px;width:250px;"
                                                         src="{{asset($product_image_path)}}"
                                                         alt="">
                                                @else
                                                    <img style="height:250px;"
                                                         src="{{asset('images/product_images/small/no_image.png')}}"
                                                         alt="">
                                                @endif

                                            </a>
                                            <div class="caption">
                                                <h5>{{$product['product_name']}}</h5>
                                                <p>
                                                    {{$product['product_code']}}
                                                </p>
                                                <h4 style="text-align:center"><a class="btn"
                                                                                 href="{{route('detailpage',$product['id'])}}">
                                                        <i
                                                            class="icon-zoom-in"></i></a> <a class="btn"
                                                                                             href="{{route('detailpage',$product['id'])}}">Add
                                                        to <i
                                                            class="icon-shopping-cart"></i></a> <a
                                                        class="btn btn-primary"
                                                        href="#">BDT.{{$product['product_price']}}</a></h4>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach

                            </ul>
                            <hr class="soft"/>
                        </div>
                    </div>
                    <br class="clr">
                </div>


                @if(isset($productDetails['product_video']) && !empty($productDetails['product_video']))
                    <div class="tab-pane fade" id="video">
                        <video controls="" width="640" height="480">
                            <source src="{{url('videos/product_videos/'.$productDetails['product_video'])}}"
                                    type="video/mp4">
                        </video>
                    </div>
                @endif


                {{--Review --}}
                <div class="tab-pane fade" id="review">
                    <div class="row">
                        <div class="span4">
                            <h3>Write a review</h3>
                            <form method="post" action="{{url('/add-rating')}}" name="ratingForm" id="ratingForm"
                                  class="form-horizontal">
                                @csrf
                                <input type="hidden" name="product_id" value="{{$productDetails['id']}}">
                                <div class="rate">
                                    <input type="radio" id="star5" name="rating" value="5"/>
                                    <label for="star5" title="text">5 stars</label>
                                    <input type="radio" id="star4" name="rating" value="4"/>
                                    <label for="star4" title="text">4 stars</label>
                                    <input type="radio" id="star3" name="rating" value="3"/>
                                    <label for="star3" title="text">3 stars</label>
                                    <input type="radio" id="star2" name="rating" value="2"/>
                                    <label for="star2" title="text">2 stars</label>
                                    <input type="radio" id="star1" name="rating" value="1"/>
                                    <label for="star1" title="text">1 star</label>
                                </div>
                                <div class="control-group">&nbsp;</div>
                                <div class="form-group">
                                    <label>Your Review*</label>
                                    <textarea name="review" style="width:300px;height:50px;" required></textarea>
                                </div>
                                <div>&nbsp;</div>
                                <div class="form-group">
                                    <input class="btn btn-primary" type="submit" name="submit">
                                </div>
                            </form>
                        </div>
                        <div class="span4">
                            <h3>Users Review</h3>
                            @if(count($ratings) > 0)
                            @foreach($ratings as $rating)
                                <div>

                                    <?php
                                    $count = 1;
                                    while($count<=($rating['rating'])){ ?>
                                        <span style="font-size:26px;color:green">&#9733;</span>
                                    <?php  $count++; } ?>
                                    <p>{{ $rating['review'] }}</p>
                                    <p>{{ $rating['user']['name'] }}</p>
                                    <p><small>{{date("d-m-y",strtotime($rating['created_at']))}}</small></p>
                                    <hr>
                                </div>

                            @endforeach

                            @else
                                <p><b>Reviews are not available for this Product</b></p>
                            @endif

                        </div>
                    </div>
                </div>
                {{--Review End--}}

            </div>
        </div>
    </div>
@endsection





