<?php  use App\Product;   ?>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>Product</th>
        <th colspan="2">Description</th>
        <th>Quantity/Update</th>
        <th>Price</th>
        <th>Category/Product<br>Discount</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>

    <?php  $total_price = 0;   ?>
    @foreach($userCartItems as $item)
        <?php  $attrPrice = Product::getDiscountedAttrPrice($item['product_id'], $item['size']);  ?>
        <tr>
            <td>
                <?php $product_image_path = "images/product_images/small/" . $item['product']['main_image'];    ?>
                @if(!empty($item['product']['main_image']) && file_exists($product_image_path))
                    <img width="60" src="{{asset($product_image_path)}}" alt=""/>
                @else
                    <img width="60" src="{{asset('images/product_images/small/no_image.png')}}" alt=""/>
                @endif
            </td>

            <td colspan="2">{{$item['product']['product_name']}} <br/>Color
                : {{$item['product']['product_color']}}<br/>Size : {{$item['size']}}
                <br/>Code:{{$item['product']['product_code']}}</td>

            <td>
                <div class="input-append">

                    <input class="span1" style="max-width:34px" value="{{$item['quantity']}}"
                           id="appendedInputButtons" size="16" type="text">
                    <button class="btn btnItemUpdate qtyMinus" type="button" data-cartid="{{$item['id']}}"><i
                            class="icon-minus"></i></button>
                    <button class="btn btnItemUpdate qtyPlus" type="button" data-cartid="{{$item['id']}}"><i
                            class="icon-plus"></i></button>
                    <button class="btn btn-danger btnItemDelete" type="button" data-cartid="{{$item['id']}}"><i
                            class="icon-remove icon-white"></i></button>

                </div>
            </td>

            <td>BDT.{{$attrPrice['product_price'] * $item['quantity'] }}</td>
            <td>BDT.{{$attrPrice['discount'] * $item['quantity'] }}</td>
            <td>BDT.{{$attrPrice['final_price'] * $item['quantity']}}</td>
        </tr>

        <?php $total_price = $total_price + ($attrPrice['final_price'] * $item['quantity'])   ?>
    @endforeach
    <tr>
        <td colspan="6" style="text-align:right">Sub Total:</td>
        <td> BDT.{{$total_price}}</td>
    </tr>
    <tr>
        <td colspan="6" style="text-align:right">Coupons Discount:</td>
        <td class="couponAmount">
            @if(Session::has('couponAmount') )
                - BDT.{{Session::get('couponAmount')}}
            @else
                BDT.0
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="6" style="text-align:right"><strong>GRANT TOTAL (BDT. {{$total_price}} - <span class="couponAmount"> BDT. 0</span> ) =</strong></td>
        <td class="label label-important" style="display:block"><strong class="grand_total"> BDT. {{ $total_price - Session::get('couponAmount') }} </strong></td>
    </tr>


    </tbody>
</table>
