<?php  use App\Product;    ?>
@extends('layouts.front_layout.front_layout')
@section('content')

    <div class="span9">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a> <span class="divider">/</span></li>
            <li class="active"> CHECKOUT</li>
        </ul>
        <h3> CHECKOUT [ <small><span class="totalCartItems">{{totalCartItems()}}</span> Item(s) </small>]<a
                href="{{url('/cart')}}" class="btn btn-large pull-right"><i
                    class="icon-arrow-left"></i> Back To Cart </a></h3>
        <hr class="soft"/>
        @if (Session::has('success_message'))
            <div class="alert alert-success " role="alert">
                <strong>Success!</strong> {{ Session::get('success_message') }} .
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php Session::forget('success_message');   ?>
        @endif

        @if (Session::has('error_message'))
            <div class="alert alert-danger " role="alert">
                <strong>Error!</strong> {{ Session::get('error_message') }} .
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php Session::forget('error_message');   ?>
        @endif


        <form name="checkoutForm" id="checkoutForm" action="{{url('/checkout')}}" method="POST">
            @csrf
            <table class="table table-bordered">
                <tr>
                    <td><strong>DELIVERY ADDRESS&nbsp;</strong>&nbsp;|&nbsp;<a
                            href="{{url('add-edit-delivery-address')}}">&nbsp;<strong>ADD</strong></a>
                    </td>
                </tr>
                @foreach($deliveryAddresses as $address)
                    <tr>
                        <td>
                            <div class="control-group" style="float:left;margin-top:-2px;margin-right: 5px;">
                                <input type="radio" id="address{{$address['id']}}" name="address_id"
                                       value="{{$address['id']}}" shipping_charges="{{$address['shipping_charges']}}" total_price="{{$total_price}}" coupon_amount="{{Session::get('couponAmount')}}"   codpincodeCount="{{$address['codpincodeCount']}}" prepaidpincodeCount="{{$address['prepaidpincodeCount']}}" >
                            </div>
                            <div class="control-group">
                                <label class="control-label">{{$address['name']}}, {{$address['address']}}
                                    , {{$address['city']}}-{{$address['pincode']}}, {{$address['state']}},{{$address['country_name']}}
                                    (M:{{$address['mobile']}})</label>
                            </div>
                        </td>
                        <td><a href="{{url('add-edit-delivery-address/'.$address['id'])}}">EDIT</a> | <a
                                href="{{url('delete-delivery-address/'.$address['id'])}}"
                                class="addressDelete">DELETE</a>
                        </td>
                    </tr>
                @endforeach
            </table>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Product</th>
                    <th colspan="2">Description</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Category/Product<br>Discount</th>
                    <th>Sub Total</th>
                </tr>
                </thead>
                <tbody>

                <?php  $total_price = 0;   ?>
                @foreach($userCartItems as $item)
                    <?php  $attrPrice = Product::getDiscountedAttrPrice($item['product_id'], $item['size']);  ?>
                    <tr>
                        <td>
                            <?php $product_image_path = "images/product_images/small/" . $item['product']['main_image'];    ?>
                            @if(!empty($item['product']['main_image']) && file_exists($product_image_path))
                                <img width="60" src="{{asset($product_image_path)}}" alt=""/>
                            @else
                                <img width="60" src="{{asset('images/product_images/small/no_image.png')}}" alt=""/>
                            @endif
                        </td>

                        <td colspan="2">{{$item['product']['product_name']}} <br/>Color
                            : {{$item['product']['product_color']}}<br/>Size : {{$item['size']}}
                            <br/>Code:{{$item['product']['product_code']}}</td>

                        <td>
                            {{$item['quantity']}}
                        </td>

                        <td>BDT.{{$attrPrice['product_price']* $item['quantity']}}</td>
                        <td>BDT.{{$attrPrice['discount'] * $item['quantity'] }}</td>
                        <td>BDT.{{$attrPrice['final_price'] * $item['quantity']}}</td>
                    </tr>

                    <?php $total_price = $total_price + ($attrPrice['final_price'] * $item['quantity']) ;  ?>
                @endforeach
                <tr>
                    <td colspan="6" style="text-align:right">Sub Total:</td>
                    <td> BDT.{{$total_price}}</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align:right">Coupons Discount:</td>
                    <td class="couponAmount">
                        @if(Session::has('couponAmount') )
                            - BDT.{{Session::get('couponAmount')}}
                        @else
                            BDT.0
                        @endif
                    </td>
                </tr>




                <tr>
                    <td colspan="6" style="text-align:right">Shipping Charge:</td>
                    <td class="shipping_charges">BDT.0</td>
                </tr>


                <tr>
                    <td colspan="6" style="text-align:right"><strong>GRANT TOTAL (BDT. {{$total_price}} - <span
                                class="couponAmount"> BDT. {{ Session::get('couponAmount', 0) }}</span> + <span class="shipping_charges">BDT.0</span> ) =</strong></td>
                    <td class="label label-important" style="display:block"><strong class="grand_total">
                            BDT. {{ $grand_total = $total_price - Session::get('couponAmount') }}
                            <?php   Session::put('grand_total',$grand_total);   ?>
                        </strong></td>
                </tr>
                </tbody>
            </table>


            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td>
                        <div class="control-group">
                            <label class="control-label"><strong> PAYMENT METHODS: </strong> </label>
                            <div class="controls">
                                <span>
                                    <span class="codMethod">
                                    <input type="radio" name="payment_getway" id="COD"
                                           value="COD">&nbsp;<strong>COD</strong>&nbsp;&nbsp;
                                    </span>
                                    <span class="prepaidMethod" >
                                        <input type="radio" name="payment_getway" id="Paypal" value="Paypal">&nbsp;<strong>Paypal</strong>
                                     </span>
                                     <span class="prepaidMethod" >
                                        <input type="radio" name="payment_getway" id="Paypal" value="SSLCommerz">&nbsp;<strong>SSLCommerze</strong>
                                     </span>
                                </span>
                            </div>
                        </div>
                    </td>
                </tr>

                </tbody>
            </table>
            <a href="{{url('/cart')}}" class="btn btn-large"><i class="icon-arrow-left"></i> Back To Cart </a>
            <button type="submit" class="btn btn-large pull-right">Place Order<i class="icon-arrow-right"></i></button>
        </form>

    </div>
@endsection
