<?php  use App\Product;  ?>

@extends('layouts.front_layout.front_layout')
@section('content')

    <div class="span9">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a> <span class="divider">/</span></li>
            <li class="active"><a href="{{ url('/orders') }}">Orders</a></li>
        </ul>
        <h3>Order #{{ $orderDetails['id'] }} Details</h3>
        <hr class="soft"/>



        <div class="row">
            <div class="span4">
                <table class="table table-striped table-bordered">
                <tr>
                    <td colspan="2"><strong>Order Details</strong></td>
                </tr>
                <tr>
                    <td>Order Date</td>
                    <td>{{  date('d-m-y',strtotime($orderDetails['created_at'])) }}</td>
                </tr>
                <tr>
                    <td>Order Status</td>
                    <td>{{ $orderDetails['order_status'] }}</td>
                </tr>

                <tr>
                    <td>Payment Status</td>
                    <td>{{ $orderDetails['payment_status'] }}</td>
                </tr>

                    @if(!empty($orderDetails['courier_name']))
                    <tr>
                        <td>Courier Name</td>
                        <td>{{ $orderDetails['courier_name'] }}</td>
                    </tr>
                    @endif

                    @if(!empty($orderDetails['tracking_number']))
                    <tr>
                        <td>Tracking Number</td>
                        <td>{{ $orderDetails['tracking_number'] }}</td>
                    </tr>
                    @endif

                <tr>
                    <td>Order Total</td>
                    <td>BDT.{{ $orderDetails['grand_total'] }}</td>
                </tr>
                <tr>
                    <td>Shipping Charges</td>
                    <td>BDT.{{ $orderDetails['shipping_charges'] }}</td>
                </tr>

                <tr>
                    <td>Coupon Code</td>
                    <td>{{ $orderDetails['coupon_code'] }}</td>
                </tr>

                <tr>
                    <td>Coupon Amount</td>
                    <td>BDT.{{ $orderDetails['coupon_amount'] }}</td>
                </tr>

                <tr>
                    <td>Payment Method</td>
                    <td>{{ $orderDetails['payment_method'] }}</td>
                </tr>

                </table>
            </div>

            <div class="span4">
                <table class="table table-striped table-bordered">
                <tr>
                    <td colspan="2"><strong>Delivery Address</strong></td>
                </tr>

                <tr>
                    <td>Name</td>
                    <td>{{ $orderDetails['name'] }}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>{{ $orderDetails['address'] }}</td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>{{ $orderDetails['city'] }}</td>
                </tr>
                <tr>
                    <td>state</td>
                    <td>{{ $orderDetails['state'] }}</td>
                </tr>
                <tr>
                    <td>Pincode</td>
                    <td>{{ $orderDetails['pincode'] }}</td>
                </tr>
                <tr>
                    <td>Mobile</td>
                    <td>{{ $orderDetails['mobile'] }}</td>
                </tr>

                </table>
            </div>
            
        </div>






        <div class="row">
            <div class="span8">
                <table class="table table-striped table-bordered">
                <tr>
                    <th>Product Image </th>
                    <th>Product Code </th>
                    <th>Product Name </th>
                    <th>Product Size </th>
                    <th>Product Color</th>
                    <th>Product Qty </th>

                </tr>


                    @foreach($orderDetails['orders_products'] as $product)

                    <tr>
                        <td>
                            <?php   $getProductImage = Product::getProductImage($product['product_id']) ;
                            ?>
                            @if(!empty($getProductImage) && file_exists(public_path('images/product_images/small/'.$getProductImage)))
                           <a target="_blank" href="{{ url('product/'.$product['product_id']) }}">
                               <img style="width: 80px;" src="{{ asset('images/product_images/small/'.$getProductImage) }}" ></a>
                           @else
                           <a target="_blank" href="{{ url('product/'.$product['product_id']) }}">
                               <img style="width: 80px;" src="{{ asset('images/product_images/medium/medium_no_Image.png') }}"   ></a>
                           @endif
                        </td>
                        <td>{{ $product['product_code'] }}</td>
                        <td>{{ $product['product_name'] }}</td>
                        <td>{{ $product['product_size'] }}</td>
                        <td>{{ $product['product_color'] }}</td>
                        <td>{{  $product['product_qty'] }}</td>
                    </tr>

                    @endforeach

                </table>
            </div>
            <div class="span6" class="align-items: centre">
                <div class="col-md-12">
                    <span><h3>Refund Information</h3></span>
                    <form action="{{route('save-refund-info')}}" method="post">
                        @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">Refund Method <span style="color: red">*</span> </label>
                                <select name="refund_method" id="" class="form-control" required style="width: -webkit-fill-available">
                                    <option value="" selected disabled>--select--</option>
                                    <option value="Bkash">Bkash</option>
                                    <option value="Nagad">Nagad</option>
                                    <option value="Rocket">Rocket</option>
                                    <option value="Paypal">Paypal</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Account No <span style="color: red">*</span> </label>
                                <input type="text" class="form-control" name="number" required style="width: -webkit-fill-available">  
                                <input type="hidden" name="order_id" value="{{ $orderDetails['id'] }}">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                    </form>
                </div>
                
            </div>
        </div>

    </div>

@endsection



