

@extends('layouts.front_layout.front_layout')
@section('content')

    <div class="span9">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a> <span class="divider">/</span></li>
            <li class="active">Order</li>
        </ul>
        <h3>Order</h3>
        <hr class="soft"/>
        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
       @endif
        <div class="row">
            <div class="span9">
                <table class="table table-striped table-bordered">
                <tr>
                    <th>#Sl</th>
                    <th>Order Id</th>
                    <th>Order Products</th>
                    <th>O.Status</th>
                    <th>P.Status</th>
                    <th>Payment Method</th>
                    <th>Grand Total</th>
                    <th>Created On</th>
                    <th>Details</th>
                    <th>Action</th>
                </tr>

                    @foreach($orders as $order)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td><a  href="{{ url('orders/'.$order['id']) }}">{{ $order['id'] }}</a></td>
                        <td>
                            @foreach($order['orders_products'] as  $pro)
                            {{ $pro['product_code'] }} ({{ $pro['product_qty'] }})<br>
                            @endforeach
                        </td>
                        <td><b>{{ $order['order_status'] }}</b></td>
                        <td><b>{{ $order['payment_status'] }}</b></td>
                        <td>{{ $order['payment_method'] }}</td>
                        <td>BDT.{{ $order['grand_total'] }}</td>
                        <td>{{  date('d-m-y',strtotime($order['created_at'])) }}</td>
                     <td><a style="text-decoration:underline" href="{{ url('orders/'.$order['id']) }}">view Details</a>
                    </td>
                    <td>
                        @if($order['payment_status']=='Paid' && $order['payment_method']=='Paypal' && $order['order_status'] !='Delivered' && $order['order_status'] !='Shipped' || $order['payment_method']=='SSLCommerz' && $order['order_status'] !='Delivered' && $order['payment_status']=='Paid' && $order['order_status'] !='Shipped')
                            <a href="{{route('refund-order',$order['id'])}}" class="btn btn-sm btn-info">Refund</a>
                        @endif
                        @if($order['payment_status'] =='Due' && $order['order_status'] !='Cancelled' && $order['order_status'] !='Shipped')
                         <a href="{{route('cancel-order',$order['id'])}}" class="btn btn-sm btn-danger">Cancel</a>
                         @if ($order['payment_method']=='SSLCommerz')
                         <a href="{{route('pay-now',$order['id'])}}" class="btn btn-sm btn-warning">Paynow</a></td>
                        @endif
                        </td>
                         
                        @elseif($order['payment_status'] =='Refund')
                         <a href="#" class="btn btn-sm btn-primary" disabled>Processing</a>
                        @elseif($order['order_status'] =='Delivered')
                        <a href="#" class="btn btn-sm btn-success" disabled>Delivered</a>
                        @elseif($order['order_status'] =='Cancelled')
                        <a href="#" class="btn btn-sm btn-danger" disabled>Cancelled</a></td>
                        @elseif($order['payment_status'] =='Success Refund')
                        <a href="#" class="btn btn-sm btn-warning" disabled>S.Refund</a></td>
                        @elseif($order['order_status'] =='Shipped')
                            <a href="#" class="btn btn-sm btn-warning" disabled>Shipped</a></td>

                        @endif
                    </tr>
                    @endforeach

                </table>
            </div>
        </div>

    </div>

@endsection


