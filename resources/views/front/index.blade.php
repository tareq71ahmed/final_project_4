<?php  use App\Product;    ?>
@extends('layouts.front_layout.front_layout')
@section('content')
<style>
    .zoom {
      transition: transform .2s;
      margin: 0 auto;
    }
    
    .zoom:hover {
      -ms-transform: scale(1.5); /* IE 9 */
      -webkit-transform: scale(1.5); /* Safari 3-8 */
      transform: scale(1.5); 
    }
    </style>
    <div class="span9">
        <div class="well well-small">
            <h4>Featured Products <small class="pull-right">{{$featuredItemsCount}}+ featured products</small></h4>
            <div class="row-fluid">
                <div id="featured" @if($featuredItemsCount > 4) class="carousel slide" @endif>
                    <div class="carousel-inner">
                        @foreach($featuredItemsChunk as $key => $featuredItem)
                            <div class="item @if($key==1) active @endif">
                                <ul class="thumbnails">
                                    @foreach($featuredItem as $item)
                                        <li class="span3">
                                            <div class="thumbnail">
                                                <i class="tag"></i>
                                                {{-- <a href="{{route('detailpage',$item['id'])}}"> --}}
                                                    <?php $product_image_path = "images/product_images/small/" . $item['main_image'];    ?>
                                                    @if(!empty($item['main_image']) && file_exists($product_image_path))
                                                        <img class="zoom"
                                                            src="{{asset($product_image_path)}}"
                                                            alt="">
                                                    @else
                                                        <img src="{{asset('images/product_images/small/no_image.png')}}"
                                                             alt="">
                                                    @endif
                                                {{-- </a> --}}
                                                <div class="caption" style="height:130px;">
                                                    <h5>{{$item['product_name']}}</h5>

                                                    <?php $discounted_price = Product::getDiscountedPrice($item['id']);     ?>

                                                    <h4><a class="btn" style="margin-left:52px;"
                                                           href="{{route('detailpage',$item['id'])}}">VIEW</a>

                                                        <span class="pull-right" style="font-size:13px;">
                                                                  @if($discounted_price > 0)
                                                                <del>BDT.{{$item['product_price']}}</del>
                                                                <font color="red"
                                                                      style="font-size:19px;">BDT.{{ $discounted_price }}</font>
                                                            @else
                                                                BDT.{{$item['product_price']}}
                                                            @endif
                                                            </span>
                                                    </h4>

                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                    <a class="left carousel-control" href="#featured" data-slide="prev">‹</a>
                    <a class="right carousel-control" href="#featured" data-slide="next">›</a>
                </div>
            </div>
        </div>
        <h4>Latest Products </h4>
        <ul class="thumbnails">
            @foreach($newProducts as $product)
                <li class="span3">
                    <div class="thumbnail" style="height:347px;">



<!--  //old-->
                     <a href="{{route('detailpage',$product['id'])}}">
                            <?php $product_image_path = "images/product_images/small/" . $product['main_image'];    ?>
                            @if(!empty($product['main_image']) && file_exists($product_image_path))
                                <img class="zoom" style="width:160px;"
                                     src="{{asset($product_image_path)}}"
                                     alt="">
                            @else
                                <img style="width:160px;" src="{{asset('images/product_images/small/no_image.png')}}"
                                     alt="">

                            @endif
                        </a>
<!--//old &ndash;&gt;-->

<!--    //new start-->
<!--    <div id="gallery" class="span3" >

        <?php $product_image_path = "images/product_images/large/" . $product['main_image'];    ?>
        @if(!empty($product['main_image']) && file_exists($product_image_path))

            <a  href="{{asset($product_image_path)}}"
               title="{{$product['product_name']}}">
                <img src="{{asset($product_image_path)}}"
                     style="width:160px;" alt="Blue Casual T-Shirt"/>

            </a>
        @else

            <a href="{{asset('images/product_images/medium/medium_no_Image.png')}}"
               title="{{$product['product_name']}}">
                <img src="{{asset('images/product_images/medium/medium_no_Image.png')}}"
                     style="width:160px;" alt="Blue Casual T-Shirt"/>
            </a>

        @endif
    </div>-->

<!--    //new End-->


                        <div class="caption">
                            <h5>{{$product['product_name']}}</h5>
                            <p>
                                {{$product['product_code']}} ({{$product['product_color']}})
                            </p>

                            <?php $discounted_price = Product::getDiscountedPrice($product['id']);     ?>

                            <h4 style="text-align:center">

                                <a class="btn" href="{{route('detailpage',$product['id'])}}">Add to<i class="icon-shopping-cart"></i></a>
                                <a class="btn btn-primary" href="{{route('detailpage',$product['id'])}}">

                                    @if($discounted_price > 0)
                                        <del>BDT.{{$product['product_price']}}</del>&nbsp;&nbsp;<font color="yellow">BDT.{{ $discounted_price }}</font>
                                    @else
                                        BDT.{{$product['product_price']}}
                                    @endif
                                </a>
                            </h4>
                        </div>
                    </div>
                </li>
            @endforeach
<!--              <div class="d-flex justify-content-center">-->
                    {{$newProducts->links()}}
               {{-- </div>--}}
        </ul>
    </div>
@endsection
