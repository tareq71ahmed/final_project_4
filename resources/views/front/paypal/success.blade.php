@extends('layouts.front_layout.front_layout')
@section('content')

    <div class="span9">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a> <span class="divider">/</span></li>
            <li class="active">Confirmed</li>
        </ul>
        <h3>Confirmed</h3>
        <hr class="soft"/>

        <div align="center">
            <h3>YOUR ORDER HAS BEEN CONFIRMED</h3>
            <p>Thanks For Payment.We Will Process Your Order very soon</p>
            <p>Your Order Number Is <strong>{{Session::get('order_id')}}</strong> And total Paid Amount is <strong>BDT.{{ Session::get('grand_total') }}</strong>
            </p>
        </div>

    </div>
@endsection

<?php
Session::forget('grand_total');
Session::forget('order_id');
Session::forget('couponCode');
Session::forget('couponAmount');

?>


