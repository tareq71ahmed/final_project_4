

@extends('layouts.front_layout.front_layout')
@section('content')

    <div class="span9">
        <ul class="breadcrumb">
            <li><a href="{{url('/')}}">Home</a> <span class="divider">/</span></li>
            <li class="active">THANKS</li>
        </ul>
        <h3>THANKS</h3>
        <hr class="soft"/>

        <div align="center">
            <h3>YOUR ORDER HAS BEEN PLACED</h3>
            <p>Your Order Number Is <strong>{{Session::get('order_id')}}</strong> And total payable Amount is <strong>BDT.{{ Session::get('grand_total') }}</strong></p>
             <p>please Make payment within 2 hours otherwise Your order will be cancel.</p>
             <p>please Make payment by clicking on below Payment Button</p>
             <div>
                <button class="your-button-class" id="sslczPayBtn"
                    token="if you have any token validation"
                    postdata="your javascript arrays or objects which requires in backend"
                    order="If you already have the transaction generated for current order"
                    endpoint="/pay-via-ajax"> Pay Now
                </button>
            </div>
        </div>

    </div>
@endsection
<script>
    (function (window, document) {
        var loader = function () {
            var script = document.createElement("script"), tag = document.getElementsByTagName("script")[0];
            script.src = "https://sandbox.sslcommerz.com/embed.min.js?" + Math.random().toString(36).substring(7);
            tag.parentNode.insertBefore(script, tag);
        };

        window.addEventListener ? window.addEventListener("load", loader, false) : window.attachEvent("onload", loader);
    })(window, document);
</script>
<?php
Session::forget('couponCode');
Session::forget('couponAmount');
?>


