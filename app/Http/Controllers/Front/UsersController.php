<?php

namespace App\Http\Controllers\Front;

use App\Cart;
use App\Country;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{

    public function loginRegister()
    {

        return view('front.users.login_register');
    }

    public function registerUser(Request $request)
    {
        if ($request->isMethod('post')) {

            Session::forget('error_message');
            Session::forget('success_message');
            $data = $request->all();
            $userCount = User::where('email', $data['email'])->count();
            if ($userCount > 0) {
                $message = "Email is already registered";
                Session::flash('error_message', $message);
                return redirect()->back();
            } else {
                $user = new User;
                $user->name = $data['name'];
                $user->mobile = $data['mobile'];
                $user->email = $data['email'];
                $user->password = bcrypt($data['password']);
                $user->status = 0;
                $user->save();
                //send confirmation Email
                $email = $data['email'];
                $messageData = [
                    'email' => $data['email'],
                    'name' => $data['name'],
                    'code' => base64_encode($data['email']),
                ];
                Mail::send('emails.confirmation', $messageData, function ($message) use ($email) {
                    $message->to($email)->subject('Confirm Your Account');
                });
                $message = "Please confirm Your E-Mail To Activate Your Account";
                Session::put('success_message', $message);
                return redirect()->back();
                //send confirmation Email End

            }

        }
    }

    public function confirmAccount($email)
    {
        Session::forget('error_message');
        Session::forget('success_message');
        $email = base64_decode($email);
        $userCount = User::where('email', $email)->count();
        if ($userCount > 0) {
            $userDetails = User::where('email', $email)->first();
            if ($userDetails->status == 1) {
                $message = "Your Account is already Activated! Ps Login To Access";
                Session::put('success_message', $message);
                return redirect('login-register');
            } else {

                User::where('email', $email)->update(['status' => 1]);

                //send sms to Register SMS
                /*
                $message = "Dear Customer !You Have been Successfully registered with E-com Website.Login to Your Account to access Orders And avaliable Offer";
                $mobile = $userDetails['mobile'];
                Sms::sendSms($message,$mobile);
                 */
                //send sms to Register SMS END

                //Send Register Email

                $messageData = ['name' => $userDetails['name'], 'mobile' => $userDetails['mobile'], 'email' => $email];
                Mail::send('emails.register', $messageData, function ($message) use ($email) {
                    $message->to($email)->subject('Welcome To E-commerce Website');
                });
                $message = "Your Email Account is Activated.You Can Login Now";
                Session::put('success_message', $message);
                return redirect('login-register');
            }
        } else {
            abort(404);
        }

    }

    public function checkEmail(Request $request)
    {
        $data = $request->all();
        $emailCount = User::where('email', $data['email'])->count();
        if ($emailCount > 0) {
            return "false";
        } else {
            return "true";
        }

    }

    public function loginUser(Request $request)
    {
        if ($request->isMethod('post')) {
            Session::forget('error_message');
            Session::forget('success_message');
            $data = $request->all();
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
                //Check Email Activated or not
                $userStatus = User::where('email', $data['email'])->first();
                if ($userStatus->status == 0) {
                    Auth::logout();
                    $message = "Account is not activated ! Ps check Your Email To Activate Your Account";
                    Session::put('error_message', $message);
                    return redirect()->back();
                }
                //Check Email Activated or not End
                if (!empty(Session::get('session_id'))) {
                    $user_id = Auth::user()->id;
                    $session_id = Session::get('session_id');
                    Cart::where('session_id', $session_id)->update(['user_id' => $user_id]);
                }
                return redirect('/cart');
            } else {
                $message = "Invalid email or password";
                Session::flash('error_message', $message);
                return redirect()->back();
            }
        }
    }

    public function logoutUser()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    public function forgotpassword(Request $request)
    {

        if ($request->isMethod('post')) {
            $data = $request->all();
            $emailCount = User::where('email', $data['email'])->count();
            if ($emailCount == 0) {
                $message = "Email does not exist!";
                Session::put('error_message', $message);
                Session::forget('success_message');
                return redirect()->back();
            }
            $random_password = str_random(8);
            $new_password = bcrypt($random_password);
            User::where('email', $data['email'])->update(['password' => $new_password]);
            $userName = User::select('name')->where('email', $data['email'])->first();
            $email = $data['email'];
            $name = $userName['name'];
            $messageData = [
                'email' => $email,
                'name' => $name,
                'password' => $random_password,
            ];
            Mail::send('emails.forgot_password', $messageData, function ($message) use ($email) {
                $message->to($email)->subject('New Password E-commerce Website');
            });
            $message = "Please Check Your Email Address to get Password";
            Session::put('success_message', $message);
            Session::forget('error_message');
            return redirect('login-register');

        }

        return view('front.users.forgot_password');
    }

    public function account(Request $request)
    {
        $user_id = Auth::user()->id;
        $userDetails = User::find($user_id)->toArray();
        $countries = Country::get()->toArray();
        if ($request->isMethod('post')) {
            $data = $request->all();
            Session::forget('error_message');
            Session::forget('success_message');
            $rules = [
                'name' => 'required|regex:/^[\pL\s\-]+$/u',
                'mobile' => 'required|numeric',
            ];
            $customMessages = [
                'name.required' => "Name is Required!",
                'name.regex' => "Valid Name is Required!",
                'mobile.required' => "Mobile Number is Required!",
                'mobile.numeric' => "Valid Mobile Number is Required!",
            ];
            $this->validate($request, $rules, $customMessages);

            $user = User::find($user_id);
            $user->name = $data['name'];
            $user->address = $data['address'];
            $user->city = $data['city'];
            $user->state = $data['state'];
            $user->country = $data['country'];
            $user->pincode = $data['pincode'];
            $user->mobile = $data['mobile'];
            $user->save();
            $message = "Your Account details is Successfully Updated";
            Session::put('success_message', $message);
            return redirect()->back();
        }
        return view('front.users.account', compact('userDetails', 'countries'));
    }

    public function chkUserPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            $user_id = Auth::user()->id;
            $chkPassword = User::select('password')->where('id', $user_id)->first();
            if (Hash::check($data['current_pwd'], $chkPassword->password)) {
                return "true";
            } else {
                return "false";
            }
        }
    }

    public function updateUserPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            $user_id = Auth::user()->id;
            $chkPassword = User::select('password')->where('id', $user_id)->first();
            if (Hash::check($data['current_pwd'], $chkPassword->password)) {
                $new_pwd = bcrypt($data['new_pwd']);
                User::where('id', $user_id)->update(['password' => $new_pwd]);
                /*$message = "Password Updated successfully.";
                Session::put('success_message',$message);
                Session::forget('error_message');*/
                return redirect()->back()->with('success_message', 'Password Updated successfully');

            } else {
                /* $message = "Current Password is incorrect.";
                Session::put('error_message',$message);
                Session::forget('success_message');*/
                return redirect()->back()->with('error_message', 'Password Updated successfully');
            }
        }
    }

}
