<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Product;

class IndexController extends Controller
{

    public function index()
    {
        $featuredItemsCount = Product::where('is_featured', 'Yes')->where('status', 1)->count();
        $featuredItems = Product::where('is_featured', 'Yes')->where('status', 1)->get()->toArray();
        $featuredItemsChunk = array_chunk($featuredItems, 4);
        $page_name = "index";
       // $newProducts = Product::orderBy('id', 'Desc')->where('status', 1)->limit(6)->get()->toArray();

       $newProducts = Product::orderBy('id', 'Desc')->where('status', 1)->paginate(6);
        //dd($newProducts);
        return view('front.index', compact('page_name', 'featuredItemsChunk', 'featuredItemsCount', 'newProducts'));
    }

}
