<?php

namespace App\Http\Controllers\Front;

use App\CmsPage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class CmsController extends Controller
{

    public function cmsPage()
    {
        $currentRoute = url()->current();
        $currentRoute = str_replace("http://127.0.0.1:8000/", "", $currentRoute);
        $cmsRoutes = CmsPage::where('status', 1)->get()->pluck('url')->toArray();
        if (in_array($currentRoute, $cmsRoutes)) {
            $cmsPageDetails = CmsPage::where('url', $currentRoute)->first()->toArray();
            return view("front.pages.cms_page", compact('cmsPageDetails'));
        } else {
            abort(404);
        }

    }

    public function contact(Request $request)
    {

        if ($request->isMethod('post')) {
            $data = $request->all();
            $rules = [
                "name" => "required",
                "email" => "email|required",
                "subject" => "required",
                "message" => "required",
            ];
            $customMessages = [
                'name.required' => "Name is Required",
                'email.email' => "Email Must be Valid",
                'email.required' => "Email is Required",
                'subject.required' => "Subject is Required",
                'message.required' => "Message is Required",
            ];

            // $this->validate( $request, $rules, $customMessages );
            $validator = Validator::make($data, $rules, $customMessages);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $email = "tareq@gmail.com";
            $messageData = [
                'name' => $data['name'],
                'email' => $data['email'],
                'subject' => $data['subject'],
                'comment' => $data['message'],
            ];
            Mail::send('emails.enquiry', $messageData, function ($message) use ($email) {
                $message->to($email)->subject('Enquiry From AmazonBd');
            });
            $message = "Thank You For Your Enquiry! We will Back You Soon";
            Session::flash('success_message', $message);
            return redirect()->back();
        }

        return view('front.pages.contact');
    }

}
