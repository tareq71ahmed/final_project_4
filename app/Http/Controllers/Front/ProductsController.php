<?php

namespace App\Http\Controllers\Front;

use App\Brand;
use App\Cart;
use App\Category;
use App\CodPincodes;
use App\Country;
use App\Coupon;
use App\DeliveryAddress;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrdersProduct;
use App\OtherSetting;
use App\PrepaidPincodes;
use App\Product;
use App\ProductsAttribute;
use App\Rating;
use App\ShippingCharge;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class ProductsController extends Controller {
    public function listing( Request $request ) {
        Paginator::useBootstrap();
        if ( $request->ajax() ) {
            $data          = $request->all();
            $url           = $data['url'];
            $categoryCount = Category::where( ['url' => $url, 'status' => 1] )->count();
            if ( $categoryCount > 0 ) {
                $categoryDetails  = Category::catDetails( $url );
                $categoryProducts = Product::with( 'brand' )->whereIn( 'category_id', $categoryDetails['catIds'] )
                    ->where( 'status', 1 );

                //If Brand Filter have been selected
                if ( isset( $data['brand'] ) && !empty( $data['brand'] ) ) {
                    $brandIds = Brand::select( 'id' )->whereIn( 'name', $data['brand'] )->pluck( 'id' );
                    $categoryProducts->whereIn( 'products.brand_id', $brandIds );
                }

                //If Fabric Filter have been selected
                if ( isset( $data['fabric'] ) && !empty( $data['fabric'] ) ) {
                    $categoryProducts->whereIn( 'products.fabric', $data['fabric'] );
                }

                //If Sleeve Filter have been selected
                if ( isset( $data['sleeve'] ) && !empty( $data['sleeve'] ) ) {
                    $categoryProducts->whereIn( 'products.sleeve', $data['sleeve'] );
                }

                //If Pattern Filter have been selected
                if ( isset( $data['pattern'] ) && !empty( $data['pattern'] ) ) {
                    $categoryProducts->whereIn( 'products.pattern', $data['pattern'] );
                }

                //If Fit Filter have been selected
                if ( isset( $data['fit'] ) && !empty( $data['fit'] ) ) {
                    $categoryProducts->whereIn( 'products.fit', $data['fit'] );
                }

                //If occasion Filter have been selected
                if ( isset( $data['occasion'] ) && !empty( $data['occasion'] ) ) {
                    $categoryProducts->whereIn( 'products.occasion', $data['occasion'] );
                }

                if ( isset( $data['sort'] ) && !empty( $data['sort'] ) ) {
                    if ( $data['sort'] == "product_latest" ) {
                        $categoryProducts->orderBy( 'id', 'desc' );
                    } else if ( $data['sort'] == "product_name_a_z" ) {
                        $categoryProducts->orderBy( 'product_name', 'Asc' );
                    } else if ( $data['sort'] == "product_name_z_a" ) {
                        $categoryProducts->orderBy( 'product_name', 'desc' );
                    } else if ( $data['sort'] == "price_lowest" ) {
                        $categoryProducts->orderBy( 'product_price', 'Asc' );
                    } else if ( $data['sort'] == "product_highest" ) {
                        $categoryProducts->orderBy( 'product_price', 'desc' );
                    }

                } else {
                    $categoryProducts->orderBy( 'id', 'desc' );
                }

                $categoryProducts = $categoryProducts->paginate( 5 );

                return view( 'front.products.ajax_products_listing' )->with( compact( 'categoryDetails', 'categoryProducts', 'url' ) );
            } else {
                abort( 404 );
            }

        } else {

            $url           = Route::getFacadeRoot()->current()->uri();
            $categoryCount = Category::where( ['url' => $url, 'status' => 1] )->count();

            if ( isset( $_REQUEST['search'] ) && !empty( $_REQUEST['search'] ) ) {
                $search_product                                 = $_REQUEST['search'];
                $categoryDetails['breadcrumbs']                 = $search_product;
                $categoryDetails['catDetails']['category_name'] = $search_product;
                $categoryDetails['catDetails']['description']   = "Search Result For " . $search_product;
                $categoryProducts                               = Product::with( 'brand' )->join( 'categories', 'categories.id', '=', 'products.category_id' )->select('products.*','categories.category_name')->where( function ( $query ) use ( $search_product ) {
                    $query->where( 'products.product_name', 'like', '%' . $search_product . '%' )
                        ->orwhere( 'products.product_code', 'like', '%' . $search_product . '%' )
                        ->orwhere( 'products.product_color', 'like', '%' . $search_product . '%' )
                        ->orwhere( 'products.description', 'like', '%' . $search_product . '%' )
                        ->orwhere( 'categories.category_name', 'like', '%' . $search_product . '%' );
                } )->where( 'products.status', 1 );
                $categoryProducts = $categoryProducts->get();
                $page_name        = "Search Result";
                return view( 'front.products.listing' )->with( compact( 'categoryDetails', 'categoryProducts', 'page_name' ) );

            } else if ( $categoryCount > 0 ) {
                $categoryDetails  = Category::catDetails( $url );
                $categoryProducts = Product::with( 'brand' )->whereIn( 'category_id', $categoryDetails['catIds'] )
                    ->where( 'status', 1 );
                $categoryProducts = $categoryProducts->paginate( 5 );
                $productFilters   = Product::productFilters();
                $fabricArray      = $productFilters['fabricArray'];
                $sleeveArray      = $productFilters['sleeveArray'];
                $patternArray     = $productFilters['patternArray'];
                $fitArray         = $productFilters['fitArray'];
                $occasionArray    = $productFilters['occasionArray'];
                //Brand Filter
                $brandArray = Brand::select( 'name' )->where( 'status', 1 )->pluck( 'name' );

                $page_name = "listing";

                return view( 'front.products.listing' )->with( compact( 'categoryDetails', 'categoryProducts', 'url', 'brandArray', 'fabricArray', 'sleeveArray', 'patternArray', 'fitArray', 'occasionArray', 'page_name' ) );
            } else {
                abort( 404 );
            }

        }
    }

    public function detail( $id ) {
        $productDetails = Product::with( ['category', 'brand', 'attributes' => function ( $query ) {
            $query->where( 'status', 1 );
        }, 'images'] )->find( $id )->toArray();
      //  dd($productDetails);
        $total_stock     = ProductsAttribute::where( 'product_id', $id )->sum( 'stock' );
        $relatedProducts = Product::where( 'category_id', $productDetails['category']['id'] )->where( 'id', '!=', $id )->limit( 3 )->inRandomOrder()->get()->toArray();
        $groupProducts   = array();
        if ( !empty( $productDetails['group_code'] ) ) {
            $groupProducts = Product::select( 'id', 'main_image' )->where( 'id', '!=', $id )->where( ['group_code' => $productDetails['group_code'], 'status' => 1] )->get()->toArray();

        }


        //Ratings Start
        $ratings = Rating::with('user')->where('status',1)->where('product_id',$id)->orderBy('id','Desc')->get()->toArray();
        $ratingsSum = Rating::where('status',1)->where('product_id',$id)->sum('rating');
        $ratingCount = Rating::where('status',1)->where('product_id',$id)->count();
        if($ratingCount>0){
            $avgRating = round($ratingsSum /$ratingCount,2) ;
            $avgStarRating = round($ratingsSum /$ratingCount) ;
        }else{
            $avgRating =0;
            $avgStarRating =0;
        }
        //Ratings End

        return view( 'front.products.detail', compact( 'productDetails', 'total_stock', 'relatedProducts', 'groupProducts','ratings','avgRating','avgStarRating' ) );
    }

    public function getproductprice( Request $request ) {
        if ( $request->ajax() ) {
            $data                   = $request->all();
            $getDiscountedAttrPrice = Product::getDiscountedAttrPrice( $data['product_id'], $data['size'] );
            return $getDiscountedAttrPrice;
        }
    }

    public function addtocart( Request $request ) {
        if ( $request->isMethod( 'post' ) ) {
            $data = $request->all();


            if ( $data['quantity'] <= 0 || $data['quantity'] == "" ) {
                $data['quantity'] = 1;
            }


            if ( empty( $data['size'] ) ) {
                $message = "Ps! Select Size";
                Session::flash( 'error_message', $message );
                return redirect()->back();
            }

            //Check Product Stock is Avliable or Not
            $getproductstock = ProductsAttribute::where( ['product_id' => $data['product_id'], 'size' => $data['size']] )->first()->toArray();

           // dd($data['quantity']);

            if ( $getproductstock['stock'] < $data['quantity'] ) {
                $message = "Required Product is Not Available";
                Session::flash( 'error_message', $message );
                return redirect()->back();
            }


            //Generate Session Id
            $session_id = Session::get( 'session_id' );
            if ( empty( $session_id ) ) {
                $session_id = Session::getId();
                Session::put( 'session_id', $session_id );

            }

            //check Product if Already Exists in Cart
            if ( Auth::check() ) {
                //User Logged In
                $countproduct = Cart::where( ['product_id' => $data['product_id'], 'size' => $data['size'], 'user_id' => Auth::user()->id] )->count();

            } else {
                //User Not Logged In
                $countproduct = Cart::where( ['product_id' => $data['product_id'], 'size' => $data['size'], 'session_id' => Session::get( 'session_id' )] )->count();
            }

            if ( $countproduct > 0 ) {

                $message = "Product Already Added in the Cart";
                session()->flash( 'error_message', $message );
                return redirect()->back();
            }

            if ( Auth::check() ) {
                $user_id = Auth::user()->id;
            } else {
                $user_id = 0;
            }

            //rrrrrrrrrr
            Session::forget( 'couponAmount' );
            Session::forget( 'couponCode' );

            //Save Product In cart
            $cart             = new Cart;
            $cart->session_id = $session_id;
            $cart->user_id    = $user_id;
            $cart->product_id = $data['product_id'];
            $cart->size       = $data['size'];
            $cart->quantity   = $data['quantity'];
            $cart->save();
            $message = "Product Added Successfully in Cart";
            session()->flash( 'success_message', $message );
            return redirect()->back();
        }
    }

    public function cart() {
        $userCartItems = Cart::userCartItems();
        return view( 'front.products.cart', compact( 'userCartItems' ) );
    }

    public function updateCartItemQty( Request $request ) {
        if ( $request->ajax() ) {

            //rrrrrrrrrr
            Session::forget( 'couponAmount' );
            Session::forget( 'couponCode' );

            $data = $request->all();

            $cartDetails   = Cart::find( $data['cartid'] );
            $avliableStock = ProductsAttribute::select( 'stock' )
                ->where( ['product_id' => $cartDetails['product_id'], 'size' => $cartDetails['size']] )->first()->toArray();

            if ( $data['qty'] > $avliableStock['stock'] ) {
                $userCartItems = Cart::userCartItems();
                return response()->json( [
                    'status'  => false,
                    'message' => 'Product Stock Is Not Available',
                    'view'    => (string) View::make( 'front.products.cart_items' )->with( compact( 'userCartItems' ) ),
                ] );
            }

            $availableSize = ProductsAttribute::where( [
                'product_id' => $cartDetails['product_id'],
                'size'       => $cartDetails['size'], 'status' => 1,
            ] )->count();

            if ( $availableSize == 0 ) {
                $userCartItems = Cart::userCartItems();
                return response()->json( [
                    'status'  => false,
                    'message' => 'Product Size Is Not Available',
                    'view'    => (string) View::make( 'front.products.cart_items' )->with( compact( 'userCartItems' ) ),
                ] );

            }

            Cart::where( 'id', $data['cartid'] )->update( ['quantity' => $data['qty']] );
            $userCartItems  = Cart::userCartItems();
            $totalCartItems = totalCartItems();
            return response()->json( [
                'status'         => true,
                'totalCartItems' => $totalCartItems,
                'view'           => (string) View::make( 'front.products.cart_items' )->with( compact( 'userCartItems' ) ),
            ] );
        }
    }

    public function deleteCartItem( Request $request ) {
        if ( $request->ajax() ) {
            //rrrrrrrrrr
            Session::forget( 'couponAmount' );
            Session::forget( 'couponCode' );

            $data = $request->all();
            Cart::where( 'id', $data['cartid'] )->delete();
            $userCartItems  = Cart::userCartItems();
            $totalCartItems = totalCartItems();
            return response()->json( [
                'totalCartItems' => $totalCartItems,
                'view'           => (string) View::make( 'front.products.cart_items' )->with( compact( 'userCartItems' ) ),
            ] );
        }
    }

    public function applyCoupon( Request $request ) {
        if ( $request->ajax() ) {
            $data          = $request->all();
            $userCartItems = Cart::userCartItems();
            $couponCount   = Coupon::where( 'coupon_code', $data['code'] )->count();
            if ( $couponCount == 0 ) {
                $userCartItems  = Cart::userCartItems();
                $totalCartItems = totalCartItems();
                //rrrrrrrrrr
                Session::forget( 'couponAmount' );
                Session::forget( 'couponCode' );
                return response()->json( [
                    'status'         => false,
                    'message'        => 'This coupon code is Not Valid',
                    'totalCartItems' => $totalCartItems,
                    'view'           => (string) View::make( 'front.products.cart_items' )->with( compact( 'userCartItems' ) ),
                ] );

            } else {

                //Check for the coupon Condition
                //Get Coupon Details
                $couponDetails = Coupon::where( 'coupon_code', $data['code'] )->first();
                //check if Coupon is Inactive
                if ( $couponDetails->status == 0 ) {
                    $message = "This coupon is not Active";
                }
                //check if Coupon is Expeired
                $expiry_date  = $couponDetails->expiry_date;
                $current_date = date( 'Y-m-d' );
                if ( $expiry_date < $current_date ) {
                    $message = "Coupon is expired";
                }

                //Check if Coupon is for Single or Multiple Times
                if ( $couponDetails->coupon_type == "Single Times" ) {
                    //Check in Orders table  if Coupon Already Avliable by the User
                    $couponCount = Order::where( ['coupon_code' => $data['code'], 'user_id' => Auth::user()->id] )->count();
                    if ( $couponCount >= 1 ) {
                        $message = "This coupon is already Availaled by You";

                    }

                }
                //Check if Coupon is for Single or Multiple Times End

                //check if Coupon is from Selected Categories
                //Get All Selected Categories From Coupon
                $catArr = explode( ",", $couponDetails->categories );
                //Get Cart Items
                $userCartItems = Cart::userCartItems();
                //Checked if Coupon Belong To   LogIn User
                //Get All Selected Users of Copupon

                if ( !empty( $couponDetails->users ) ) {
                    $usersArr = explode( ",", $couponDetails->users );
                    //Get User ID's of All Selected users
                    foreach ( $usersArr as $key => $user ) {
                        $getUserID = User::select( 'id' )->where( 'email', $user )->first()->toArray();
                        $userID[]  = $getUserID['id'];
                    }

                }

                //Get Cart Total amount

                $total_amount = 0;

                foreach ( $userCartItems as $key => $item ) {
                    if ( !in_array( $item['product']['category_id'], $catArr ) ) {
                        $message = "This Coupon Code is not for one of the selected products.";
                    }

                    if ( !empty( $couponDetails->users ) ) {
                        if ( !in_array( $item['user_id'], $userID ) ) {
                            $message = "This Coupon Code is not for You!";
                        }
                    }

                    $attrPrice    = Product::getDiscountedAttrPrice( $item['product_id'], $item['size'] );
                    $total_amount = $total_amount + ( $attrPrice['final_price'] * $item['quantity'] );

                }

                if ( isset( $message ) ) {
                    $userCartItems  = Cart::userCartItems();
                    $totalCartItems = totalCartItems();
                    return response()->json( [
                        'status'         => false,
                        'message'        => $message,
                        'totalCartItems' => $totalCartItems,
                        'view'           => (string) View::make( 'front.products.cart_items' )->with( compact( 'userCartItems' ) ),
                    ] );
                } else {
                    //Check if Amount type is  Fixed or Percentage
                    if ( $couponDetails->amount_type == "Fixed" ) {
                        $couponAmount = $couponDetails->amount;
                    } else {
                        $couponAmount = $total_amount * ( $couponDetails->amount / 100 );
                    }
                    $grand_total = $total_amount - $couponAmount;

                    //Add Coupon Code  & Amount in Session
                    Session::put( 'couponAmount', $couponAmount );
                    Session::put( 'couponCode', $data['code'] );
                    $message        = "Coupon Code successfully Applied.You Are Availing Discount!";
                    $totalCartItems = totalCartItems();
                    $userCartItems  = Cart::userCartItems();
                    return response()->json( [
                        'status'         => true,
                        'message'        => $message,
                        'totalCartItems' => $totalCartItems,
                        'couponAmount'   => $couponAmount,
                        'grand_total'    => $grand_total,
                        'view'           => (string) View::make( 'front.products.cart_items' )->with( compact( 'userCartItems' ) ),
                    ] );

                }

            }

        }
    }

    public function checkout( Request $request ) {
        $userCartItems = Cart::userCartItems();

        if ( count( $userCartItems ) == 0 ) {
            $message = "Shopping Cart is Empty! Ps Add Product to Checkout";
            Session::put( 'error_message', $message );
            return redirect( 'cart' );
        }

        $total_price  = 0;
        $total_weight = 0;
        foreach ( $userCartItems as $item ) {

            $product_weight = ( $item['product']['product_weight'] ) * $item['quantity'];
            // $product_weight = $item['product']['product_weight'] ;
            $total_weight = $total_weight + $product_weight;
            $attrPrice    = Product::getDiscountedAttrPrice( $item['product_id'], $item['size'] );
            $total_price  = $total_price + ( $attrPrice['final_price'] * $item['quantity'] );
        }

        //Min/max Cart Amount
        $otherSettings = OtherSetting::where( 'id', 1 )->first()->toArray();
        //Minimim Cart Amount
        if ( $total_price < $otherSettings['min_cart_value'] ) {
            $message = "Max Cart Amount Must be BDT." . $otherSettings['min_cart_value'];
            Session::put( 'error_message', $message );
            return redirect()->back();
        }
        //Minimim Cart Amount End

        //Max Cart Amount
        if ( $total_price > $otherSettings['max_cart_value'] ) {
            $message = "Max Cart Amount Must be BDT." . $otherSettings['max_cart_value'];
            Session::put( 'error_message', $message );
            return redirect()->back();
        }
        //Max Cart Amount End
        //Min/max Cart Amount End

        $deliveryAddresses = DeliveryAddress::deliveryAddresses();
        foreach ( $deliveryAddresses as $key => $value ) {
            $shippingCharges                             = ShippingCharge::getShippingCharges( $total_weight, $value['country_name'] );
            $deliveryAddresses[$key]['shipping_charges'] = $shippingCharges;

            //check if Delivery Address  Pincode Exists in COD Pincodes
            $deliveryAddresses[$key]['codpincodeCount'] = CodPincodes::where( 'pincode', $value['pincode'] )->count();
            //check if Delivery Address   Pincode Exists in Prepaid Pincodes
            $deliveryAddresses[$key]['prepaidpincodeCount'] = PrepaidPincodes::where( 'pincode', $value['pincode'] )->count();

        }
        /*  echo "<pre>";print_r($deliveryAddresses); die;*/

        if ( $request->isMethod( 'post' ) ) {
            $data = $request->all();

            /* echo "<pre>";print_r($userCartItems); die ;*/

            //Website Sequrity Check
            foreach ( $userCartItems as $key => $cart ) {
                //Prevent Disbaled Cart To Order
                $product_status = Product::getProductStatus( $cart['product_id'] );
                if ( $product_status == 0 ) {
                    /*Product::deleteCartProduct($cart['product_id']);*/
                    $message = ( $cart['product']['product_name'] ) . " is not Available.So ps removed from Cart";
                    Session::flash( 'error_message', $message );
                    return redirect( '/cart' );
                }

                //Prevent Out of Stock Product to Order
                $product_stock = Product::getProductStock( $cart['product_id'], $cart['size'] );
                if ( $product_stock == 0 ) {
                    /*Product::deleteCartProduct($cart['product_id']);*/
                    $message = ( $cart['product']['product_name'] ) . " is not Available.So ps removed from Cart";
                    Session::flash( 'error_message', $message );
                    return redirect( '/cart' );
                }
                //Prevent Out of Stock Product to Order End

                //Prevent Disbaled or Delete Attributes to Order
                $getAttributeCount = Product::getAttributeCount( $cart['product_id'], $cart['size'] );
                if ( $getAttributeCount == 0 ) {
                    /*Product::deleteCartProduct($cart['product_id']);*/
                    $message = ( $cart['product']['product_name'] ) . " is not Available.So ps removed from Cart";
                    Session::flash( 'error_message', $message );
                    return redirect( '/cart' );
                }
                //Prevent Disbaled or Delete Attributes to Order End

                //Prevent Disbaled Category  to Order
                $category_status = Product::getCategoryStatus( $cart['product']['category_id'] );
                if ( $category_status == 0 ) {
                    /*Product::deleteCartProduct($cart['product_id']);*/
                    $message = ( $cart['product']['product_name'] ) . " is not Available.So ps removed from Cart";
                    Session::flash( 'error_message', $message );
                    return redirect( '/cart' );
                }
                //Prevent Disbaled Category  to Order End

            }
            //Website Sequrity Check End

            if ( empty( $data['address_id'] ) ) {
                $message = "Please Select Delivery Address";
                session::flash( 'error_message', $message );
                return redirect()->back();
            }

            if ( empty( $data['payment_getway'] ) ) {
                $message = "Please Select Payment Method";
                session::flash( 'error_message', $message );
                return redirect()->back();
            }

            if ( $data['payment_getway'] == "COD" ) {
                $payment_method = "COD";
                // $order_status   = "New";
            } elseif($data['payment_getway'] == "Paypal") {
                $payment_method = "Paypal";
                // $order_status   = "Pending";
            }else{
                $payment_method = "SSLCommerz";
            }

            //Get Delivery Address From address_id*
            $deliveryAddress = DeliveryAddress::where( 'id', $data['address_id'] )->first()->toArray();
            // Get shipping Charges
            $shipping_charges = ShippingCharge::getShippingCharges( $total_weight, $deliveryAddress['country_name'] );

            //Calculate Grand Total
            $grand_total = $total_price + $shipping_charges - Session::get( 'couponAmount' );
            //Insert Grand Total in session variable
            Session::put( 'grand_total', $grand_total );

            DB::beginTransaction();

            //Insert Orders Details
            $order                   = new Order;
            $order->user_id          = Auth::user()->id;
            $order->name             = $deliveryAddress['name'];
            $order->address          = $deliveryAddress['address'];
            $order->city             = $deliveryAddress['city'];
            $order->state            = $deliveryAddress['state'];
            $order->country          = $deliveryAddress['country_name'];
            $order->pincode          = $deliveryAddress['pincode'];
            $order->mobile           = $deliveryAddress['mobile'];
            $order->email            = Auth::user()->email;
            $order->shipping_charges = $shipping_charges;
            $order->coupon_code      = Session::get( 'couponCode' );
            $order->coupon_amount    = Session::get( 'couponAmount' );
            $order->payment_status     = "Due";
            $order->order_status     = "New";
            $order->payment_method   = $payment_method;
            $order->payment_getway   = $data['payment_getway'];
            $order->grand_total      = Session::get( 'grand_total' );
            $order->save();

            //Get Last Order Id
            $order_id = DB::getPdo()->lastInsertId();
            //Get User Cart Item
            $cartItems = Cart::where( 'user_id', Auth::user()->id )->get()->toArray();

            foreach ( $cartItems as $key => $item ) {
                $cartItem                = new OrdersProduct;
                $cartItem->order_id      = $order_id;
                $cartItem->user_id       = Auth::user()->id;
                $getProductDetails       = Product::select( 'product_code', 'product_name', 'product_color' )->where( 'id', $item['product_id'] )->first()->toArray();
                $cartItem->product_id    = $item['product_id'];
                $cartItem->product_code  = $getProductDetails['product_code'];
                $cartItem->product_name  = $getProductDetails['product_name'];
                $cartItem->product_color = $getProductDetails['product_color'];
                $cartItem->product_size  = $item['size'];
                $getDiscountedAttrPrice  = Product::getDiscountedAttrPrice( $item['product_id'], $item['size'] );
                $cartItem->product_price = $getDiscountedAttrPrice['final_price'];
                $cartItem->product_qty   = $item['quantity'];
                $cartItem->save();
                if ( $data['payment_getway'] == "COD" ) {
                    //Reduce stock Management
                    $getProductStock = ProductsAttribute::where( ['product_id' => $item['product_id'], 'size' => $item['size']] )->first()->toArray();
                    $newStock        = $getProductStock['stock'] - $item['quantity'];
                    ProductsAttribute::where( ['product_id' => $item['product_id'], 'size' => $item['size']] )->update( ['stock' => $newStock] );
                    //Reduce stock Management End
                }

            }

            //Empty The  User Cart
            Cart::where( 'user_id', Auth::user()->id )->delete();

            //Insert Order Id In dsession Variable
            Session::put( 'order_id', $order_id );
            DB::commit();

            if ( $data['payment_getway'] == "COD" ) {

                /*
                // send Order SMS
                $message ="Dear Customer. Your Order ".$order_id."has been placed by AmazonBd.We will intimate you once your order is shipped";
                $mobile = Auth::user()->mobile;
                Sms::sendSms($message,$mobile);
                // send Order SMS END
                 */

                $orderDetails = Order::with( 'orders_products' )->where( 'id', $order_id )->first()->toArray();

                //Send Order Email
                $email       = Auth::user()->email;
                $messageData = [
                    'email'        => $email,
                    'name'         => Auth::user()->name,
                    'order_id'     => $order_id,
                    'orderDetails' => $orderDetails,

                ];
                Mail::send( 'emails.order', $messageData, function ( $message ) use ( $email ) {
                    $message->to( $email )->subject( 'Order Placed By AmazonBd' );
                } );
                //Send Order Email End

                return redirect( '/thanks' );
            } else if ( $data['payment_getway'] == "Paypal" ) {
                //Paypal - Redirect user to paypal page after placing order

                return redirect( 'paypal' );

            } elseif($data['payment_getway'] == "SSLCommerz"){
                return redirect( 'sslcommerz' );
            } else{
                echo "Others payment Method Coming Soon";
                die;
            }

        }

        return view( 'front.products.checkout' )->with( compact( 'userCartItems', 'deliveryAddresses', 'total_price' ) );
    }

    public function thanks() {
        //Empty The user cart
        if ( Session::has( 'order_id' ) ) {
            Cart::where( 'user_id', Auth::user()->id )->delete();
            return view( 'front.products.thanks' );
        } else {
            return redirect( '/cart' );
        }

    }

    public function addEditDeliveryAddress( $id = null, Request $request ) {

        if ( $id == "" ) {
            //Add Delivery Address
            $title   = "Add Delivery Address";
            $address = new DeliveryAddress;
            $message = "Delivery Address Added Successfully";
        } else {
            //Edit/Update Delivery Address
            $title   = "Edit Delivery Address";
            $address = DeliveryAddress::find( $id );
            $message = "Delivery Address Updated Successfully";
        }

        $countries = Country::get()->toArray();
        if ( $request->isMethod( 'post' ) ) {
            $data = $request->all();
            // echo"<pre>";print_r($data);die();
            $rules = [
                'name'         => 'required|regex:/^[\pL\s\-]+$/u',
                'address'      => 'required',
                'city'         => 'required|regex:/^[\pL\s\-]+$/u',
                'state'        => 'required|regex:/^[\pL\s\-]+$/u',
                'pincode'      => 'required|numeric',
                'country_name' => 'required',
                'mobile'       => 'required|numeric|digits:11',

            ];
            $customMessages = [
                'name.required'         => "Name is Required!",
                'name.regex'            => "Valid Name is Required!",
                'address.required'      => "Address is Required!",
                'city.required'         => "City is Required!",
                'city.regex'            => "valid City is Required!",
                'state.required'        => "State is Required!",
                'state.regex'           => "Valid State is Required!",
                'pincode.required'      => "pincode Number is Required!",
                'pincode.numeric'       => "Valid pincode  is Required!",
                'country_name.required' => "Select Your Country!",
                'mobile.required'       => "Mobile Number is Required!",
                'mobile.numeric'        => "Valid Mobile Number is Required!",
                'mobile.digits'         => "Mobile Number Must be 11",
            ];
            $this->validate( $request, $rules, $customMessages );
            $address->user_id      = Auth::user()->id;
            $address->name         = $data['name'];
            $address->address      = $data['address'];
            $address->city         = $data['city'];
            $address->state        = $data['state'];
            $address->country_name = $data['country_name'];
            $address->pincode      = $data['pincode'];
            $address->mobile       = $data['mobile'];
            $address->save();
            Session::put( 'success_message', $message );
            return redirect( 'checkout' );

        }

        return view( 'front.products.add_edit_delivery_address', compact( 'title', 'address', 'countries' ) );

    }

    public function deleteDeliveryAddress( $id ) {
        DeliveryAddress::where( 'id', $id )->delete();
        $message = "Delivery Address Deleted Successfully";
        Session::put( 'success_message', $message );
        return redirect()->back();
    }

    public function checkPincode( Request $request ) {
        if ( $request->isMethod( 'post' ) ) {
            $data = $request->all();
            if ( is_numeric( $data['pincode'] ) && $data['pincode'] > 0 && $data['pincode'] == round( $data['pincode'], 0 ) ) {

                $codPincodeCount     = CodPincodes::where( 'pincode', $data['pincode'] )->count();
                $prepaidPincodeCount = PrepaidPincodes::where( 'pincode', $data['pincode'] )->count();

                if ( $codPincodeCount == 0 && $prepaidPincodeCount == 0 ) {
                    echo "This Post Code is not Available for Delivery";
                    die;
                } else {
                    echo "This Post Code is Available for Delivery";
                    die;
                }

            } else {
                echo "Please Enter valid Pincode";
                die;
            }
        }

    }

}
