<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Order;
use App\RefundOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class OrdersController extends Controller
{

    public function orders()
    {
        $orders = Order::with('orders_products')->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->get()->toArray();
        return view('front.orders.orders', compact('orders'));
    }

    public function orderDetails($id)
    {
        $orderDetails = Order::with('orders_products')->where('id', $id)->first()->toArray();
        //dd($orderDetails);
        return view('front.orders.order_details', compact('orderDetails'));

    }
    public function refundOrder($id){
        $orderDetails = Order::with('orders_products')->where('id', $id)->first()->toArray();
        //dd($orderDetails);
        return view('front.orders.refund_details', compact('orderDetails'));
    }
    public function saveRefundInfo(Request $request){
        $data = new RefundOrder();
        $data->order_id = $request->order_id;
        $data->refund_method = $request->refund_method;
        $data->number  = $request->number;
        $data->save();
        $order = DB::table('orders')
              ->where('id', $request->order_id)
              ->update(['payment_status' => 'Refund']);
        return redirect('orders')->with('message', 'Refund Request Successfully Send.Please wait For full process !');
    }
    public function cancelOrder($id){
        $order = DB::table('orders')
              ->where('id', $id)
              ->update(['order_status' => 'Cancelled']);
        return redirect()->back()->with('message', 'Order Cancelled Successfully !');
    }

}
