<?php

namespace App\Http\Controllers\Front;

use App\Cart;
use App\Http\Controllers\Controller;
use App\Order;
use App\ProductsAttribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use DB;

class PaypalController extends Controller
{

    public function paypal()
    {
        //Empty The user cart
        if (Session::has('order_id')) {
            $orderDetails = Order::where('id', Session::get('order_id'))->first()->toArray();
            $nameArr = explode(' ', $orderDetails['name']);
            return view('front.paypal.paypal', compact('orderDetails', 'nameArr'));
        } else {
            return redirect('/cart');
        }
    }

    public function success()
    {
        if (Session::has('order_id')) {
            $order = DB::table('orders')
              ->where('id', Session::get('order_id'))
              ->update(['payment_status' => 'Paid']);
            Cart::where('user_id', Auth::user()->id)->delete();
            return view('front.paypal.success');
        } else {
            return redirect('/cart');
        }

    }

    public function fail()
    {
        return view('front.paypal.fail');
    }

    public function ipn(Request $request)
    {
        $data = $request->all();
        if ($data['payment_status'] == "Completed") {
            $order_id = Session::get('order_id');
            Order::where('id', $order_id)->update(['order_status' => 'Paid']);

            /*
            // send Order SMS
            $message ="Dear Customer. Your Order ".$order_id."has been placed by AmazonBd.We will intimate you once your order is shipped";
            $mobile = Auth::user()->mobile;
            Sms::sendSms($message,$mobile);
            // send Order SMS END
             */

            $orderDetails = Order::with('orders_products')->where('id', $order_id)->first()->toArray();

            foreach ($orderDetails['orders_products'] as $order) {
                //Reduce stock Management
                $getProductStock = ProductsAttribute::where(['product_id' => $order['product_id'], 'size' => $order['size']])->first()->toArray();
                $newStock = $getProductStock['stock'] - $order['quantity'];
                ProductsAttribute::where(['product_id' => $order['product_id'], 'size' => $order['size']])->update(['stock' => $newStock]);
                //Reduce stock Management End
            }

            //Send Order Email
            $email = Auth::user()->email;
            $messageData = [
                'email' => $email,
                'name' => Auth::user()->name,
                'order_id' => $order_id,
                'orderDetails' => $orderDetails,

            ];
            Mail::send('emails.order', $messageData, function ($message) use ($email) {
                $message->to($email)->subject('Order Placed By AmazonBd');
            });
            //Send Order Email End

        }

    }

}
