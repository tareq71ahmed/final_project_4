<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Rating;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class RatingsController extends Controller
{

    public function  ratings(){
        Session::put('page', 'ratings');
        $ratings = Rating::with(['user','product'])->get()->toArray();
       return view('admin.ratings.ratings',compact('ratings'));
    }



    public function update_rating_status(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Rating::where('id', $data['rating_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'rating_id' => $data['rating_id']]);
        }
    }


}
