<?php

namespace App\Http\Controllers\Admin;

use App\AdminsRole;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrdersLog;
use App\OrderStatus;
use App\RefundOrder;
use App\User;
use Carbon\Carbon;
use Dompdf\Dompdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use DB;

class OrdersController extends Controller
{

    public function orders()
    {
        Session::put('page', 'orders');
        $orders = Order::with('orders_products')->orderBy('id', 'DESC')->get()->toArray();

        $order_status = OrderStatus::where('status',1)->get();

        //set Admin/Sub-Admin Permission for orders
        $orderModuleCount = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'orders'])->count();

        if (Auth::guard('admin')->user()->type == 'superadmin') {
            $orderModule['view_access'] = 1;
            $orderModule['edit_access'] = 1;
            $orderModule['full_access'] = 1;
        } else if ($orderModuleCount == 0) {
            $message = 'The feature is Restricted for You';
            Session::flash('error_message', $message);
            return redirect('admin/dashboard');
        } else {
            $orderModule = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'orders'])->first();
        }
        //set Admin/Sub-Admin Permission for orders End
        return view('admin.orders.orders', compact('orders', 'orderModule','order_status'));

    }

    public function orderDetails($id)
    {
        $orderDetails = Order::with('orders_products')->where('id', $id)->first()->toArray();
      //  dd($orderDetails['payment_getway']);
        $userDetails = User::where('id', $orderDetails['user_id'])->first()->toArray();
        $orderStatuses = OrderStatus::where('status', 1)->get()->toArray();
       // dd($orderStatuses);
        $orderLog = OrdersLog::where('order_id', $id)->orderBy('id', 'Desc')->get()->toArray();
        return view('admin.orders.order_details', compact('orderDetails', 'userDetails', 'orderStatuses', 'orderLog'));

    }

    public function updateorderStatus(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            Order::where('id', $data['order_id'])->update(['order_status' => $data['order_status']]);
            Session::put('success_message', 'Order status has been updated successfully');
            if($data['order_status']=='Delivered'){
                $order = DB::table('orders')
                ->where('id', $request->order_id)
                ->update(['payment_status' => 'Paid']);
            }
            

            //Update  Courier Name And  Tracking Number
            if (!empty($data['courier_name']) && !empty($data['tracking_number'])) {
                Order::where('id', $data['order_id'])->update(['courier_name' => $data['courier_name'], 'tracking_number' => $data['tracking_number']]);
            }
            //Update  Courier Name And  Tracking Number end

            $deliveryDetails = Order::select('mobile', 'email', 'name')->where('id', $data['order_id'])->first()->toArray();

            //send sms to User
            /*$message = "Dear Customer !You Order #'" . $data['order_id'] . "'  status has been Updated to '" . $data['order_status'] . "' places with AmazonBd ";
            $mobile = $deliveryDetails['mobile'];
            Sms::sendSms($message, $mobile);*/
            //send sms to User End

            //send Email To User
            $orderDetails = Order::with('orders_products')->where('id', $data['order_id'])->first()->toArray();
            $email = $deliveryDetails['email'];
            $messageData = [
                'email' => $email,
                'name' => $deliveryDetails['name'],
                'order_id' => $data['order_id'],
                'order_status' => $data['order_status'],
                'courier_name' => $data['courier_name'],
                'tracking_number' => $data['tracking_number'],
                'orderDetails' => $orderDetails,
            ];
            Mail::send('emails.order_status', $messageData, function ($message) use ($email) {
                $message->to($email)->subject('Order Status Updated By AmazonBd');
            });

            //send Email To User  End

            //Update Order Logs
            $log = new OrdersLog;
            $log->order_id = $data['order_id'];
            $log->order_status = $data['order_status'];
            $log->save();
            //Update Order Logs End

            return redirect()->back();

        }

    }

    public function viewOrderinvoice($id)
    {
        $orderDetails = Order::with('orders_products')->where('id', $id)->first()->toArray();
        $userDetails = User::where('id', $orderDetails['user_id'])->first()->toArray();
        return view('admin.orders.order_invoice', compact('orderDetails', 'userDetails'));
    }

    public function printPDFinvoice($id)
    {
        $orderDetails = Order::with('orders_products')->where('id', $id)->first()->toArray();
        $userDetails = User::where('id', $orderDetails['user_id'])->first()->toArray();
        $Output = '<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <title>Example 2</title>
      <style>
         @font-face {
         font-family: SourceSansPro;
         src: url(SourceSansPro-Regular.ttf);
         }
         .clearfix:after {
         content: "";
         display: table;
         clear: both;
         }
         a {
         color: #0087C3;
         text-decoration: none;
         }
         body {
         position: relative;
         width: 21cm;
         height: 29.7cm;
         margin: 0 auto;
         color: #555555;
         background: #FFFFFF;
         font-family: Arial, sans-serif;
         font-size: 14px;
         font-family: SourceSansPro;
         }
         header {
         padding: 10px 0;
         margin-bottom: 20px;
         border-bottom: 1px solid #AAAAAA;
         }
         #logo {
         float: left;
         margin-top: 8px;
         }
         #logo img {
         height: 70px;
         }
         #company {
         float: right;
         text-align: right;
         }
         #details {
         margin-bottom: 50px;
         }
         #client {
         padding-left: 6px;
         border-left: 6px solid #0087C3;
         float: left;
         }
         #client .to {
         color: #777777;
         }
         h2.name {
         font-size: 1.4em;
         font-weight: normal;
         margin: 0;
         }
         #invoice {
         float: right;
         text-align: right;
         }
         #invoice h1 {
         color: #0087C3;
         font-size: 2.4em;
         line-height: 1em;
         font-weight: normal;
         margin: 0  0 10px 0;
         }
         #invoice .date {
         font-size: 1.1em;
         color: #777777;
         }
         table {
         width: 100%;
         border-collapse: collapse;
         border-spacing: 0;
         margin-bottom: 20px;
         }
         table th,
         table td {
         padding: 20px;
         background: #EEEEEE;
         text-align: center;
         border-bottom: 1px solid #FFFFFF;
         }
         table th {
         white-space: nowrap;
         font-weight: normal;
         }
         table td {
         text-align: right;
         }
         table td h3{
         color: #57B223;
         font-size: 1.2em;
         font-weight: normal;
         margin: 0 0 0.2em 0;
         }
         table .no {
         color: #FFFFFF;
         font-size: 1.6em;
         background: #57B223;
         }
         table .desc {
         text-align: left;
         }
         table .unit {
         background: #DDDDDD;
         }
         table .qty {
         }
         table .total {
         background: #57B223;
         color: #FFFFFF;
         }
         table td.unit,
         table td.qty,
         table td.total {
         font-size: 1.2em;
         }
         table tbody tr:last-child td {
         border: none;
         }
         table tfoot td {
         padding: 10px 20px;
         background: #FFFFFF;
         border-bottom: none;
         font-size: 1.2em;
         white-space: nowrap;
         border-top: 1px solid #AAAAAA;
         }
         table tfoot tr:first-child td {
         border-top: none;
         }
         table tfoot tr:last-child td {
         color: #57B223;
         font-size: 1.4em;
         border-top: 1px solid #57B223;
         }
         table tfoot tr td:first-child {
         border: none;
         }
         #thanks{
         font-size: 2em;
         margin-bottom: 50px;
         }
         #notices{
         padding-left: 6px;
         border-left: 6px solid #0087C3;
         }
         #notices .notice {
         font-size: 1.2em;
         }
         footer {
         color: #777777;
         width: 100%;
         height: 30px;
         position: absolute;
         bottom: 0;
         border-top: 1px solid #AAAAAA;
         padding: 8px 0;
         text-align: center;
         }
      </style>
   </head>
   <body>
      <header class="clearfix">
         <div id="logo">
            <h1>Order Invoice </h1>
         </div>
         </div>
      </header>
      <main>
         <div id="details" class="clearfix">
            <div id="client">
               <div class="to">INVOICE TO:</div>
               <h2 class="name">' . $orderDetails['name'] . '</h2>
               <div class="address">' . $orderDetails['address'] . ',' . $orderDetails['city'] . ',' . $orderDetails['state'] . '</div>
               <div class="address">' . $orderDetails['pincode'] . '</div>
               <div class="email"><a href="' . $orderDetails['email'] . '">' . $orderDetails['email'] . '</a></div>
            </div>
            <div style="float:right">
               <h1>ORDER ID #' . $orderDetails['id'] . '</h1>
               <div class="date">Order Date : ' . date('d-m-y', strtotime($orderDetails['created_at'])) . '</div>
                <div class="date">Order Amount : BDT.' . $orderDetails['grand_total'] . '</div>
                <div class="date">Order Status : ' . $orderDetails['order_status'] . '</div>
                <div class="date">Payment Method : ' . $orderDetails['payment_method'] . '</div>
            </div>
         </div>
         <table border="0" cellspacing="0" cellpadding="0">
            <thead>
               <tr>
                  <th class="unit">Product Code</th>
                  <th class="desc">SIZE</th>
                  <th class="unit">COLOR</th>
                  <th class="qty">PRICE</th>
                  <th class="unit">QTY</th>
                  <th class="total">TOTAL</th>
               </tr>
            </thead>
            <tbody>';
        $subTotal = 0;
        foreach ($orderDetails['orders_products'] as $product) {
            $Output .= '<tr>
                  <td class="unit">' . $product['product_code'] . '</td>
                  <td class="desc">' . $product['product_size'] . '</td >
                  <td class="unit" >' . $product['product_color'] . '</td >
                  <td class="qty" >' . $product['product_price'] . '</td >
                  <td class="unit" >' . $product['product_qty'] . '</td >
                  <td class="total" >' . $product['product_price'] * $product['product_qty'] . '</td >
               </tr >';
            $subTotal = $subTotal + ($product['product_price'] * $product['product_qty']);
        }
        $Output .= '</tbody >
            <tfoot >
               <tr >
                  <td colspan = "2" ></td >
                  <td colspan = "2" > SUBTOTAL</td >
                  <td >BDT.' . $subTotal . ' </td >
               </tr >
               <tr >
                  <td colspan = "2" ></td >
                  <td colspan = "2" >Shipping Cgarges</td >
                  <td > BDT.0 </td >
               </tr >

               <tr>
                  <td colspan = "2" ></td >
                  <td colspan = "2" >Coupon Discount</td >';
        if ($orderDetails['coupon_amount'] > 0) {
            $Output .= '<td>BDT.' . $orderDetails['coupon_amount'] . ' </td>';
        } else {
            $Output .= '<td>BDT.0</td>';
        }
        $Output .= '</tr >
               <tr >
                  <td colspan = "2" ></td >
                  <td colspan = "2" > GRAND TOTAL </td >
                  <td >BDT.' . $orderDetails['grand_total'] . '</td >
               </tr >
            </tfoot >
         </table >
      </main >
      <footer >
         Invoice was created on a computer and is valid without the signature and seal .
      </footer >
   </body >
</html >';
        // reference the Dompdf namespace
        $dompdf = new Dompdf();
        $dompdf->loadHtml($Output);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
        // reference the Dompdf namespace
        return view('admin . orders . order_invoice', compact('orderDetails', 'userDetails'));
    }


    public function viewOrdersCharts(){
        $current_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->month)->count();
        $before_1_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(1))->count();
        $before_2_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(2))->count();
        $before_3_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(3))->count();
        $ordersCount = array($current_month_orders,$before_1_month_orders,$before_2_month_orders,$before_3_month_orders);

        $current_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->month)->count();
        $before_1_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(1))->count();
        $before_2_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(2))->count();
        $before_3_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(3))->count();
        $ordersCount1 = array($current_month_orders1,$before_1_month_orders1,$before_2_month_orders1,$before_3_month_orders1);
       //echo "<pre>";print_r($ordersCount);die();
        return view('admin.orders.view_orders_charts',compact('ordersCount','ordersCount1'));
    }
    public function filterOrder(Request $request){
      $payment_status = $request->payment_status;
      $order_status = $request->order_status;
      $payment_method = $request->payment_method;
      $from_date = $request->from_date;
      $to_date = $request->to_date;
      $order_s = OrderStatus::where('status',1)->get();
      $orders  = Order::
      where(function($delivery) use ($payment_status, $order_status,$payment_method, $from_date, $to_date) {
          if (!empty($payment_status) || $payment_status != '') {
              $delivery->where('payment_status', $payment_status);
          }
          if (!empty($order_status) || $order_status != '') {
              $delivery->where('order_status', $order_status);
          }
          if (!empty($payment_method) || $payment_method != '') {
             $delivery->where('payment_method', $payment_method);
          }
          if (!empty($from_date) || $from_date != '') {
              $delivery->where('date', '>=', $from_date);
          }
          if (!empty($to_date) || $to_date != '') {
              $delivery->where('date','<=', $to_date);
          }
      })->get();
        //set Admin/Sub-Admin Permission for orders
        $orderModuleCount = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'orders'])->count();

        if (Auth::guard('admin')->user()->type == 'superadmin') {
            $orderModule['view_access'] = 1;
            $orderModule['edit_access'] = 1;
            $orderModule['full_access'] = 1;
        } else if ($orderModuleCount == 0) {
            $message = 'The feature is Restricted for You';
            Session::flash('error_message', $message);
            return redirect('admin/dashboard');
        } else {
            $orderModule = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'orders'])->first();
        }
        //set Admin/Sub-Admin Permission for orders End
      return view('admin.orders.filter_order',compact('orders','orderModule','order_s'));
    }
    public function orderGraph(){       
        // $current_month_orders = Order::where('payment_status','Delivered')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->month)->count();
        $total_orders = Order::count();
        $current_month_orders = Order::where('order_status','Delivered')->count();
        $cancelled_orders = Order::where('order_status','Cancelled')->count();
        $Shipped_orders = Order::where('order_status','Shipped')->count();
        $progress_orders = Order::where('order_status','In Process')->count();
        $new_orders = Order::where('order_status','New')->count();
        $hold_orders = Order::where('order_status','Hold')->count();

        $current_month_orders = $current_month_orders*100/$total_orders;
        $cancelled_orders = $cancelled_orders*100/$total_orders;
        $Shipped_orders = $Shipped_orders*100/$total_orders;
        $progress_orders = $progress_orders*100/$total_orders;
        $new_orders = $new_orders*100/$total_orders;
        $hold_orders = $hold_orders*100/$total_orders;
        $ordersCount = array($current_month_orders,$cancelled_orders,$Shipped_orders,$progress_orders,$new_orders,$hold_orders);
       //echo "<pre>";print_r($ordersCount);die();
        return view('admin.orders.order_graph',compact('ordersCount'));
    }
    public function paymentGraph(){
        $total_orders = Order::count();
        $due_orders = Order::where('payment_status','Due')->count();
        $paid_orders = Order::where('payment_status','Paid')->count();
        $refund_orders = Order::where('payment_status','Refund')->count();
        $success_refund_orders = Order::where('payment_status','Success Refund')->count();
        $due_orders = $due_orders*100/$total_orders;
        $paid_orders = $paid_orders*100/$total_orders;
        $refund_orders = $refund_orders*100/$total_orders;
        $success_refund_orders = $success_refund_orders*100/$total_orders;
        $ordersCount = array($due_orders,$paid_orders,$refund_orders,$success_refund_orders);
        return view('admin.orders.orders_payment_graph',compact('ordersCount')); 
    }
    public function paymentMethodGraph(){
        $total_orders = Order::count();
        $cod_orders = Order::where('payment_method','COD')->count();
        $paypal_orders = Order::where('payment_method','Paypal')->count();
        $ssl_orders = Order::where('payment_method','SSLCommerz')->count();
        $cod_orders = $cod_orders*100/$total_orders;
        $paypal_orders = $paypal_orders*100/$total_orders;
        $ssl_orders = $ssl_orders*100/$total_orders;
        $ordersCount = array($cod_orders,$paypal_orders,$ssl_orders);
        return view('admin.orders.payment_method_graph',compact('ordersCount')); 
    }
    public function refundRequest($id){
        $orderDetails = Order::with('orders_products')->where('id', $id)->first()->toArray();
      //  dd($orderDetails['payment_getway']);
        $userDetails = User::where('id', $orderDetails['user_id'])->first()->toArray();
        $orderStatuses = OrderStatus::where('status', 1)->get()->toArray();
       // dd($orderStatuses);
        $orderLog = OrdersLog::where('order_id', $id)->orderBy('id', 'Desc')->get()->toArray();
        $refund = RefundOrder::where('order_id',$id)->first();
        return view('admin.orders.refund_details',compact('orderDetails','userDetails','orderStatuses','orderLog','refund')); 
    }
    public function saveRefund(Request $request){
        DB::table('orders')
              ->where('id', $request->order_id)
              ->update(['payment_status' => 'Success Refund']);
              //Send refund Email
        $order = Order::find($request->order_id);
              $email       = $order->email;
              $messageData = [
                  'email'        => $email,
                  'name'         => $order->name,
                  'order_id'     => $request->order_id,
                  'grand_total'     => $order->grand_total,

              ];
              Mail::send( 'emails.refund', $messageData, function ( $message ) use ( $email ) {
                  $message->to( $email )->subject( 'Payment Refund From AmazonBd' );
              } );
              //Send refund Email End
        return redirect('admin/orders')->with('message', 'Refund Successfully. !');
    }

}
