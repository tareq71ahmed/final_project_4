<?php

namespace App\Http\Controllers\Admin;

use App\CmsPage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CmsController extends Controller
{

    public function cmsPages()
    {

        Session::put('page', 'cms-pages');
        $cms_pages = CmsPage::get();
        return view('admin.pages.cms-pages', compact('cms_pages'));
    }

    public function updatePageStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            CmsPage::where('id', $data['page_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'page_id' => $data['page_id']]);
        }
    }

    public function addEditCmsPage($id = null, Request $request)
    {

        if ($id == '') {
            $title = "Add Cms Page";
            $cmspage = new CmsPage;
            $message = "Cms Page Added Successfully!";

        } else {
            $title = "Edit Cms Page";
            $cmspage = CmsPage::find($id);
            $message = "Cms Page Updated Successfully!";

        }

        if ($request->isMethod('post')) {
            $data = $request->all();
            $rules = [
                'title' => 'required',
                'url' => 'required',
                'description' => 'required',
            ];
            $customMessages = [
                'title.required' => "Title Required!",
                'url.required' => "URL Must be Required!",
                'description.required' => "Description URL is Required!",
            ];

            $this->validate($request, $rules, $customMessages);
            $cmspage->title = $data['title'];
            $cmspage->url = $data['url'];
            $cmspage->description = $data['description'];
            $cmspage->meta_title = $data['meta_title'];
            $cmspage->meta_description = $data['meta_description'];
            $cmspage->meta_keywords = $data['meta_keywords'];
            $cmspage->status = 1;
            $cmspage->save();
            Session::flash('success_message', $message);
            return redirect('admin/cms-pages');

        }

        return view("admin.pages.add_edit_cmspage", compact('title', 'cmspage'));

    }

    public function deleteCmsPage($id)
    {
        CmsPage::where('id', $id)->delete();
        Session::flash('success_message', 'CMS Page Has been deleted successfully');
        return redirect()->back();
    }

}
