<?php

namespace App\Http\Controllers\Admin;

use App\AdminsRole;
use App\Coupon;
use App\Http\Controllers\Controller;
use App\Section;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CouponsController extends Controller
{

    public function coupons()
    {

        Session::put('page', 'coupons');
        $coupons = Coupon::get()->toArray();

        //set Admin/Sub-Admin Permission for coupon
        $couponModuleCount = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'coupons'])->count();

        if (Auth::guard('admin')->user()->type == 'superadmin') {
            $couponModule['view_access'] = 1;
            $couponModule['edit_access'] = 1;
            $couponModule['full_access'] = 1;
        } else if ($couponModuleCount == 0) {
            $message = 'The feature is Restricted for You';
            Session::flash('error_message', $message);
            return redirect('admin/dashboard');
        } else {
            $couponModule = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'coupons'])->first();
        }
        //set Admin/Sub-Admin Permission for coupon End

        return view('admin.coupons.coupons', compact('coupons', 'couponModule'));
    }

    public function updateCouponStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Coupon::where('id', $data['coupon_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'coupon_id' => $data['coupon_id']]);
        }
    }

    public function addEditCoupon(Request $request, $id = null)
    {
        if ($id == "") {
            //Add Coupon
            $coupon = new Coupon;
            $selCats = array();
            $selUsers = array();
            $title = "Add Coupon";
            $message = "Coupon Added successfully";

        } else {
            //Update Coupon
            $coupon = Coupon::find($id);
            $selCats = explode(',', $coupon['categories']);
            $selUsers = explode(',', $coupon['users']);
            $title = "Edit Coupon";
            $message = "Coupon Updated successfully";
        }

        if ($request->isMethod('post')) {
            $data = $request->all();

            //Coupon Validation

            $rules = [
                'categories' => 'required',
                'coupon_option' => 'required',
                'coupon_type' => 'required',
                'amount_type' => 'required',
                'amount' => 'required|numeric',
                'expiry_date' => 'required',
            ];
            $customMessages = [
                'categories.required' => "Select Category!",
                'coupon_option.required' => "Select Coupon Option!",
                'coupon_type.required' => "Select CouponType!",
                'amount_type.required' => "AmountType  is Required!",
                'amount.required' => "Enter Amount!",
                'amount.numeric' => "Amount Must be Valid!",
                'expiry_date.required' => "Enter Expiry Date!",
            ];
            $this->validate($request, $rules, $customMessages);

            //Coupon Validation End

            if (isset($data['users'])) {
                $users = implode(',', $data['users']);
            } else {
                $users = "";
            }

            if (isset($data['categories'])) {
                $categories = implode(',', $data['categories']);
            }

            if ($data['coupon_option'] == "Automatic") {
                $coupon_code = str_random(8);
            } else {
                $coupon_code = $data['coupon_code'];
            }

            $coupon->coupon_option = $data['coupon_option'];
            $coupon->coupon_code = $coupon_code;
            $coupon->categories = $categories;
            $coupon->users = $users;
            $coupon->coupon_type = $data['coupon_type'];
            $coupon->amount_type = $data['amount_type'];
            $coupon->amount = $data['amount'];
            $coupon->expiry_date = $data['expiry_date'];
            $coupon->status = 1;
            $coupon->save();
            Session::flash('success_message', $message);
            return redirect('admin/coupons');

        }

        //sections with Categories and Sub-Categories
        $categories = Section::with('categories')->get();
        $categories = json_decode(json_encode($categories), true);
        //Users
        $users = User::select('email')->where('status', 1)->get()->toArray();
        return view('admin.coupons.add_edit_coupon', compact('coupon', 'title', 'categories', 'users', 'selCats', 'selUsers'));

    }

    public function deleteCoupon($id)
    {
        Coupon::where('id', $id)->delete();
        Session::flash('success_message', 'Coupon Has been deleted successfully');
        return redirect()->back();

    }

}
