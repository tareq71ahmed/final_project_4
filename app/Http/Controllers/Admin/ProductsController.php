<?php

namespace App\Http\Controllers\Admin;

use App\AdminsRole;
use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductsAttribute;
use App\ProductsImage;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class ProductsController extends Controller
{

    public function products()
    {
        $products = Product::with(['category' => function ($query) {
            $query->select('id', 'category_name');
        }, 'section' => function ($query) {
            $query->select('id', 'name');
        }])->get();
        //dd($products);
        Session::put('page', 'products');

        //set Admin/Sub-Admin Permission for Product
        $productModuleCount = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'products'])->count();
        if (Auth::guard('admin')->user()->type == 'superadmin') {
            $productModule['view_access'] = 1;
            $productModule['edit_access'] = 1;
            $productModule['full_access'] = 1;
        } else if ($productModuleCount == 0) {
            $message = 'The feature is Restricted for You';
            Session::flash('error_message', $message);
            return redirect('admin/dashboard');
        } else {
            $productModule = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'products'])->first();
        }
        //set Admin/Sub-Admin Permission for Product End

        return view('admin.products.products')->with(compact('products', 'productModule'));

    }

    public function updateProductStatus(Request $request)
    {

        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Product::where('id', $data['product_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'product_id' => $data['product_id']]);
        }

    }

    public function deleteProduct($id)
    {
        Product::where('id', $id)->delete();
        Session::flash('success_message', 'Product Has been deleted successfully');
        return redirect()->back();
    }

    public function addEditProduct(Request $request, $id = null)
    {
        if ($id == "") {
            $title = "Add Product";
            $product = new Product;
            $productdata = [];
            $message = "Product Added successfully";
        } else {
            $title = "Edit Product";
            $productdata = Product::find($id);
            $product = Product::find($id);
            $message = "Product Updated successfully";
        }

        if ($request->isMethod('post')) {
            $data = $request->all();
            //Product Validation
            $rules = [
                'category_id' => 'required',
                'brand_id' => 'required',
                'product_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'product_code' => 'required|regex:/^[\w-]*$/',
                'group_code' => 'required|numeric',
                'product_price' => 'required|numeric',
                'product_color' => 'required|regex:/^[\pL\s\-]+$/u',
                'product_discount' => 'required|numeric',
                'product_weight' => 'required|numeric',
                'main_image' => 'image|mimes:jpeg,jpg,png',
                'product_video' => 'mimetypes:video/x-matroska,video/x-flv,video/mp4|max:5120',
            ];
            $customMessages = [
                'category_id.required' => "Category is Required!",
                'product_name.required' => "Product Name is Required!",
                'brand_id.required' => "Brand is Required!",
                'product_name.regex' => "Product Name Must be Required!",
                'product_code.required' => "Product Code is Required!",
                'product_code.regex' => "Valid Product Code is Required!",
                'group_code.required' => "Product Group Code is Required!",
                'group_code.numeric' => "Product Group Code Must be numeric!",
                'product_price.required' => "Product Price is Required!",
                'product_price.numeric' => "Product Price Must be numeric!",
                'product_color.required' => "Product Color is Required!",
                'product_color.regex' => "Valid Product Color is Required!",
                'product_discount.required' => "Product Discount Must be Required!",
                'product_discount.numeric' => "Product Discount Must be numeric!",
                'product_weight.required' => "Product Weight Must be Required!",
                'product_weight.numeric' => "Product Weight Must be numeric!",
                'main_image.image' => "File Must be an Image!",
                'main_image.mimes' => "File Must be jpeg,jpg or png",
                'product_video.mimetypes' => "File Must be mkv,flv or mp4",
                'product_video.max' => "File Max 5120(5MB)", //10 Mb:10240
            ];
            $this->validate($request, $rules, $customMessages);

            //Add product
            if (empty($data['is_featured'])) {
                $is_featured = "No";
            } else {
                $is_featured = "Yes";
            }

            if (empty($data['fabric'])) {
                $data['fabric'] = "";
            }

            if (empty($data['pattern'])) {
                $data['pattern'] = "";
            }

            if (empty($data['sleeve'])) {
                $data['sleeve'] = "";
            }

            if (empty($data['fit'])) {
                $data['fit'] = "";
            }

            if (empty($data['occasion'])) {
                $data['occasion'] = "";
            }

            if (empty($data['meta_title'])) {
                $data['meta_title'] = "";
            }

            if (empty($data['meta_keywords'])) {
                $data['meta_keywords'] = "";
            }

            if (empty($data['description'])) {
                $data['description'] = "";
            }

            if (empty($data['wash_care'])) {
                $data['wash_care'] = "";
            }

            if (empty($data['meta_description'])) {
                $data['meta_description'] = "";
            }

            if (empty($data['product_discount'])) {
                $data['product_discount'] = 0;
            }
            if (empty($data['product_weight'])) {
                $data['product_weight'] = 0;
            }

            //Upload Product Images
            if ($request->hasFile('main_image')) {
                $image_tmp = $request->file('main_image');
                if ($image_tmp->isValid()) {
                    // Generate New Image
                   // $image_name = $image_tmp->getClientOriginalName();
                    $extention = $image_tmp->getClientOriginalExtension();
                    $imageName =  rand(111, 99999) . time() .'.' . $extention;
                    $large_image_path = 'images/product_images/large/' . $imageName;
                    $medium_image_path = 'images/product_images/medium/' . $imageName;
                    $small_image_path = 'images/product_images/small/' . $imageName;
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(500, 500)->save($medium_image_path);
                    Image::make($image_tmp)->resize(250, 250)->save($small_image_path);
                    //save image
                    $product->main_image = $imageName;
                }
            }
            //Upload Product Images End

            //Upload Product Video
            if ($request->hasFile('product_video')) {
                $video_tmp = $request->file('product_video');
                if ($video_tmp->isValid()) {
                    $video_name = $video_tmp->getClientOriginalName();
                    $extention = $video_tmp->getClientOriginalExtension();
                    $videoName = $video_name . '-' . rand() . '.' . $extention;
                    $video_path = 'videos/product_videos/';
                    $video_tmp->move($video_path, $videoName);
                    //save video
                    $product->product_video = $videoName;
                }
            }
            //Upload Product Video End

            $categoryDetails = Category::find($data['category_id']);
            $product->section_id = $categoryDetails['section_id'];
            $product->brand_id = $data['brand_id'];
            $product->category_id = $data['category_id'];
            $product->product_name = $data['product_name'];
            $product->product_code = $data['product_code'];
            $product->product_color = $data['product_color'];
            $product->group_code = $data['group_code'];
            $product->product_price = $data['product_price'];
            $product->product_weight = $data['product_weight'];
            $product->product_discount = $data['product_discount'];
            $product->description = $data['description'];
            $product->wash_care = $data['wash_care'];
            $product->fabric = $data['fabric'];
            $product->pattern = $data['pattern'];
            $product->sleeve = $data['sleeve'];
            $product->fit = $data['fit'];
            $product->occasion = $data['occasion'];
            $product->meta_title = $data['meta_title'];
            $product->meta_keywords = $data['meta_keywords'];
            $product->meta_description = $data['meta_description'];
            $product->is_featured = $is_featured;
            $product->status = 1;
            $product->save();
            Session::flash('success_message', $message);
            return redirect('admin/products');
        }

        $productFilters = Product::productFilters();
        $fabricArray = $productFilters['fabricArray'];
        $sleeveArray = $productFilters['sleeveArray'];
        $patternArray = $productFilters['patternArray'];
        $fitArray = $productFilters['fitArray'];
        $occasionArray = $productFilters['occasionArray'];

        //sections with Categories and Sub-Categories
        $categories = Section::with('categories')->get();
        $categories = json_decode(json_encode($categories), true);

        $brands = Brand::where('status', 1)->get();
        $brands = json_decode(json_encode($brands), true);

        return view('admin.products.add_edit_product', compact('title', 'fabricArray', 'sleeveArray', 'patternArray', 'fitArray', 'occasionArray', 'categories', 'productdata', 'brands'));
    }

    public function deleteProductImage($id)
    {
        $productImage = Product::select('main_image')->where('id', $id)->first();
        $small_image_path = 'images/product_images/small/';
        $medium_image_path = 'images/product_images/medium/';
        $large_image_path = 'images/product_images/large/';

        if (file_exists($small_image_path . $productImage->main_image)) {
            unlink($small_image_path . $productImage->main_image);
        }

        if (file_exists($medium_image_path . $productImage->main_image)) {
            unlink($medium_image_path . $productImage->main_image);
        }
        if (file_exists($large_image_path . $productImage->main_image)) {
            unlink($large_image_path . $productImage->main_image);
        }

        Product::where('id', $id)->update(['main_image' => '']);
        Session::flash('success_message', 'Product Image deleted successfully');
        return redirect()->back();
    }

    public function deleteProductVideo($id)
    {
        $productVideo = Product::select('product_video')->where('id', $id)->first();
        $product_video_path = 'videos/product_videos/';

        if (file_exists($product_video_path . $productVideo->product_video)) {
            unlink($product_video_path . $productVideo->product_video);
        }

        Product::where('id', $id)->update(['product_video' => '']);
        Session::flash('success_message', 'Product Video deleted successfully');
        return redirect()->back();
    }

    //..................Products Attribute..................

    public function addAttributes(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['sku'] as $key => $value) {
                if (!empty($value)) {

                    //If SKU Already Exists
                    $attrCountSKU = ProductsAttribute::where(['sku' => $value])->count();
                    if ($attrCountSKU > 0) {
                        $message = "SKU already exists. Please Add Another SKU";
                        Session::flash('error_message', $message);
                        return redirect()->back();
                    }
                    //If SKU Already Exists End

                    //If Size Already Exists
                    $attrCountSize = ProductsAttribute::where(['product_id' => $id, 'size' => $data['size'][$key]])->count();
                    if ($attrCountSize > 0) {
                        $message = "Size already exists. Please Add Another Size";
                        Session::flash('error_message', $message);
                        return redirect()->back();
                    }
                    //If Size Already Exists End
                    $attribute = new ProductsAttribute();
                    $attribute->product_id = $id;
                    $attribute->sku = $data['sku'][$key];
                    $attribute->size = $data['size'][$key];
                    $attribute->price = $data['price'][$key];
                    $attribute->stock = $data['stock'][$key];
                    $attribute->status = 1;
                    $attribute->save();
                }

            }
            $message = "Product Attribute Add Successfully";
            Session::flash('success_message', $message);
            return redirect()->back();
        }
        $productdata = Product::select('id', 'product_name', 'product_code', 'product_color', 'product_price', 'main_image')->with('attributes')->find($id);
        // $productdata =json_decode(json_encode($productdata));
        $title = "Product Attributes";
        return view('admin.products.add_attributes')->with(compact('productdata', 'title'));
    }

    public function editAttributes(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            foreach ($data['attrId'] as $key => $attr) {
                if (!empty($attr)) {
                    ProductsAttribute::where(['id' => $data['attrId'][$key]])
                        ->update(['price' => $data['price'][$key], 'stock' => $data['stock'][$key]]);
                }

            }

            Session::flash('success_message', 'Product Attribute Updated successfully');
            return redirect()->back();
        }

    }

    public function updateAttributeStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            ProductsAttribute::where('id', $data['attribute_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'attribute_id' => $data['attribute_id']]);
        }

    }

    public function deleteAttribute($id)
    {
        ProductsAttribute::where('id', $id)->delete();
        $message = "Product Has been deleted successfully";
        Session::flash('success_message', $message);
        return redirect()->back();
    }

    //Add Multiple Images
    public function addImages(Request $request, $id)
    {
        if ($request->isMethod('post')) {

            if ($request->hasFile('images')) {
                $images = $request->file('images');
                foreach ($images as $key => $image) {
                    $productImage = new ProductsImage;
                    $image_tmp = Image::make($image);
                    // $originalName=$image->getClientOriginalName();
                    $extention = $image->getClientOriginalExtension();
                    $imageName = rand(111, 99999) . time() . "." . $extention;
                    //image path
                    $large_image_path = 'images/product_images/large/' . $imageName;
                    $medium_image_path = 'images/product_images/medium/' . $imageName;
                    $small_image_path = 'images/product_images/small/' . $imageName;
                    Image::make($image_tmp)->save($large_image_path);
                    Image::make($image_tmp)->resize(520, 600)->save($medium_image_path);
                    Image::make($image_tmp)->resize(260, 300)->save($small_image_path);
                    //save image
                    $productImage->image = $imageName;
                    $productImage->product_id = $id;
                    $productImage->status = 1;
                    $productImage->save();
                }
                $message = "Product Image successfully saved";
                Session::flash('success_message', $message);
                return redirect()->back();
            }
        }
        $productdata = Product::with('images')->select('id', 'product_name', 'product_code', 'product_color', 'main_image')->find($id);
        $productdata = json_decode(json_encode($productdata), true);
        $title = "Product Images";
        return view('admin.products.add_images', compact('productdata', 'title'));
    }

    public function updateImageStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            ProductsImage::where('id', $data['image_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'image_id' => $data['image_id']]);
        }

    }

    public function deleteImage($id)
    {

        $productImage = ProductsImage::select('image')->where('id', $id)->first();
        $small_image_path = 'images/product_images/small/';
        $medium_image_path = 'images/product_images/medium/';
        $large_image_path = 'images/product_images/large/';

        if (file_exists($small_image_path . $productImage->image)) {
            unlink($small_image_path . $productImage->image);
        }

        if (file_exists($medium_image_path . $productImage->image)) {
            unlink($medium_image_path . $productImage->image);
        }
        if (file_exists($large_image_path . $productImage->image)) {
            unlink($large_image_path . $productImage->image);
        }

        ProductsImage::where('id', $id)->delete();
        Session::flash('success_message', 'Product Image deleted successfully');
        return redirect()->back();

    }

}
