<?php

namespace App\Http\Controllers\Admin;

use App\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class BannersController extends Controller
{

    public function banners()
    {

        Session::put('page', 'banners');
        $banners = Banner::get()->toArray();
        return view('admin.banners.banners', compact('banners'));

    }

    public function addeditBanner(Request $request, $id = null)
    {
        if ($id == "") {
            $banner = new Banner;
            $title = "Add Banner Image";
            $message = "Banner Added Successfully";
        } else {
            $banner = Banner::find($id);
            $title = "Edit Banner Image";
            $message = "Banner Updated Successfully";

        }
        if ($request->isMethod('post')) {
            $data = $request->all();

            $rules = [
                'image' => 'image|mimes:jpeg,jpg,png',
            ];
            $customMessages = [
                'image.image' => "File Must be an Image!",
                //'image.required' => "File Must be Required!",
                'image.mimes' => "File Must be jpeg,jpg or png",
            ];
            $this->validate($request, $rules, $customMessages);

            $banner->link = $data['link'] ?? '';
            $banner->title = $data['title'] ?? '';
            $banner->alt = $data['alt'] ?? '';
            //Upload Product Images
            if ($request->hasFile('image')) {
                $image_tmp = $request->file('image');
                if ($image_tmp->isValid()) {
                    // Generate New Image
                    $image_name = $image_tmp->getClientOriginalName();
                    $extention = $image_tmp->getClientOriginalExtension();
                    $imageName = $image_name . '-' . rand(111, 99999) . '.' . $extention;
                    $banner_image_path = 'images/banner_images/' . $imageName;
                    Image::make($image_tmp)->resize(1170, 480)->save($banner_image_path);
                    //save image
                    $banner->image = $imageName;
                }
            }
            //Upload Product Images End
            $banner->status = 1;
            $banner->save();
            Session::flash('success_message', $message);
            return redirect('admin/banners');

        }

        return view("admin.banners.add_edit_banner", compact('title', 'banner'));
    }

    public function updateBannerStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Banner::where('id', $data['banner_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'banner_id' => $data['banner_id']]);
        }
    }

    public function deleteBannerImage($id)
    {
        $bannerImage = Banner::select('image')->where('id', $id)->first();
        $bannerImage_path = 'images/banner_images/';
        if (file_exists($bannerImage_path . $bannerImage->image)) {
            unlink($bannerImage_path . $bannerImage->image);
        }
        Banner::where('id', $id)->update(['image' => '']);
        Session::flash('success_message', 'Banner Image deleted successfully');
        return redirect()->back();
    }

    public function deleteBanner($id)
    {
        $bannerImage = Banner::where('id', $id)->first();
        $banner_image_path = 'images/banner_images/';
        if (file_exists($banner_image_path . $bannerImage->image)) {
            unlink($banner_image_path . $bannerImage->image);
        }
        Banner::where('id', $id)->delete();
        Session::flash('success_message', 'Banner  deleted successfully');
        return redirect()->back();

    }

}
