<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class UsersController extends Controller
{

    public function users()
    {
        $users = User::get()->toArray();
        Session::put('page', 'users');
        return view('admin.users.users', compact('users'));
    }

    public function updateUserStatus(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            User::where('id', $data['user_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'user_id' => $data['user_id']]);
        }
    }

    public function viewUsersChart()
    {
        $current_month_users = User::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->month)->count();
        $before_1_month_users = User::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(1))->count();
        $before_2_month_users = User::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(2))->count();
        $before_3_month_users = User::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(3))->count();
        $usersCount = array($current_month_users,$before_1_month_users,$before_2_month_users,$before_3_month_users);
        return view('admin.users.view_users_chart',compact('usersCount'));
    }


}
