<?php

namespace App\Http\Controllers\Admin;

use App\AdminsRole;
use App\Category;
use App\Http\Controllers\Controller;
use App\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class CategoryController extends Controller
{
    public function categories()
    {
        Session::put('page', 'categories');
        $categories = Category::with(['section', 'parentcategory'])->get();

        //set Admin/Sub-Admin Permission for Category
        $categoryModuleCount = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'categories'])->count();

        if (Auth::guard('admin')->user()->type == 'superadmin') {
            $categoryModule['view_access'] = 1;
            $categoryModule['edit_access'] = 1;
            $categoryModule['full_access'] = 1;
        } else if ($categoryModuleCount == 0) {
            $message = 'The feature is Restricted for You';
            Session::flash('error_message', $message);
            return redirect('admin/dashboard');
        } else {
            $categoryModule = AdminsRole::where(['admin_id' => Auth::guard('admin')->user()->id, 'module' => 'categories'])->first();
        }
        //set Admin/Sub-Admin Permission for Category End

        return view('admin.categories.categories', compact('categories', 'categoryModule'));
    }

    public function updateCategoryStatus(Request $request)
    {

        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Category::where('id', $data['category_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'category_id' => $data['category_id']]);
        }

    }

    public function addEditCategory(Request $request, $id = null)
    {
        if ($id == "") {
            //Add Category Functionally
            $title = "Add Category";
            $category = new Category;
            $categorydata = [];
            //$getcategories = [];
            $getcategories = Category::with('subcategories')->where(['parent_id' => 0])->get();
            $getcategories = json_decode(json_encode($getcategories), true);
            $message = "Category Added Successfully";

        } else {
            //Edit/Update Category Functionally
            $title = "Edit Category";
            $categorydata = Category::where('id', $id)->first();
            $getcategories = Category::with('subcategories')->where(['parent_id' => 0, 'section_id' => $categorydata['section_id']])->get();
            $getcategories = json_decode(json_encode($getcategories), true);
            $category = Category::find($id);
            $message = "Category Updated Successfully";

        }

        if ($request->isMethod('post')) {
            $data = $request->all();

            $rules = [
                'category_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'section_id' => 'required',
                'url' => 'required',
                'category_discount' => 'required|numeric',
                'category_image' => 'image|mimes:jpeg,jpg,png,gif,svg',

            ];
            $customMessages = [
                'category_name.required' => "Category Name is Required!",
                'category_name.regex' => "Valid Category Name is Required!",
                'section_id.required' => "Section Must be Required!",
                'url.required' => "Category URL is Required!",
                'category_discount.required' => "Category Discount Must be Required!",
                'category_discount.numeric' => "Category Discount Must be a Positive Number!",
                'category_image.image' => "File Must be an Image!",
                'category_image.mimes' => "File Must be jpeg,jpg,png,gif or svg",

            ];
            $this->validate($request, $rules, $customMessages);

            // Upload Category Images
            if ($request->hasFile('category_image')) {
                $image_tmp = $request->file('category_image');
                if ($image_tmp->isValid()) {
                    // Generate New Image
                    $extention = $image_tmp->getClientOriginalExtension();
                    $imageName = rand(111, 99999) . '.' . $extention;
                    $imagePath = 'images/category_images/' . $imageName;
                    //Upload The Image
                    Image::make($image_tmp)->save($imagePath);
                    $category->category_image = $imageName;
                }
            }
            // Upload Category Images   End

            if (empty($data['description'])) {
                $data['description'] = "";
            }
            if (empty($data['meta_title'])) {
                $data['meta_title'] = "";
            }
            if (empty($data['meta_description'])) {
                $data['meta_description'] = "";
            }
            if (empty($data['meta_keywords'])) {
                $data['meta_keywords'] = "";
            }

            $category->parent_id = $data['parent_id'];
            $category->section_id = $data['section_id'];
            $category->category_name = $data['category_name'];
            $category->category_discount = $data['category_discount'];
            $category->description = $data['description'];
            $category->url = $data['url'];
            $category->meta_title = $data['meta_title'];
            $category->meta_description = $data['meta_description'];
            $category->meta_keywords = $data['meta_keywords'];
            $category->status = 1;
            $category->save();
            Session::flash('success_message', $message);
            return redirect('admin/categories');
        }
        //Get All section
        $getsection = Section::get();
        return view('admin.categories.add_edit_category')->with(compact('title', 'getsection', 'categorydata', 'getcategories'));
    }

    public function appendCategoryLevel(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $getcategories = Category::with('subcategories')->where(['section_id' => $data['section_id'], 'parent_id' => 0, 'status' => 1])->get();
            $getcategories = json_decode(json_encode($getcategories), true);
            return view('admin.categories.append_categories_level', compact('getcategories'));
        }
    }

    public function deleteCategoryImage($id)
    {
        $categoryImage = Category::select('category_image')->where('id', $id)->first();
        $category_image_path = 'images/category_images/';
        if (file_exists($category_image_path . $categoryImage->category_image)) {
            unlink($category_image_path . $categoryImage->category_image);
        }
        Category::where('id', $id)->update(['category_image' => '']);
        Session::flash('success_message', 'Category Image deleted successfully');
        return redirect()->back();
    }

    public function deleteCategory($id)
    {
        Category::where('id', $id)->delete();
        Session::flash('success_message', 'Category Has been deleted successfully');
        return redirect()->back();
    }

}
