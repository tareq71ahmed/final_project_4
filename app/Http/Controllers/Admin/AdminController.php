<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\AdminsRole;
use App\Http\Controllers\Controller;
use App\Order;
use App\OtherSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{

    public function dashboard()
    {
        Session::put('page', 'dashboard');

        //$current_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->month)->count();
        $current_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->month)->count();
        $before_1_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(1))->count();
        $before_2_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(2))->count();
        $before_3_month_orders = Order::whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(3))->count();
        $ordersCount = array($current_month_orders,$before_1_month_orders,$before_2_month_orders,$before_3_month_orders);

        $current_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->month)->count();
        $before_1_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(1))->count();
        $before_2_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(2))->count();
        $before_3_month_orders1 = Order::where('payment_status','Paid')->whereYear('created_at', Carbon::now()->year)->WhereMonth('created_at', Carbon::now()->subMonth(3))->count();
        $ordersCount1 = array($current_month_orders1,$before_1_month_orders1,$before_2_month_orders1,$before_3_month_orders1);

        return view('admin.admin_dashboard',compact('ordersCount','ordersCount1'));
    }

    public function settings()
    {
        Session::put('page', 'settings');
        $adminDetails = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        return view('admin.admin_settings', compact('adminDetails'));
    }

    public function login(Request $request)
    {

        if ($request->isMethod('post')) {
            $data = $request->all();
            $rules = [
                'email' => 'required|email|max:255', //unique:admins
                'password' => 'required',
            ];
            $customMessages = [
                'email.required' => "Email is Required!",
                'email.email' => "Valid Email is Required!",
                'password.required' => "password is Required!",
            ];

            $this->validate($request, $rules, $customMessages);

            if (Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password'], 'status' => 1])) {
                return redirect('admin/dashboard');
            } else {
                Session::flash('error_message', 'Invalid email or Password');
                return redirect()->back();
            }
        }
        return view('admin.admin_login');

    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/admin');
    }

    public function chkCurrentPassword(Request $request)
    {
        $data = $request->all();
        if (Hash::check($data['current_pwd'], Auth::guard('admin')->user()->password)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function updateCurrentPassword(Request $request)
    {
        if ($request->isMethod('post')) {
            $data = $request->all();
            if (Hash::check($data['current_pwd'], Auth::guard('admin')->user()->password)) {
                if ($data['new_pwd'] == $data['confirm_pwd']) {

                    Admin::where('id', Auth::guard('admin')->user()->id)->update(['password' => bcrypt($data['new_pwd'])]);
                    Session::flash('success_message', "Password Updated Successfully");

                } else {
                    Session::flash('error_message', "New Password And Confirm Password not Match");

                }

            } else {
                Session::flash('error_message', "Your Current Password is incorrect");

            }
            return redirect()->back();
        }

    }

    public function updateAdminDetails(Request $request)
    {
        Session::put('page', 'update-admin-details');
        if ($request->isMethod('post')) {
            $data = $request->all();
            $rules = [
                'admin_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'admin_mobile' => 'required|numeric',
                'admin_image' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ];
            $customMessages = [
                'admin_name.required' => "Name is Required!",
                'admin_name.regex' => "Valid Name is Required!",
                'admin_mobile.required' => "Mobile Number is Required!",
                'admin_mobile.numeric' => "Valid Mobile Number is Required!",
                'admin_image.image' => "File Must be an Image!",
                'admin_image.mimes' => "File Must be jpeg,jpg,png,gif or svg",
            ];
            $this->validate($request, $rules, $customMessages);
            //Upload Image
            if ($request->hasFile('admin_image')) {
                $image_tmp = $request->file('admin_image');
                if ($image_tmp->isValid()) {
                    // Generate New Image
                    $extention = $image_tmp->getClientOriginalExtension();
                    $imageName = rand(111, 99999) . '.' . $extention;
                    $imagePath = 'images/admin_images/admin_photos/' . $imageName;
                    //Upload The Image
                    Image::make($image_tmp)->resize(700, 700)->save($imagePath);
                } else if (!empty($data['current_admin_image'])) {
                    $imageName = $data['current_admin_image'];
                } else {
                    $imageName = "";
                }
            }

            //Update Admin Details
            Admin::where('email', Auth::guard('admin')->user()->email)->update(
                [
                    'name' => $data['admin_name'],
                    'mobile' => $data['admin_mobile'],
                    'image' => $imageName??"",
                ]
            );
            Session::flash('success_message', "Admin details Updated Successfully");
            return redirect()->back();

        }
        return view('admin.update_admin_details');

    }

    public function adminsSubadmins()
    {
        if (Auth::guard('admin')->user()->type == 'subadmin') {
            Session::flash('error_message', 'You Have No Access on this Page ! This Feature is Restricted ');
            return redirect("admin/dashboard");
        }
        Session::put('page', 'admins-subadmins');
        $admins_subadmins = Admin::get();
        return view('admin.admins_subadmins.admins_subadmins', compact('admins_subadmins'));
    }

    public function update_admin_Subadmin_Status(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            if ($data['status'] == "Active") {
                $status = 0;
            } else {
                $status = 1;
            }
            Admin::where('id', $data['admin_id'])->update(['status' => $status]);
            return response()->json(['status' => $status, 'admin_id' => $data['admin_id']]);
        }
    }

    public function deleteAdminSubadmin($id)
    {
        Admin::where('id', $id)->delete();
        $message = "Admin/subAdmin Has been deleted successfully";
        Session::flash('success_message', $message);
        return redirect()->back();
    }

    public function addEditAdminSubadmin(Request $request, $id = null)
    {
        Session::put('page', 'admins-subadmins');
        if ($id == "") {
            //Add Admin/Sub-Admin
            $title = "Admin/Sub-Admin";
            $admindata = new Admin;

            $message = "Admin/Sub-Admin has been added successfully";

        } else {
            //Edit Admin/Sub-Admin
            $title = "Update Admin/Sub-Admin";
            $admindata = Admin::find($id);
            $message = "Admin/Sub-Admin has been Updated successfully";

        }

        if ($request->isMethod('post')) {
            $data = $request->all();
            if ($id == "") {
                $adminCount = Admin::where('email', $data['admin_email'])->count();
                if ($adminCount > 0) {
                    Session::flash('error_message', 'Admin/Sub-Admin Already Exists');
                    return redirect('admin/admins-subadmins');
                }
            }
            $rules = [
                'admin_name' => 'required|regex:/^[\pL\s\-]+$/u',
                'admin_mobile' => 'required',
                'admin_image' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ];
            $customMessages = [
                'admin_name.required' => "Name is Required!",
                'admin_name.regex' => "Valid Name is Required!",
                'admin_mobile.required' => "Mobile Number is Required!",
               /* 'admin_mobile.numeric' => "Valid Mobile Number is Required!",
                'admin_mobile.min' => "Valid Mobile Number is Required!",
                'admin_mobile.max' => "Valid Mobile Number is Required!",*/
                'admin_image.image' => "File Must be an Image!",
                'admin_image.mimes' => "File Must be jpeg,jpg,png,gif or svg",
            ];
            $this->validate($request, $rules, $customMessages);

            //Upload Image
            if ($request->hasFile('admin_image')) {
                $image_tmp = $request->file('admin_image');
                if ($image_tmp->isValid()) {
                    // Generate New Image
                    $extention = $image_tmp->getClientOriginalExtension();
                    $imageName = rand(111, 99999) . '.' . $extention;
                    $imagePath = 'images/admin_images/admin_photos/' . $imageName;
                    //Upload The Image
                    Image::make($image_tmp)->resize(700, 700)->save($imagePath);
                } else if (!empty($data['current_admin_image'])) {
                    $imageName = $data['current_admin_image'];
                } else {
                    $imageName = "";
                }
            }

            $admindata->image = $imageName ?? "";
            $admindata->name = $data['admin_name'];
            $admindata->mobile = $data['admin_mobile'];
            if ($id == "") {
                $admindata->email = $data['admin_email'];
                $admindata->type = $data['admin_type'];
            }
            if ($data['admin_password'] != "") {
                $admindata->password = bcrypt($data['admin_password']);
            }
            $admindata->save();
            Session::flash('success_message', $message);
            return redirect('admin/admins-subadmins');


        }

        return view("admin.admins_subadmins.add_edit_admin_subadmin", compact('title', 'admindata', 'message'));
    }

    public function updateRole(Request $request, $id)
    {
        Session::put('page', 'admins-subadmins');
        if ($request->isMethod('post')) {
            $data = $request->all();
            unset($data['_token']);

            AdminsRole::where('admin_id', $id)->delete();

            foreach ($data as $key => $value) {

                if (isset($value['view'])) {
                    $view = $value['view'];
                } else {
                    $view = 0;
                }

                if (isset($value['edit'])) {
                    $edit = $value['edit'];
                } else {
                    $edit = 0;
                }

                if (isset($value['full'])) {
                    $full = $value['full'];
                } else {
                    $full = 0;
                }

                AdminsRole::where('admin_id', $id)->insert(['admin_id' => $id, 'module' => $key, 'view_access' => $view, 'edit_access' => $edit, 'full_access' => $full]);

            }
            $message = "Roles And Permission Updated Successfully";
            Session::flash('success_message', $message);
            return redirect()->back();
        }

        $adminDetails = Admin::where('id', $id)->first()->toArray();
        $adminRoles = AdminsRole::where('admin_id', $id)->get()->toArray();

        $title = "Update" . "   " . $adminDetails['name'] . "   " . "(" . $adminDetails['type'] . ")" . "  " . " Roles/Permission";
        return view("admin.admins_subadmins.update_roles", compact('title', 'adminDetails', 'adminRoles'));

    }

    public function updateOtherSettings(Request $request)
    {
        Session::put('page', 'update-other-settings');
        $updateOtherSettings = OtherSetting::where('id', 1)->first()->toArray();
        $title = 'Other Settings';

        if ($request->isMethod('post')) {
            $data = $request->all();
            OtherSetting::where('id', 1)->update(['min_cart_value' => $data['min_cart_value'], 'max_cart_value' => $data['max_cart_value']]);
            $message = "Min/Max Cart Value Updated Successfully";
            Session::flash('success_message', $message);
            return redirect()->back();
        }

        return view('admin.other_settings', compact('title', 'updateOtherSettings'));

    }

}
