<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*Route::get( '/', function () {
return view( 'welcome' );
} );*/

//  app/Http/Middleware/VerifyCsrfToken.php

use App\Category;
use App\CmsPage;
use App\Http\Controllers\SslCommerzPaymentController;

/*Auth::routes();*/

Route::get( '/home', 'HomeController@index' )->name( 'home' );

Route::prefix( '/admin' )->namespace( 'Admin' )->group( function () {
    //All the Admin routes will be defined Here
    Route::match( ['get', 'post'], '/', 'AdminController@login' );
    Route::group( ['middleware' => ['admin']], function () {
        Route::get( 'dashboard', 'AdminController@dashboard' );
        Route::get( 'settings', 'AdminController@settings' );
        Route::get( 'logout', 'AdminController@logout' );
        Route::post( 'check-current-pwd', 'AdminController@chkCurrentPassword' );
        Route::post( 'update-current-pwd', 'AdminController@updateCurrentPassword' );
        Route::match( ['get', 'post'], 'update-admin-details', 'AdminController@updateAdminDetails' );

        // sections

        Route::get( 'sections', 'SectionController@sections' );
        Route::post( 'update-section-status', 'SectionController@updateSectionStatus' );

        //Brands

        Route::get( 'brands', 'BrandController@brands' );
        Route::post( 'update-brand-status', 'BrandController@updateBrandStatus' );
        Route::match( ['get', 'post'], 'add-edit-brand/{id?}', 'BrandController@addEditBrand' );
        Route::get( 'delete-brand/{id}', 'BrandController@deleteBrand' );

        // Categories

        Route::get( 'categories', 'CategoryController@categories' );
        Route::post( 'update-category-status', 'CategoryController@updateCategoryStatus' );
        Route::match( ['get', 'post'], 'add-edit-category/{id?}', 'CategoryController@addEditCategory' );
        Route::post( 'append-categories-level', 'CategoryController@appendCategoryLevel' );
        Route::get( 'delete-category-image/{id}', 'CategoryController@deleteCategoryImage' );
        Route::get( 'delete-category/{id}', 'CategoryController@deleteCategory' );

        //Products

        Route::get( 'products', 'ProductsController@products' );
        Route::post( 'update-product-status', 'ProductsController@updateProductStatus' );
        Route::get( 'delete-product/{id}', 'ProductsController@deleteProduct' );
        Route::match( ['get', 'post'], 'add-edit-product/{id?}', 'ProductsController@addEditProduct' );
        Route::get( 'delete-product-image/{id}', 'ProductsController@deleteProductImage' );
        Route::get( 'delete-product-video/{id}', 'ProductsController@deleteProductVideo' );

        //Products Attributes

        Route::match( ['get', 'post'], 'add-attributes/{id}', 'ProductsController@addAttributes' );
        Route::post( 'edit-attributes/{id}', 'ProductsController@editAttributes' );
        Route::post( 'update-attribute-status', 'ProductsController@updateAttributeStatus' );
        Route::get( 'delete-attribute/{id}', 'ProductsController@deleteAttribute' );

        //Add Multiple Images

        Route::match( ['get', 'post'], 'add-images/{id}', 'ProductsController@addImages' );
        Route::post( 'update-image-status', 'ProductsController@updateImageStatus' );
        Route::get( 'delete-image/{id}', 'ProductsController@deleteImage' );

        //Banner
        Route::get( 'banners', 'BannersController@banners' );
        Route::match( ['get', 'post'], 'add-edit-banner/{id?}', 'BannersController@addeditBanner' );
        Route::post( 'update-banner-status', 'BannersController@updateBannerStatus' );
        Route::get( 'delete-banner-image/{id}', 'BannersController@deleteBannerImage' );
        Route::get( 'delete-banner/{id}', 'BannersController@deleteBanner' );
        //Coupon
        Route::get( 'coupons', 'CouponsController@coupons' );
        Route::post( 'update-coupon-status', 'CouponsController@updateCouponStatus' );
        Route::match( ['get', 'post'], 'add-edit-coupon/{id?}', 'CouponsController@addEditCoupon' );
        Route::get( 'delete-coupon/{id}', 'CouponsController@deleteCoupon' );

        //orders
        Route::get( 'orders', 'OrdersController@orders' );
        Route::get( 'orders/{id}', 'OrdersController@orderDetails' );
        Route::post( 'update-order-status', 'OrdersController@updateorderStatus' );
        Route::get( 'view-order-invoice/{id}', 'OrdersController@viewOrderinvoice' );
        Route::get( 'print-order-invoice/{id}', 'OrdersController@printPDFinvoice' );
        //orders chart
        Route::get( 'view-orders-charts', 'OrdersController@viewOrdersCharts' );
        //filter order
        Route::get( 'filter/order', 'OrdersController@filterOrder' )->name('filter-order');
        Route::get( 'order/graph', 'OrdersController@orderGraph' )->name('order-graph');
        Route::get( 'payment/graph', 'OrdersController@paymentGraph' )->name('order-payment-graph');
        Route::get( 'payment/method/graph', 'OrdersController@paymentMethodGraph' )->name('order-method-graph');
        //refund 
        Route::get( 'refund/request/{id}', 'OrdersController@refundRequest' )->name('refund-request');
        Route::post( 'refund/save', 'OrdersController@saveRefund' )->name('save-refund-status');
        

        //Shipping Charges
        Route::get( 'view-shipping-charges', 'ShippingController@viewShippingCharges' );
        Route::post( 'update-shipping-status', 'ShippingController@updateShippingStatus' );
        Route::match( ['get', 'post'], 'edit-shipping-charges/{id?}', 'ShippingController@editShippingCharges' );

        //Users
        Route::get( 'users', 'UsersController@users' );
        Route::post( 'update-user-status', 'UsersController@updateUserStatus' );

        //View Users
        Route::get( 'view-users-chart', 'UsersController@viewUsersChart' );


        //Cms
        Route::get( 'cms-pages', 'CmsController@cmsPages' );
        Route::post( 'update-page-status', 'CmsController@updatePageStatus' );
        Route::match( ['get', 'post'], 'add-edit-cms-page/{id?}', 'CmsController@addEditCmsPage' );
        Route::get( 'delete-page/{id}', 'CmsController@deleteCmsPage' );

        // Admin / SubAdmin
        Route::get( 'admins-subadmins', 'AdminController@adminsSubadmins' );
        Route::match( ['get', 'post'], 'add-edit-admin-subadmin/{id?}', 'AdminController@addEditAdminSubadmin' );
        Route::post( 'update-admin-status', 'AdminController@update_admin_Subadmin_Status' );
        Route::get( 'delete-admin/{id}', 'AdminController@deleteAdminSubadmin' );
        Route::match( ['get', 'post'], 'update-role/{id}', 'AdminController@updateRole' );

        //Other Settings
        Route::match( ['get', 'post'], 'update-other-settings', 'AdminController@updateOtherSettings' );

        //Ratings
        Route::get( 'ratings', 'RatingsController@ratings' );
        Route::post( 'update-rating-status', 'RatingsController@update_rating_status' );


    } );

} );

/*  Frontend  */
Route::namespace ( 'Front' )->group( function () {
    Route::get( '/', 'IndexController@index' );

    //Category Route
    $catUrls = Category::select( 'url' )->where( 'status', 1 )->get()->pluck( 'url' )->toArray();
    foreach ( $catUrls as $url ) {
        Route::get( '/' . $url, 'ProductsController@listing' );
    }

    //CMS Route
    $cmsUrls = CmsPage::select( 'url' )->where( 'status', 1 )->get()->pluck( 'url' )->toArray();
    foreach ( $cmsUrls as $url ) {
        Route::get( '/' . $url, 'CmsController@cmsPage' );
    }

    //Product Detail Page
    Route::get( '/product/{id}', 'ProductsController@detail' )->name( 'detailpage' );
    Route::post( '/get-product-price', 'ProductsController@getproductprice' );
    //Add To Cart
    Route::post( '/add-to-cart', 'ProductsController@addtocart' );

    //Shopping Cart Route
    Route::get( '/cart', 'ProductsController@cart' );
    //Update Cart Item
    Route::post( '/update-cart-item-qty', 'ProductsController@updateCartItemQty' );

    //Delete Cart Item
    Route::post( '/delete-cart-item', 'ProductsController@deleteCartItem' );

    //Login/Register
    Route::get( '/login-register', ['as' => 'login', 'uses' => 'UsersController@loginRegister'] );
    //Login user
    Route::post( '/login', 'UsersController@loginUser' );
    //Register user
    Route::post( '/register', 'UsersController@registerUser' );
    //Check Email If Exists
    Route::match( ['get', 'post'], '/check-email', 'UsersController@checkEmail' );
    //Logout User
    Route::get( '/logout', 'UsersController@logoutUser' );

    //Confirm Account
    Route::match( ['get', 'post'], '/confirm/{code}', 'UsersController@confirmAccount' );
    //Forgot Password
    Route::match( ['get', 'post'], '/forgot-password', 'UsersController@forgotpassword' );

    //check Delivery Pincode
    Route::post( '/check-pincode', 'ProductsController@checkPincode' );

    //Product search
    Route::get( '/search-products', 'ProductsController@listing' );

    //contact page
    Route::match( ['get', 'post'], '/contact', 'CmsController@contact' );

    //Product Rating & Review
    Route::match( ['get', 'post'], '/add-rating', 'RatingsController@addRating' );

    Route::group( ['middleware' => ['auth']], function () {

        //Users Account
        Route::match( ['get', 'post'], '/account', 'UsersController@account' );

        //Users Orders
        Route::get( '/orders', 'OrdersController@orders' );

        //User Order Details
        Route::get( '/orders/{id}', 'OrdersController@orderDetails' );
        //refund order
        Route::get( '/refund/order/{id}', 'OrdersController@refundOrder')->name('refund-order');
        Route::post( '/refund/order', 'OrdersController@saveRefundInfo')->name('save-refund-info');
        Route::get( '/cancel/order/{id}', 'OrdersController@cancelOrder')->name('cancel-order');
        

        //Check User Current Password
        Route::post( '/check-user-pwd', 'UsersController@chkUserPassword' );
        //Update User Current Password
        Route::post( '/update-user-pwd', 'UsersController@updateUserPassword' );
        //Apply Coupon
        Route::post( '/apply-coupon', 'ProductsController@applyCoupon' );

        //checkout
        Route::match( ['get', 'post'], '/checkout', 'ProductsController@checkout' );

        //Add-Edit-Delivery Address
        Route::match( ['get', 'post'], '/add-edit-delivery-address/{id?}', 'ProductsController@addEditDeliveryAddress' );

        //Delete  Delivery Address
        Route::get( '/delete-delivery-address/{id}', 'ProductsController@deleteDeliveryAddress' );

        //Thanks Page
        Route::get( '/thanks', 'ProductsController@thanks' );

        //Paypal
        Route::get( '/paypal', 'PaypalController@paypal' );
        //sslcommerz
        Route::get('/sslcommerz', [SslCommerzPaymentController::class, 'sslcommerz']);
        Route::get('/paynow/{id}', [SslCommerzPaymentController::class, 'paynow'])->name('pay-now');
        //paypal success
        Route::get( '/paypal/success', 'PaypalController@success' );

        //paypal Fail
        Route::get( '/paypal/fail', 'PaypalController@fail' );

        //paypal IPN
        Route::post( '/paypal/ipn', 'PaypalController@ipn' );
        

    } );
    // SSLCOMMERZ Start
    Route::get('/example1', [SslCommerzPaymentController::class, 'exampleEasyCheckout']);
    Route::get('/example2', [SslCommerzPaymentController::class, 'exampleHostedCheckout']);

    Route::post('/pay', [SslCommerzPaymentController::class, 'index']);
    Route::post('/pay-via-ajax', [SslCommerzPaymentController::class, 'payViaAjax']);

    Route::post('/success', [SslCommerzPaymentController::class, 'success']);
    Route::post('/fail', [SslCommerzPaymentController::class, 'fail']);
    Route::post('/cancel', [SslCommerzPaymentController::class, 'cancel']);

    Route::post('/ipn', [SslCommerzPaymentController::class, 'ipn']);
    //SSLCOMMERZ END

} );


