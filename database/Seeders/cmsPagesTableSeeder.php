<?php

namespace Database\Seeders;

use App\CmsPage;
use Illuminate\Database\Seeder;

class cmsPagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $cmsPageRecords =[
                ['id'=>1,'title'=>'About us','description'=>"Content Page is coming","url"=>"about-us",
                    "meta_title"=>"About us","meta_description"=>"About E-commerce website","meta_keywords"=>"about-us,about-ecommerce","status"=>1],

              ['id'=>2,'title'=>'Privacy Policy','description'=>"Content Page is coming","url"=>"privacy-policy",
                  "meta_title"=>"Privacy policy","meta_description"=>"Privacy Policy of E-commerce website","meta_keywords"=>"privacy-policy","status"=>1]
          ];

          CmsPage::insert($cmsPageRecords);
    }
}
