<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Coupon;


class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        $couponRecords =[
            ['id' => 1,'coupon_option'=>'Manual','coupon_code'=>'test,eidmubarak,ramadan','categories'=>'1,2','users'=>'tareq@gmail.com,amit@gmail.com,bayzid@gmail.com','coupon_type'=>'Single',
                'amount_type'=>'Percentage,','amount'=>'10','expiry_date'=>'2021-12-30','status'=>'1'],
        ];
        Coupon::insert($couponRecords);

    }
}
