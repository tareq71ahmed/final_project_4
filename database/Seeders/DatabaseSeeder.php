<?php
namespace Database\Seeders;
use AdminsTableSeeder;
use BannersTableSeeder;
use BrandTableSeeder;
use CategoryTableSeeder;
use Illuminate\Database\Seeder;
use ProductsAttributesTableSeeder;
use ProductsTableseeder;
use SectionsTableSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
          $this->call(AdminsTableSeeder::class);
          $this->call(SectionsTableSeeder::class);
          $this->call(CategoryTableSeeder::class);
          $this->call(ProductsTableseeder::class);
          $this->call(ProductsAttributesTableSeeder::class);
        // // $this->call(ProductsImagesTableSeeder::class); //ata bad dichi
          $this->call(BrandTableSeeder::class);
          $this->call(BannersTableSeeder::class);
          $this->call(CouponsTableSeeder::class);
          $this->call(CountryTableSeeder::class);
          $this->call(UserTableSeeder::class);
          $this->call(DeliveryAddressTableSeeder::class);
          $this->call(OrderStatusTableSeeder::class);
          $this->call(ShippingChargesSeeder::class);
          $this->call(CodPincodesTableSeeder::class);
          $this->call(PrepaidPincodesTableSeeder::class);
          $this->call(cmsPagesTableSeeder::class);
          $this->call(OthersettingsTableSeeder::class);
          $this->call(RatingsTableSeeder::class);
    }
}
