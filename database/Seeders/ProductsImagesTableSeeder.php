<?php

use Illuminate\Database\Seeder;
use App\ProductsImage;
class ProductsImagesTableSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        $productsImageRecords=[
            ['id'=>1,'product_id'=>1,'image'=>'no_image.png','status'=>'1'],
        ];
        ProductsImage::insert($productsImageRecords);
    }
}
