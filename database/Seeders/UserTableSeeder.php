<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRecords =[
            ['id' => 1,'name'=>'Tareq Ahmed','address'=>'Dhaka','city'=>'Dhaka','state'=>'Dhaka','country'=>'Bangladesh','pincode'=>'1000',
                'mobile'=>'01679091279','email'=>'tareq@gmail.com','email_verified_at'=>'','password'=>'$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2','status'=>'1','remember_token'=>''],
            ['id' => 2,'name'=>'Amit saha','address'=>'Dhaka','city'=>'Dhaka','state'=>'Dhaka','country'=>'Bangladesh','pincode'=>'1000',
                'mobile'=>'01000000000','email'=>'amit@gmail.com','email_verified_at'=>'','password'=>'$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2','status'=>'1','remember_token'=>''],
            ['id' => 3,'name'=>'Bayzid Ahmed','address'=>'Dhaka','city'=>'Dhaka','state'=>'Dhaka','country'=>'Bangladesh','pincode'=>'1000',
                'mobile'=>'02345678901','email'=>'bayzid@gmail.com','email_verified_at'=>'','password'=>'$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2','status'=>'1','remember_token'=>''],
        ];

        User::insert($userRecords);
    }
}
