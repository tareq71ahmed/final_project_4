<?php

namespace Database\Seeders;

use App\Rating;
use Illuminate\Database\Seeder;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ratingRecords = [
            ['id'=>1,'user_id'=>1,'product_id'=>1,'review'=>'very very Good','rating'=>5,'status'=>1],
            ['id'=>2,'user_id'=>2,'product_id'=>2,'review'=>'very very Good!Recommend','rating'=>5,'status'=>1],
            ['id'=>3,'user_id'=>1,'product_id'=>1,'review'=>'very very Good!Recommend','rating'=>5,'status'=>1],
            ['id'=>4,'user_id'=>3,'product_id'=>1,'review'=>'very very Good!Recommend','rating'=>5,'status'=>1],
        ];

        Rating::insert($ratingRecords);
    }
}
