<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        //'email'=>'admin@gmail.com'
        // password : 12345678
        $adminRecords = [
            ['id' => 1, 'name' => 'admin', 'type' => 'superadmin', 'mobile' => '04123456789', 'email' => 'admin@gmail.com',
                'password' => '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', 'image' => '', 'status' => '1'],
            ['id' => 2, 'name' => 'Amit Saha', 'type' => 'subadmin', 'mobile' => '04160100961', 'email' => 'amit@gmail.com',
                'password' => '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', 'image' => '', 'status' => '1'],
            ['id' => 3, 'name' => 'Bayzid Ahmed', 'type' => 'subadmin', 'mobile' => '04160100976', 'email' => 'bayzid@gmail.com',
                'password' => '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', 'image' => '', 'status' => '1'],
            ['id' => 4, 'name' => 'Tareq Ahmed', 'type' => 'admin', 'mobile' => '04160100955', 'email' => 'tareq@gmail.com',
                'password' => '$2y$10$i.NM69WdMMRPVSTjSyrSMu1.icpFNMIngVT3NFw9KrHPtvhCEfgU2', 'image' => '', 'status' => '1']
        ];

        DB::table('admins')->insert($adminRecords);

        /*
        foreach($adminRecords as $key => $record){
            \App\Admin::create($record);
        }
        */


    }
}
