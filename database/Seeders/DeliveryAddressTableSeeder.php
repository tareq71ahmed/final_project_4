<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\DeliveryAddress;

class DeliveryAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deliveryRecords = [
            ['id' => 1, 'user_id' => 1, 'name' => 'Tareq Ahmed', 'address' => 'Dhaka', 'city' => 'Dhaka', 'state' => 'Dhaka', 'pincode' => '1000','country_name'=>'Bangladesh', 'mobile' => '01679091279'],
            ['id' => 2, 'user_id' => 2, 'name' => 'Amit saha', 'address' => 'Dhaka', 'city' => 'Dhaka', 'state' => 'Dhaka', 'pincode' => '1000','country_name'=>'Bangladesh', 'mobile' => '01000000000'],
            ['id' => 3, 'user_id' => 3, 'name' => 'Bayzid Ahmed', 'address' => 'Dhaka', 'city' => 'Dhaka', 'state' => 'Dhaka', 'pincode' => '1000','country_name'=>'Bangladesh', 'mobile' => '02345678901']

        ];
        DeliveryAddress::insert($deliveryRecords);
    }
}
