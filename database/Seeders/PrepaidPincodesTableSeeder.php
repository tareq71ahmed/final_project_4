<?php

namespace Database\Seeders;

use App\PrepaidPincodes;
use Illuminate\Database\Seeder;

class PrepaidPincodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prepaidPincodes = [
            ['id' => 1, 'pincode' => '1000'],
            ['id' => 2, 'pincode' => '1001'],
            ['id' => 3, 'pincode' => '1002'],
            ['id' => 4, 'pincode' => '1003'],
            ['id' => 5, 'pincode' => '1004'],
            ['id' => 6, 'pincode' => '1005'],
        ];

        PrepaidPincodes::insert($prepaidPincodes);
    }
}
