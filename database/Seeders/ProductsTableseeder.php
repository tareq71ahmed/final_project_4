<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductsTableseeder extends Seeder
{
    /**
     * Run the database Seeders.
     *
     * @return void
     */
    public function run()
    {

        $productRecords = [
            ['id' => 1, 'category_id' => 1, 'section_id' => 1,'brand_id' => 1,'product_name' => 'Blue Casual T-shirt', 'product_code' => 'BT001', 'product_color' => 'Blue','group_code'=>'100',
                'product_price' => '1500', 'product_discount' => '10', 'product_weight' => '200', 'product_video' => '', 'main_image' => '',
                'description' => 'This is 100% Original', 'wash_care' => '', 'fabric' => 'Cotton', 'pattern' => 'Plain', 'sleeve' => 'Half Sleeve',
                'fit' => 'Slim', 'occasion' => 'Casual', 'meta_title' => '', 'meta_description' => '', 'meta_keywords' => '', 'is_featured' => 'Yes', 'status' => 1
            ],

            ['id' => 2, 'category_id' => 2, 'section_id' => 1,'brand_id' => 2, 'product_name' => 'Red Casual T-shirt', 'product_code' => 'R001', 'product_color' => 'Red','group_code'=>'100',
                'product_price' => '2000', 'product_discount' => '10', 'product_weight' => '200', 'product_video' => '', 'main_image' => '',
                'description' => 'This is 100% Original', 'wash_care' => '', 'fabric' => 'Cotton', 'pattern' => 'Plain', 'sleeve' => 'Half Sleeve',
                'fit' => 'Slim', 'occasion' => 'Casual', 'meta_title' => '', 'meta_description' => '', 'meta_keywords' => '', 'is_featured' => 'Yes', 'status' => 1
            ]

        ];
        Product::insert($productRecords);


    }
}
