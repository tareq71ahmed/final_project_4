<?php

namespace Database\Seeders;

use App\OtherSetting;
use Illuminate\Database\Seeder;

class OthersettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $OtherSettings = [
            ['id' => 1, 'min_cart_value' => 250, 'max_cart_value' => 10000],

        ];
        OtherSetting::insert($OtherSettings);
    }
}
